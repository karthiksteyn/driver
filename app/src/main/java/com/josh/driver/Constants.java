package com.josh.driver;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Sugan on 15/3/17.
 */
public interface Constants {

    interface AppKey {
        String EXTERNAL_FOLDER_NAME = "josh/driver";
        String GOOGLE_API_KEY = "AIzaSyBtvnkVg5R9BPCGDVV-31tDIB0ehkK72iM";
        String MODE = "ANDROID";
        String APP_TOKEN = "DAPP";
        LatLng CHENNAI_LATLNG = new LatLng(13.067439, 80.237617);
        String FILE_PROVIDER_AUTHORITY = "com.josh.driver.fileprovider";
        String WAITING_REQUEST = "waiting_request";
        String FORCE_REQUEST = "force_request";
        String SIGNOUT_EXPIRY = "signout_expiry";
        String FORCE_TRIP_STATUS = "force_trip_status";
        int OFF_DUTY = 0;
        int ON_DUTY = 1;
        int GO_HOME = 2;

    }

    interface Receiver {
        String RECEIVER_KEY = "com.josh.driver.request.receiver";
        String BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
        String LOCATION_RECEIVER = "com.josh.driver.location.receiver";
    }

    interface TripStatus {
        int WAIT_LOCATE = 3;
        int WAIT_VERIFY = 4;
        int WAIT_START = 5;
        int WAIT_STOP = 6;
        int WAIT_COMPLETE = 7;
        int WAIT_RATE = 8;
        int RATED = 9;

    }

    interface Headers {
        String AUTHORIZATION = "x-access-token";
        String APP_AUTHORIZATION = "x-app-token";
        String USER_AUTHORIZATION = "x-user-token";
    }

    interface Message {
        String NAME_EMPTY = "Name should not be empty";
        String NAME_VALIDATION_FAILED = "Name should be more than 2 characters";
        String EMAIL_EMPTY = "Email should not be empty";
        String EMAIL_VALIDATION_FAILED = "Please enter a valid email address";
        String NO_INTERNET = "No Internet Connection";
        String VERIFICATION_CODE_EMPTY = "Verification code should not be empty";
        String VERIFICATION_CODE_VALIDATION_FAILED = "Verification code should be more than 3 characters";
        String UNKNOWN = "UnKnown";
    }

    interface AlertMessage {
        //common messages for api failure
        String NO_INTERNET_CONNECTION = "Please check your internet connection and try again.";
        String DATA_NOT_FOUND = "No data found. Contact our support center.";
        String JSON_PARSING = "Unable to send data to our server. Contact our support center.";
        String TIME_OUT = "Unable to connect to our server. Make sure you are having a stable internet connection.";
        String SERVER_CONNECTION_FAILED = "Unable to connect to our server. Make sure you are having a stable internet connection.";
        String UNABLE_TO_CONNECT_SERVER = "Unable to connect to our server. Please try again later.";
        String LOG_OUT_TITLE = "Log Out?";
        String LOG_OUT_CONTENT = "Do you want to Continue?";
        String LOG_IN_EXPIRED_TITLE = "Login Expired!";
        String LOG_IN_EXPIRED_CONTENT = "Oops! User logged in other device.";
        String YES = "YES";
        String CANCEL = "CANCEL";
    }

    interface BundleKey {
        String CURRENT_POSITION = "current_position";
        String CURRENT_VALUE = "current_value";
        String REQUEST = "request";
        String LATITUDE = "latitude";
        String LONGITUDE = "longitude";
        String CONTACT_LIST = "contactList";
        String PHONE_NUMBER = "phoneNumber";
        String ORDER = "order";
        String IMAGE_HANDLER_IMAGE_TYPE = "imageHandlerImageType";
        String IMAGE_HANDLER_IMAGE_LOCATION = "imageHandlerImageLocation";
    }

    interface SharedKey {
        String SERVER_TIME = "serverTime";
        String APP_KEY = "app_key";
        String DISPLAY_NAME = "display_name";
        String DISPLAY_PIC = "display_pic";
        String AUTH_TOKEN = "auth_token";
        String USER_TOKEN = "user_token";
        String DRIVER_ID = "driver_id";
        String FORCED_PWD_CHANGE = "forced_pwd_change";
        String DEFAULT_TYPE = "default_type";
        String CURRENT_TYPE = "current_type";
        String DRIVER_PHONE = "driver_phone";
        String OTP = "otp";
        String PROFILE_CREATED = "profile_created";
        String TOTAL_RIDES = "total_rides";
        String BALANCE = "balance";
        String MILEAGE = "mileage";
        String ONLINE_DURATION = "online_duration";
        String REVENUE = "revenue";
        String DUTY_STATUS = "duty_status";
        String VEHICLE_DETAILS = "vehicle_details";
        String ACTIVE_BOOKING_ID = "active_booking_id";
        String TRIP_DETAILS = "trip_details";
        String LAST_KNOWN_LOCATION = "last_known_location";

    }

    interface ErrorCode{
        //custom error codes for internal data handling
        int ERROR = 1443;
        int DATA_NOT_FOUND = 1444;
        int JSON_PARSING = 1445;
        int SERVICE_UNAVAILABLE = 1447;
        int SERVER_CONNECTION_FAILED = 1448;
        int TIME_OUT = 1449;

        //defined error codes from api
        int SESSION_EXPIRED = 401;
    }

    interface BroadcastKey {

    }

    interface ServerKey {
        String AUTHORIZATION = "authorization";
        String SOURCE_TYPE = "sourceType";
        String PROFILE_PIC = "PROFILE";
    }

    enum ValidationKey {
        EMAIL,
        NAME
    }

    interface PermissionMessages {
        String CAMERA_DENIED = "Enable storage permission to continue camera";
        String STORAGE_DENIED = "Enable storage permission to continue";
    }


}
