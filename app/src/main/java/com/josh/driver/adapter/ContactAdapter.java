package com.josh.driver.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hm.widgets.CustomTextView;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.dto.ContactDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sathish on 02-May-17.
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.CountryCodeHolder> {

    private List<ContactDto> mContactListDto;
    private List<ContactDto> mSearchingFilterList;
    private CountryCodeItemClickListener mContactItemClickListener;
    private boolean mIsFromSetUpProfile;

    public ContactAdapter(List<ContactDto> contactList, CountryCodeItemClickListener countryCodeItemClickListener) {
        mContactListDto = contactList;
        mSearchingFilterList = contactList;
        mContactItemClickListener = countryCodeItemClickListener;
    }

    public void setFilter(String newText) {
        List<ContactDto> results = new ArrayList<>();

        if (newText != null && newText.length() > 0) {
            List<ContactDto> filterList = new ArrayList();
            for (int i = 0; i < mSearchingFilterList.size(); i++) {
                if ((mSearchingFilterList.get(i).getName().toUpperCase()).contains(newText.toString().toUpperCase())) {
                    filterList.add(mSearchingFilterList.get(i));
                }
            }
            results = filterList;
        } else {
            results = mSearchingFilterList;
        }
        mContactListDto = results;
        notifyDataSetChanged();
    }

    public class CountryCodeHolder extends RecyclerView.ViewHolder {

        CustomTextView mName;
        CustomTextView mMobileNumber;

        public CountryCodeHolder(View itemView) {
            super(itemView);

            mName = (CustomTextView) itemView.findViewById(R.id.inflater_contact_list_item_name_tv);
            mMobileNumber = (CustomTextView) itemView.findViewById(R.id.inflater_contact_list_item_phone_number_tv);
        }
    }

    @Override
    public CountryCodeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_contact_list_item, null);
        return new CountryCodeHolder(view);
    }

    @Override
    public void onBindViewHolder(CountryCodeHolder holder, final int position) {
        final ContactDto countryListDto = mContactListDto.get(position);
        String name;
        name = countryListDto.getName() != null ? countryListDto.getName() : Constants.Message.UNKNOWN;
        holder.mMobileNumber.setText(countryListDto.getMbNo());
        holder.mName.setText(name);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContactItemClickListener.onContactClick(position, countryListDto);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mContactListDto.size();
    }

    public interface CountryCodeItemClickListener {
        void onContactClick(int position, ContactDto countryListDto);
    }


}
