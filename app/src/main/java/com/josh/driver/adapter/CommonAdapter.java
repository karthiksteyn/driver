package com.josh.driver.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

public class CommonAdapter<T> extends ArrayAdapter<T> {

    private final OnGetViewListener listener;
    private final int viewId;
    private List<Integer> disabledItemPosition;

    public CommonAdapter(Context context, OnGetViewListener listener) {
        super(context, 0);
        this.listener = listener;
        this.viewId = -1;
    }

    public CommonAdapter(Context context, int tvResId, List<T> objects,
                         OnGetViewListener listener) {
        super(context, tvResId, objects);
        this.listener = listener;
        this.viewId = tvResId;
    }

    public CommonAdapter(Context context, int tvResId, List<T> objects,
                         OnGetViewListener listener, List<Integer> disabledItemPosition) {
        super(context, tvResId, objects);
        this.listener = listener;
        this.viewId = tvResId;
        this.disabledItemPosition = disabledItemPosition;
    }

    public CommonAdapter(Context context, int viewId, T[] objects, OnGetViewListener listener) {
        super(context, viewId, objects);
        this.listener = listener;
        this.viewId = viewId;
    }

    public CommonAdapter(Context context, OnGetViewListener listener, int tvResId) {
        super(context, tvResId);
        this.listener = listener;
        this.viewId = tvResId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return listener.getView(position, convertView, parent, getItem(position), viewId);
    }

    public Object[] getAllItems() {
        Object[] objects = null;
        int count = getCount();
        if (count > 0) {
            objects = new Object[count];
            for (int i = 0; i < count; i++) {
                objects[i] = getItem(i);
            }
        }
        return objects;
    }

    @Override
    public boolean isEnabled(int position) {
        boolean enabled = true;
        if (null != disabledItemPosition && disabledItemPosition.size() > 0) {
            for (int i : disabledItemPosition) {
                if (i == position) {
                    enabled = false;
                }
            }
        }
        return enabled;
    }


    public interface OnGetViewListener<T> {

        View getView(int position, View convertView, ViewGroup parent,
                     T object, int viewId);

    }

}