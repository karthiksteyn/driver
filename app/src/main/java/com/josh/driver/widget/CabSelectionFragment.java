package com.josh.driver.widget;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hm.fragment.BaseFragment;
import com.hm.utils.CodeSnippet;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.adapter.CommonAdapter;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.Headers;
import com.josh.driver.dto.request.CabTypeRequest;
import com.josh.driver.dto.response.CabType;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CabSelectionFragment extends BaseFragment implements AdapterView.OnItemClickListener,
        ModelHandler.ModelHanlderListener<ApiStatusDto>, CommonAdapter.OnGetViewListener<CabType>, View.OnClickListener {

    private ListView mCabTypeListView;

    private RelativeLayout mcabTypeParentLayout;

    private View mDialogBgView;

    private CommonAdapter<CabType> mCommonAdapter;

    private CabSelectionListener mCabSelectionListener;

    List<CabType> mCabTypeList;

    private String mDriverId;

    private Headers mHeaders;

    public CabSelectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inflater_cab_selection, container, false);

        mcabTypeParentLayout = (RelativeLayout) view.findViewById(R.id.dialog_cab_type_parent_layout);
        mCabTypeListView = (ListView) view.findViewById(R.id.dialog_cab_type_lv);
        mCabTypeListView.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(hasNetworkConnection(getString(R.string.no_internet))) {
            showProgressbar();
            CabTypeRequest cabTypeRequest = new CabTypeRequest();
            final String cabTypeCode = mCabTypeList.get(position).getCode();
            cabTypeRequest.setType(cabTypeCode);
            ModelHandler.getInstance().enqueueTask(new ModelHandler.ModelHanlderListener<ApiStatusDto>() {

                @Override
                public void onSuccessApi(ApiStatusDto response) {
                    if(response.isResultOk()){
                        closeCarTypesParentView();
                        DataHandler.getInstance().saveCurrentType(cabTypeCode);
                        mCabSelectionListener.onTypeChangeSuccess(cabTypeCode);
                    }else{
                        showMessage(response.getDescription());
                    }
                    dismissProgressbar();
                }

                @Override
                public void onFailureApi(int errorCode, String errorMessage) {
                    showMessage(errorMessage);
                    dismissProgressbar();
                }

                @Override
                public void onAuthorizationFailed() {

                }
            }, RequestCreator.getInstance().onCabTypeChange(mDriverId, mHeaders, cabTypeRequest));
        }
    }

    public void setDialogBgView(View dialogBgView) {
        mDialogBgView = dialogBgView;
    }

    public void setListener(CabSelectionListener cabSelectionListener) {
        mCabSelectionListener = cabSelectionListener;
    }

        public void onCarTypeClick() {
            if(hasNetworkConnection(getString(R.string.no_internet))){
                    showProgressbar();
                mDriverId = DataHandler.getInstance().retrieveDriverId();
                mHeaders = getTokens();
                ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().getCabTypes(mDriverId, mHeaders));

            }
    }

    public void closeCarTypesParentView(){
        if (mcabTypeParentLayout.getVisibility() == View.VISIBLE) {
            mcabTypeParentLayout.setVisibility(View.GONE);
            mDialogBgView.setClickable(false);
            mDialogBgView.animate()
                    .alpha(0)
                    .setDuration(500)
                    .setInterpolator(new DecelerateInterpolator());
        }
    }

    private Headers getTokens() {
        Headers headers = new Headers();
        headers.setAuthToken(DataHandler.getInstance().retrieveAuthToken());
        headers.setUserToken(DataHandler.getInstance().retrieveUserToken());
        headers.setAppToken(Constants.AppKey.APP_TOKEN);
        return headers;
    }

    private void onShowCabTypeList() {
        mcabTypeParentLayout.setVisibility(View.VISIBLE);
        mCabSelectionListener.onSetCabSelectionBack();
        mDialogBgView.setClickable(true);
        mDialogBgView.setOnClickListener(this);
        mDialogBgView.animate()
                .alpha(1)
                .setDuration(200)
                .setInterpolator(new DecelerateInterpolator());
    }

    @Override
    public void onSuccessApi(ApiStatusDto apiStatusDto) {
        if (apiStatusDto.isResultOk()) {
            mCabTypeList = CodeSnippet.getArrayFromMap(apiStatusDto.getData(),
                    CabType.class);
            if(mCabTypeList.size() > 1){
                mCommonAdapter = new CommonAdapter<>(getContext(), R.layout.inflater_cab_selection_row,
                        mCabTypeList, this);
                mCabTypeListView.setAdapter(mCommonAdapter);

                onShowCabTypeList();
            }
        } else {
            showMessage(apiStatusDto.getDescription());

        }
        dismissProgressbar();
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        showMessage(errorMessage);
        dismissProgressbar();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, CabType object, int viewId) {
        Context context = getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        if (convertView == null) {
            convertView = layoutInflater.inflate(viewId, parent, false);
        }

        ImageView cabImg = ((ImageView) convertView.findViewById(R.id.cab_iv));
        TextView cabText = ((TextView) convertView.findViewById(R.id.cab_tv));

        if(object.getCode().equals(getString(R.string.short_co))){
            cabText.setText(getString(R.string.co));
            cabImg.setImageResource(R.drawable.ic_micro_select);
        }else if(object.getCode().equals(getString(R.string.short_se))){
            cabText.setText(getString(R.string.se));
            cabImg.setImageResource(R.drawable.ic_sedan_select);

        }else if(object.getCode().equals(getString(R.string.short_sp))){
            cabText.setText(getString(R.string.sp));
            cabImg.setImageResource(R.drawable.ic_suv_select);
        }

        return convertView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_my_home_dialog_bg_view:
                closeCarTypesParentView();
                break;
        }
    }

    public interface CabSelectionListener {

        void onTypeChangeSuccess(String currentCarType);

        void onSetCabSelectionBack();
    }
}
