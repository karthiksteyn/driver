package com.josh.driver.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.josh.driver.R;
import com.josh.driver.handler.MediaProcessor;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Nandakumar on 10/11/16.
 */
public class MenuHeaderView extends RelativeLayout {

    public MenuHeaderView(Context context) {
        super(context);
    }

    public MenuHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MenuHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setData(String name) {
        ((TextView) findViewById(R.id.inflater_menu_lv_header_username_tv)).setText(name);
    }

//    public void setData(ProfileDto profileDto) {
//        final Context context = getContext();
//        final int size = getResources().getDimensionPixelSize(R.dimen.dp_50);
//        String imageKey = profileDto.getImageKey();
//        if (imageKey != null) {
//            Glide.with(context).load(new MediaProcessor().getProfilePicUrl(imageKey)).override(size, size)
//                    .signature(new StringSignature(profileDto.getProfilePicUpdatedTime() + System.currentTimeMillis() + ""))
//                    .bitmapTransform(new CropCircleTransformation(context)).placeholder(R.drawable.walkthrough_person)
//                    .error(R.drawable.walkthrough_person)
//                    .into((ImageView) findViewById(R.id.inflater_menu_lv_header_profile_pic_iv));
//        }
//        ((TextView) findViewById(R.id.inflater_menu_lv_header_username_tv)).setText(profileDto.getFullName());
//
//    }
}
