package com.josh.driver.handler;

/**
 * Created by sugan on 14/03/17.
 */
public enum Env {
    LIVE, STAGE, DEV, TEST
}
