package com.josh.driver.handler;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import com.josh.driver.dto.ContactDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
 * Created by Sri on 13-Sep-16.
 */
public class ContactsHandler {
    private static final String TAG = "ContactsHandler";
    private static ContactsHandler sContactsHandler;
    public static ContactsHandler getInstance() {
        if (sContactsHandler == null) {
            sContactsHandler = new ContactsHandler();
        }
        return sContactsHandler;
    }
    public List<ContactDto> getPhoneContactList(Context context) {
        List<ContactDto> contactList = new ArrayList<>();
        HashMap<String, String> savedContactsMap = new HashMap<>();
        HashMap<String, ContactDto> nonAppUserDtoHashMap = new HashMap<>();
        String phoneNumber;
        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if (cursor != null) {
            while (cursor.moveToNext()) {
                phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                        .replaceAll("[^0-9+]", "");
                String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                if (name.equals(phoneNumber)) {
                    name = "UNKNOWN";
                }
                if (!savedContactsMap.containsKey(phoneNumber)) {
                    Log.d(TAG, name + " = " + contactId + " = " + phoneNumber);
                    ContactDto nonAppUserDto = new ContactDto(contactId, phoneNumber, name);
                    savedContactsMap.put(phoneNumber, name);
                    nonAppUserDtoHashMap.put(contactId, nonAppUserDto);
                    contactList.add(nonAppUserDto);
                }
            }
            cursor.close();
        }
        return contactList;
    }
}