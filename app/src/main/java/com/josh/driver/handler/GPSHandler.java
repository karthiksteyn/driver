package com.josh.driver.handler;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

/**
 * Created by Sugan on 14/3/17.
 */
public class GPSHandler implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public final static int REQUEST_LOCATION = 101;
    public static int DEFAULT = -1;
    private LocationRequest mLocationRequest;
    private boolean mContinuousUpdatesEnabled = false;
    private GoogleApiClient mGoogleApiClient = null;
    private ConnectionListener mConnectionListener;
    private Context context;
    private int mInterval = 1000;
    private int mFastestInterval = 5000;
    private int mPriority = LocationRequest.PRIORITY_HIGH_ACCURACY;

    public GPSHandler(Context context, ConnectionListener mConnectionListener) {
        this.context = context;
        this.mConnectionListener = mConnectionListener;
        build();
    }


    public void connect() {
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        } else {
            mConnectionListener.onGetLastKnownLocation(null, null);
        }
    }

    public void disconnect() {
        mGoogleApiClient.disconnect();
    }

    public boolean isConnecting() {
        return mGoogleApiClient.isConnecting();
    }

    public boolean isConnected() {
        return mGoogleApiClient.isConnected();
    }

    public Context getContext() {
        return mGoogleApiClient.getContext();
    }


    private void build() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public boolean isNetworkEnabled(boolean onlyGps) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (onlyGps) {
            return gps_enabled;
        } else {
            return gps_enabled || network_enabled;
        }
    }

    public void startContinuousLocationListening(int interval, int fastestInterval, int priority,
                                                 int expirationDuration) {
        if (interval != DEFAULT)
            this.mInterval = interval;
        if (fastestInterval != DEFAULT)
            this.mFastestInterval = fastestInterval;
//        if (priority != DEFAULT)
//            this.mPriority = priority;
        if (mLocationRequest != null) {
            stopLocationUpdates();
        }
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(this.mInterval);
        mLocationRequest.setFastestInterval(this.mFastestInterval);
        mLocationRequest.setPriority(this.mPriority);
        if (expirationDuration > 0) {
            mLocationRequest.setExpirationDuration(expirationDuration);
        }
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            if (context instanceof Activity) {
                                status.startResolutionForResult((Activity) context
                                        , REQUEST_LOCATION);
                            }
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                }
            }
        });
        if (mContinuousUpdatesEnabled) {
            startLocationUpdates();
        } else {
            mContinuousUpdatesEnabled = true;
            if (mGoogleApiClient.isConnected()) {
                startLocationUpdates();
            }
        }
    }

    public void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    public void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected() && mContinuousUpdatesEnabled) {
            mContinuousUpdatesEnabled = false;
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    public void onPause() {
        stopLocationUpdates();
    }

    public void onResume() {
        if (mGoogleApiClient.isConnected() && mContinuousUpdatesEnabled) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mConnectionListener.onGetLastKnownLocation(bundle, mLastLocation);
        if (mContinuousUpdatesEnabled) {
            startLocationUpdates();
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        mConnectionListener.onConnectionSuspended(i);
    }

    @Override
    public void onLocationChanged(Location location) {
        mConnectionListener.onLocationChanged(location);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mConnectionListener.onConnectionFailed(connectionResult);
    }


    /**
     * <p>
     * This is an interface which implements onConnected, onConnectionSuspended, onConnectionFailed, onLocationChanged
     * </p>
     */
    public interface ConnectionListener {

        void onGetLastKnownLocation(Bundle bundle, Location location);

        void onConnectionSuspended(int i);

        void onConnectionFailed(ConnectionResult connectionResult);

        void onLocationChanged(Location location);
    }


}
