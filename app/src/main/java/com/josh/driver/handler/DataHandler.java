package com.josh.driver.handler;


import android.content.SharedPreferences;
import android.location.Location;

import com.hm.utils.CodeSnippet;
import com.josh.driver.Constants;

import com.hm.HmLibrary;
import com.josh.driver.JoshDriver;
import com.josh.driver.dto.ProfileDto;
import com.josh.driver.dto.UserDto;
import com.josh.driver.dto.request.LoginRequestDto;
import com.josh.driver.dto.request.UpdateDriverLocRequest;
import com.josh.driver.dto.response.AppDataResponseDto;
import com.josh.driver.dto.response.BalanceSummaryResponseDto;
import com.josh.driver.dto.response.LoginResponseDto;
import com.josh.driver.dto.response.TripDetailsResponseDto;

public class DataHandler implements Constants.SharedKey {

    private static DataHandler sDataHandler;

    private HmLibrary mHmLibrary;
    private SharedPreferences mSharedPreferences;

    public DataHandler() {
        mHmLibrary = HmLibrary.getHmLibrary();
    }

    public static DataHandler getInstance() {
        if (sDataHandler == null) {
            sDataHandler = new DataHandler();
        }
        return sDataHandler;
    }

    public void saveDisplayName(String displayName) {
        mHmLibrary.setSharedStringData(DISPLAY_NAME, displayName);
    }

    public String retrieveDisplayName() {
        return mHmLibrary.getSharedStringData(DISPLAY_NAME);
    }

    public void saveDisplayPic(String displayPic) {
        mHmLibrary.setSharedStringData(DISPLAY_PIC, displayPic);
    }

    public String retrieveDisplayPic() {
        return mHmLibrary.getSharedStringData(DISPLAY_PIC);
    }

    public void saveAuthToken(String authToken) {
        mHmLibrary.setSharedStringData(AUTH_TOKEN, authToken);
    }

    public String retrieveAuthToken() {
        return mHmLibrary.getSharedStringData(AUTH_TOKEN);
    }

    public void saveUserToken(String userToken) {
        mHmLibrary.setSharedStringData(USER_TOKEN, userToken);
    }

    public String retrieveUserToken() {
        return mHmLibrary.getSharedStringData(USER_TOKEN);
    }

    public void saveDriverId(String driverId) {
        mHmLibrary.setSharedStringData(DRIVER_ID, driverId);
    }

    public String retrieveDriverId() {
        return mHmLibrary.getSharedStringData(DRIVER_ID);
    }

    public void saveForcedPwdChange(String forcedPwdChange) {
        mHmLibrary.setSharedStringData(FORCED_PWD_CHANGE, forcedPwdChange);
    }

    public String retrieveForcePwdChange() {
        return mHmLibrary.getSharedStringData(FORCED_PWD_CHANGE);
    }

    public void saveDefaultType(String defaultType) {
        mHmLibrary.setSharedStringData(DEFAULT_TYPE, defaultType);
    }

    public String retrieveDefaultType() {
        return mHmLibrary.getSharedStringData(DEFAULT_TYPE);
    }

    public void saveCurrentType(String currentType) {
        mHmLibrary.setSharedStringData(CURRENT_TYPE, currentType);
    }

    public String retrieveCurrentType() {
        return mHmLibrary.getSharedStringData(CURRENT_TYPE);
    }

    public String retrieveAppKey() {
        return mHmLibrary.getSharedStringData(APP_KEY);
    }

    public void saveAppData(AppDataResponseDto appDataResponseDto) {
        String appKey = retrieveAppKey();
        if (appKey == null || !appKey.equals(appDataResponseDto.getAppKey())) {
            mHmLibrary.setSharedStringData(APP_KEY, appDataResponseDto.getAppKey());

        }
    }

    public void saveProfileDetails(UserDto userDto, long profileUpdatedTime) {
        //Save profile details

    }


    public ProfileDto retrieveProfileDetails() {
        ProfileDto profileDto = new ProfileDto();
        //update ProfileDto from saved details
        profileDto.setPhone(retrieveDriverPhone());
        profileDto.setName(retrieveDisplayName());
        profileDto.setDefaultType(retrieveDefaultType());
        profileDto.setSelectedType(retrieveCurrentType());
        profileDto.setVehicle(retrieveVehicleDetails());
        return profileDto;
    }

    public void saveProfileCreated(boolean created) {
        mHmLibrary.setSharedBooleanData(PROFILE_CREATED, created);
    }

    public boolean retrieveProfileCreated() {
        return mHmLibrary.getSharedBooleanData(PROFILE_CREATED);
    }

    public void saveDriverPhone(String mobile) {
        mHmLibrary.setSharedStringData(DRIVER_PHONE, mobile);
    }

    public String retrieveDriverPhone() {
        return mHmLibrary.getSharedStringData(DRIVER_PHONE);
    }

    public void saveOtp(String otp) {
        mHmLibrary.setSharedStringData(OTP, otp);
    }

    public String retrieveOtp() {
        return mHmLibrary.getSharedStringData(OTP);
    }

    public void saveBalance(float balance) {
        mHmLibrary.setSharedFloatData(BALANCE, balance);
    }

    public float retrieveBalance() {
        return mHmLibrary.getSharedFloatData(BALANCE);
    }

    public void saveMileage(float mileage) {
        mHmLibrary.setSharedFloatData(MILEAGE, mileage);
    }

    public float retrieveMileage() {
        return mHmLibrary.getSharedFloatData(MILEAGE);
    }

    public void saveOnlineDuration(float onlineDuration) {
        mHmLibrary.setSharedFloatData(ONLINE_DURATION, onlineDuration);
    }

    public float retrieveOnlineDuration() {
        return mHmLibrary.getSharedFloatData(ONLINE_DURATION);
    }

    public void saveRevenue(float revenue) {
        mHmLibrary.setSharedFloatData(REVENUE, revenue);
    }

    public float retrieveRevenue() {
        return mHmLibrary.getSharedFloatData(REVENUE);
    }

    public void saveTotalRides(int totalRides) {
        mHmLibrary.setSharedIntData(TOTAL_RIDES, totalRides);
    }

    public int retrieveTotalRides() {
        return mHmLibrary.getSharedIntData(TOTAL_RIDES);
    }

    public void saveDutyStatus(int dutyStatus) {
        mHmLibrary.setSharedIntData(DUTY_STATUS, dutyStatus);
    }

    public int retrieveDutyStatus() {
        return mHmLibrary.getSharedIntData(DUTY_STATUS);
    }

    public void saveVehicleDetails(String veicleDetails) {
        mHmLibrary.setSharedStringData(VEHICLE_DETAILS, veicleDetails);
    }

    public String retrieveVehicleDetails() {
        return mHmLibrary.getSharedStringData(VEHICLE_DETAILS);
    }

    public void saveBookingId(String bookingId) {
        mHmLibrary.setSharedStringData(ACTIVE_BOOKING_ID, bookingId);
    }

    public String retrieveBookingId() {
        return mHmLibrary.getSharedStringData(ACTIVE_BOOKING_ID);
    }

    public void saveTripDetails(TripDetailsResponseDto tripDetailsResponseDto) {

        if(tripDetailsResponseDto != null) {
            mHmLibrary.setSharedStringData(TRIP_DETAILS,
                    CodeSnippet.getJsonStringFromObject(tripDetailsResponseDto));
        }else{
            mHmLibrary.setSharedStringData(TRIP_DETAILS, null);
            saveBookingId(null);
        }

    }

    public TripDetailsResponseDto retrieveTripDetails() {

        String tripDetails = mHmLibrary.getSharedStringData(TRIP_DETAILS);
        if (tripDetails != null) {
            return CodeSnippet.getObjectFromJson(tripDetails, TripDetailsResponseDto.class);
        }
        return null;
    }

    public void saveLastKnownLocation(UpdateDriverLocRequest updateDriverLocRequest) {
        if(updateDriverLocRequest != null) {
            mHmLibrary.setSharedStringData(LAST_KNOWN_LOCATION,
                    CodeSnippet.getJsonStringFromObject(updateDriverLocRequest));
        }
    }

    public UpdateDriverLocRequest retrieveLastKnownLocation() {
        String location = mHmLibrary.getSharedStringData(LAST_KNOWN_LOCATION);
        if (location != null) {
            return CodeSnippet.getObjectFromJson(location, UpdateDriverLocRequest.class);
        }
        return null;
    }

    public void setServerTime(String serverTime) {
        JoshDriver.getInstance().setServerTime(serverTime);
    }

    public long getServerTime() {
        return JoshDriver.getInstance().getServerTime();
    }

    public void saveUserData(LoginResponseDto loginResponseDto, LoginRequestDto loginRequestDto) {
        saveDisplayName(loginResponseDto.getDisplayname());
        saveDisplayPic(loginResponseDto.getProfileimage());
        saveAuthToken(loginResponseDto.getXaccesstoken());
        saveUserToken(loginResponseDto.getXusertoken());
        saveDriverId(loginResponseDto.getDriverid());
        saveDefaultType(loginResponseDto.getDefaulttype());
        saveCurrentType(loginResponseDto.getCurrenttype());
        saveDutyStatus(loginResponseDto.getDutystatus());
        saveVehicleDetails(loginResponseDto.getVehicledetails());
        if(loginResponseDto.getActivebookingid() != null){
            saveBookingId(loginResponseDto.getActivebookingid());
        }
        saveProfileCreated(true);
        saveDriverPhone(loginRequestDto.getMobile());

    }

    public void saveTransactionData(BalanceSummaryResponseDto balanceSummaryResponseDto) {
        saveBalance(balanceSummaryResponseDto.getBalance() != 0 ? balanceSummaryResponseDto.getBalance() : 0.0f);
        saveMileage(balanceSummaryResponseDto.getMileage() != 0 ? balanceSummaryResponseDto.getMileage() : 0.0f);
        saveOnlineDuration(balanceSummaryResponseDto.getOnlineduration() != 0  ? balanceSummaryResponseDto.getOnlineduration() : 0.0f);
        saveRevenue(balanceSummaryResponseDto.getRevenue() != 0  ? balanceSummaryResponseDto.getRevenue() : 0.0f);
        saveTotalRides(balanceSummaryResponseDto.getTotalrides());
    }


    public void onClearAllData() {
        HmLibrary.getHmLibrary().clearAllData();
    }
}
