package com.josh.driver.handler;

import android.os.Environment;

import com.josh.driver.Constants;

import java.io.File;

/**
 * Created by Nandakumar on 2/6/16.
 */
public class MediaProcessor {

    private static final String TEMP_PIC_PATH = "temp_pic";
    private static final String JPG = ".jpg";

    private static File getAppExternalStorageDirectory() {
        File appFolder = new File(Environment.getExternalStorageDirectory(), Constants.AppKey.EXTERNAL_FOLDER_NAME);
        if (!appFolder.exists()) {
            appFolder.mkdirs();
        }
        return appFolder;
    }

    public String getProfilePicUrl(String key) {
        return Url.getFactory().getUrl(UrlId.CDN) + "/" + key + "?" +
                Constants.ServerKey.AUTHORIZATION + "=" + DataHandler.getInstance().retrieveAuthToken()
                + "&&" + Constants.ServerKey.SOURCE_TYPE + "=" + Constants.ServerKey.PROFILE_PIC + "&dimension=250x250";

    }

    public static String getTempPicPath() {
        return new File(getAppExternalStorageDirectory(), TEMP_PIC_PATH + JPG).getPath();
    }


}
