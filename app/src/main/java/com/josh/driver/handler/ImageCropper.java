package com.josh.driver.handler;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.josh.driver.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nandakumar on 19/11/14.
 */
public class ImageCropper {

    private static final String TAG = "ImageCropper";
    private Context mContext;
    private Uri mImageUri;
    private OnGetCropIntentListener mOnGetCropIntentListener;

    public ImageCropper(Context context, Uri imageUri, OnGetCropIntentListener onGetCropIntentListener) {
        mContext = context;
        mImageUri = imageUri;
        mOnGetCropIntentListener = onGetCropIntentListener;
    }

    public void createCropIntent(final Uri tempImageUri) {
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
        /**
         * Open image crop app by starting an intent
         * ‘com.android.camera.action.CROP‘.
         */
        final Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        /**
         * Check if there is image cropper app installed.
         */
        List<ResolveInfo> list = mContext.getPackageManager().queryIntentActivities(
                intent, 0);

        int size = list.size();

        /**
         * If there is no image cropper app, display warning message
         */
        if (size == 0) {

            Toast.makeText(mContext, "Can not find image crop app",
                    Toast.LENGTH_SHORT).show();

        } else {
            /**
             * Specify the image path, crop dimension and scale
             */
            intent.setData(tempImageUri);

            intent.putExtra("outputX", 300);
            intent.putExtra("outputY", 300);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", false);
//            File tempImg = new File(mImageUri.getPath());
//            if (tempImg.exists()) {
//                tempImg.delete();
//            }
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageUri);
            /**
             * There is posibility when more than one image cropper app exist,
             * so we have to check for it first. If there is only one app, open
             * then app.
             */

            if (size > 0) {
                Intent cropIntent = new Intent(intent);
                ResolveInfo res = list.get(0);

                cropIntent.setComponent(new ComponentName(res.activityInfo.packageName,
                        res.activityInfo.name));
                mOnGetCropIntentListener.onCropIntentCreated(cropIntent);
            } else {
                mOnGetCropIntentListener.onNoImageCropper();
            }
           /* if (size == 1) {
                Intent cropIntent = new Intent(intent);
                ResolveInfo res = list.get(0);

                cropIntent.setComponent(new ComponentName(res.activityInfo.packageName,
                        res.activityInfo.name));
                mOnGetCropIntentListener.onCropIntentCreated(cropIntent);
            } else {
                *//**
             * If there are several app exist, create a custom chooser to
             * let user selects the app.
             *//*
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();

                    co.title = mContext.getPackageManager().getApplicationLabel(
                            res.activityInfo.applicationInfo);
                    co.icon = mContext.getPackageManager().getApplicationIcon(
                            res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);

                    co.appIntent
                            .setComponent(new ComponentName(
                                    res.activityInfo.packageName,
                                    res.activityInfo.name));

                    cropOptions.add(co);
                }

                CropOptionAdapter adapter = new CropOptionAdapter(
                        mContext.getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                mOnGetCropIntentListener.onCropIntentCreated(cropOptions.get(item).appIntent);
                            }
                        });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancelClick(DialogInterface dialog) {

                        if (tempImageUri != null) {
                            File file = new File(tempImageUri.getPath());
                            file.delete();

//                            mContext.getContentResolver().delete(tempImageUri, null,
//                                    null);
                        }
                    }
                });

                AlertDialog alert = builder.create();

                alert.show();
            }*/
        }
    }

    public interface OnGetCropIntentListener {

        void onCropIntentCreated(Intent intent);

        void onNoImageCropper();

    }

    public class CropOptionAdapter extends ArrayAdapter<CropOption> {
        private ArrayList<CropOption> mOptions;
        private LayoutInflater mInflater;

        public CropOptionAdapter(Context context, ArrayList<CropOption> options) {
            super(context, R.layout.inflater_crop_selector, options);

            mOptions = options;

            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup group) {
            if (convertView == null)
                convertView = mInflater.inflate(R.layout.inflater_crop_selector, null);

            CropOption item = mOptions.get(position);

            if (item != null) {

                ((ImageView) convertView.findViewById(R.id.iv_icon))
                        .setImageDrawable(item.icon);
                ((TextView) convertView.findViewById(R.id.tv_name))
                        .setText(item.title);

                return convertView;
            }

            return null;
        }
    }

    public class CropOption {
        public CharSequence title;
        public Drawable icon;
        public Intent appIntent;
    }
}
