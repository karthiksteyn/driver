package com.josh.driver.handler;


import com.josh.driver.Constants;

/**
 * Created by Nandakumar on 9/29/16.
 */
public class ValidationHandler {

    private String mMessage;

    public boolean validateName(Constants.ValidationKey validationKey, String name) {
        if (name.isEmpty()) {
            assignMessage(validationKey, true);
            return false;
        } else {
            assignMessage(validationKey, false);
            if (name.length() < 3) {
                return false;
            }
        }
        return true;
    }


    public boolean validateEmail(String email) {
        if(email.isEmpty()){
            assignMessage(Constants.ValidationKey.EMAIL, true);
            return false;
        }else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            assignMessage(Constants.ValidationKey.EMAIL, false);
            return false;
        }
        return true;
    }

    private void assignMessage(Constants.ValidationKey validationKey, boolean empty) {
        switch (validationKey) {

            case EMAIL:
                if (empty) {
                    mMessage = Constants.Message.EMAIL_EMPTY;
                } else {
                    mMessage = Constants.Message.EMAIL_VALIDATION_FAILED;
                }
                break;
            case NAME:
                if (empty) {
                    mMessage = Constants.Message.NAME_EMPTY;
                } else {
                    mMessage = Constants.Message.NAME_VALIDATION_FAILED;
                }
                break;
        }
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }


}
