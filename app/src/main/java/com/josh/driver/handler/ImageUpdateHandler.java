package com.josh.driver.handler;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import com.hm.Log;
import com.hm.activity.BaseActivity;
import com.josh.driver.Constants;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Nandakumar on 5/11/15.
 */
public class ImageUpdateHandler implements ImageCropper.OnGetCropIntentListener {

    private static final String TAG = "ImageUpdateHandler";

    private static ImageUpdateHandler sImageUpdateHandler;
    public static final int PICK_IMAGE_REQUEST_CODE = 11;
    public static final int CROP_IMAGE_REQUEST_CODE = 12;
    public static final int UCROP_IMAGE_REQUEST_CODE = 69;
    private boolean mStartFromActivity;
    private static Activity mActivity;
    private Fragment mFragment;
    private Context mContext;
    private ImageUpdateHandleListener mImageUpdateHandleListener;
    private ImageCompressListener mImageCompressListener;

    private Uri mImageLocation;
    private ImageType mImageType;

    private ImageUpdateHandler() {

    }

    public enum ImageType {
        TEMP_IMAGE,
        PROFILE_PIC,
    }

    public static final String PROFILE_PIC_IMAGE_NAME = "profile_pic.jpg";
    public static final String PROFILE_PIC_TEMP_IMAGE_NAME = "temp_profile_pic.jpg";
    private static final int IMAGE_WIDTH = 1280;
    private static final int IMAGE_HEIGHT = 1280;


    public static ImageUpdateHandler getInstance() {
        if (sImageUpdateHandler == null) {
            sImageUpdateHandler = new ImageUpdateHandler();
        }
        return sImageUpdateHandler;
    }

    public static ImageUpdateHandler getInstance(Activity activity, ImageUpdateHandleListener imageUpdateHandleListener, boolean savedState, Context context) {
        if (sImageUpdateHandler == null) {
            sImageUpdateHandler = new ImageUpdateHandler();
        }
        sImageUpdateHandler.mContext = context;
        sImageUpdateHandler.mActivity = activity;
        sImageUpdateHandler.mStartFromActivity = true;
        sImageUpdateHandler.mImageUpdateHandleListener = imageUpdateHandleListener;
        sImageUpdateHandler.setupImageHandler(savedState);
        return sImageUpdateHandler;
    }

    public static ImageUpdateHandler getInstance(Fragment fragment, ImageUpdateHandleListener imageUpdateHandleListener, boolean savedState, Context context) {
        if (sImageUpdateHandler == null) {
            sImageUpdateHandler = new ImageUpdateHandler();
        }
        sImageUpdateHandler.mContext = context;
        sImageUpdateHandler.mFragment = fragment;
        sImageUpdateHandler.mActivity = fragment.getActivity();
        sImageUpdateHandler.mImageUpdateHandleListener = imageUpdateHandleListener;
        sImageUpdateHandler.setupImageHandler(savedState);
        return sImageUpdateHandler;
    }

    public ImageUpdateHandler(ImageCompressListener listener) {
        mImageCompressListener = listener;
    }

    public void setupImageHandler(boolean savedState) {
        if (!savedState) {
            File file = new File(getImageUri(ImageType.PROFILE_PIC).getPath());
            if (file.exists()) {
                file.delete();
            }
        }
    }

    public Bundle onSaveInstance(Bundle bundle) {
        if (mImageType != null) {
            bundle.putSerializable(Constants.BundleKey.IMAGE_HANDLER_IMAGE_TYPE, mImageType);
        }
        return bundle;
    }

    public void onRestoreInstance(Bundle bundle) {
        mImageType = (ImageType) bundle.get(Constants.BundleKey.IMAGE_HANDLER_IMAGE_TYPE);
        String imageLocationPath = bundle.getString(Constants.BundleKey.IMAGE_HANDLER_IMAGE_LOCATION);
        if (imageLocationPath != null) {
            mImageLocation = Uri.parse(imageLocationPath);
        }
    }


    public void onTakeProfilePic() {
        mImageType = ImageType.PROFILE_PIC;
        initImageSelector(getImageUri(ImageType.TEMP_IMAGE));
    }

    private boolean canProcess() {
        return mActivity != null && !mActivity.isFinishing();
    }

    private void initImageSelector(Uri tempImageUri) {
        Intent chooserIntent = Intent.createChooser(getGalleryIntent(), "Select an action");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                getCameraIntents(tempImageUri)
                        .toArray(new Parcelable[]{}));
        if (mStartFromActivity) {
            mActivity.startActivityForResult(chooserIntent, PICK_IMAGE_REQUEST_CODE);
        } else {
            mFragment.startActivityForResult(chooserIntent, PICK_IMAGE_REQUEST_CODE);
        }
    }

    private void openCamera(Uri tempImageUri) {
        Intent cameraImtent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraImtent.putExtra(MediaStore.EXTRA_OUTPUT, tempImageUri);

        if (mStartFromActivity) {
            mActivity.startActivityForResult(cameraImtent, PICK_IMAGE_REQUEST_CODE);
        } else {
            mFragment.startActivityForResult(cameraImtent, PICK_IMAGE_REQUEST_CODE);
        }
    }

    private Intent getGalleryIntent() {
        return new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    }

    private List<Intent> getCameraIntents(Uri cameraFileUri) {
        List<Intent> cameraIntents = new ArrayList<>();
        Intent targetIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        targetIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraFileUri);
        cameraIntents.add(targetIntent);
        return cameraIntents;
    }

    public Uri getImageUri(ImageType imageType) {
        Uri imageUri = null;
        switch (imageType) {
            case TEMP_IMAGE:
                imageUri = FileProvider.getUriForFile(mContext, Constants.AppKey.FILE_PROVIDER_AUTHORITY, new File(MediaProcessor.getTempPicPath()));
                Log.d(TAG, "Temp Uri = " + imageUri.getPath());
                break;
            case PROFILE_PIC:
                final String profileImageName = "Profile_Img_" + System.currentTimeMillis() + ".jpg";
                imageUri = getPhotoUri(profileImageName, "ProfileImages");

                mImageLocation = imageUri;
                break;
        }
        return imageUri;
    }

    private Uri getImageLocation() {
        return mImageLocation;
    }

    private Uri getPhotoUri(String fileName, String type) {
        Uri imageUri = null;
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), Constants.AppKey.EXTERNAL_FOLDER_NAME);
        File profilePicDir = new File(mediaStorageDir, "Profile Images");
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }

        if (!profilePicDir.exists()) {
            profilePicDir.mkdirs();
        }

        if (Objects.equals(type, "ProfileImages")) {
            imageUri = FileProvider.getUriForFile(mContext, Constants.AppKey.FILE_PROVIDER_AUTHORITY,
                    new File(profilePicDir.getPath() + File.separator + fileName));
            Log.d(TAG, "Profile pic Uri = " + imageUri.getPath());
        }
        return imageUri;
    }

    private void navigateToCropImage(Uri tempImageUri, Uri imageUri, int x, int y) {
        UCrop.of(tempImageUri, imageUri).start(mActivity);
    }

    @Override
    public void onCropIntentCreated(Intent intent) {
        if (mStartFromActivity) {
            mActivity.startActivityForResult(intent, CROP_IMAGE_REQUEST_CODE);
        } else {
            mFragment.startActivityForResult(intent, CROP_IMAGE_REQUEST_CODE);
        }
    }

    @Override
    public void onNoImageCropper() {
        fetchProfilePicBitmap(true, null);
    }


    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        Log.d(TAG, "onActivityResult: ");
        if (mImageUpdateHandleListener != null) {
            mImageUpdateHandleListener.checkHasPermission(new CheckPermissionListener() {
                @Override
                public void onPermissionGrant() {
                    switch (requestCode) {
                        case PICK_IMAGE_REQUEST_CODE:
                            onPickImageResult(resultCode, data);
                            break;
                        case UCROP_IMAGE_REQUEST_CODE:
                            onCropImageResult(resultCode, data);
                            break;
                    }
                }

                @Override
                public void onPermissionFailed() {
                    mImageUpdateHandleListener.onTakePicCancelled(Constants.PermissionMessages.CAMERA_DENIED);
                }
            });
        }
    }

    private void onPickImageResult(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (null == data) {
                handleImageResult(null);
            } else {
                Uri imageUri = data.getData();
                if (null != imageUri) {
                    handleImageResult(imageUri);
//                    checkForPermission(imageUri);
                } else {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                        handleImageResult(null);
                    } else {
                        ((BaseActivity) mActivity).showMessage("");
                        mImageUpdateHandleListener.onTakePicFailed();
                    }
                }
            }
        }
    }

    private void handleImageResult(Uri imageUri) {
        switch (mImageType) {
            case PROFILE_PIC:
                navigateToCropImage(imageUri == null ? getImageUri(ImageType.TEMP_IMAGE) : imageUri,
                        getImageUri(ImageType.PROFILE_PIC), 200, 200);
               /* shrinkImage(mImageType, imageUri == null ? getImageUri(ImageType.TEMP_IMAGE) : imageUri,
                        getImageUri(ImageType.PROFILE_PIC));*/
                break;
        }
    }

    public int getRotateAngle(ExifInterface exifInterface) {
        final int ORIENTATION_0 = 0;
        final int ORIENTATION_90 = 90;
        final int ORIENTATION_180 = 180;
        final int ORIENTATION_270 = 270;
        int orientation = exifInterface.
                getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        Log.i("orientation", String.valueOf(orientation));
        switch (orientation) {
            case ORIENTATION_0:
            case ExifInterface.ORIENTATION_NORMAL:
                return 0;
            case ORIENTATION_90:
            case ExifInterface.ORIENTATION_ROTATE_90:
                return 90;
            case ORIENTATION_180:
            case ExifInterface.ORIENTATION_ROTATE_180:
                return 180;
            case ORIENTATION_270:
            case ExifInterface.ORIENTATION_ROTATE_270:
                return 270;
        }
        return -1;
    }

    private void onCropImageResult(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            fetchProfilePicBitmap(false, data);
        } else {
//            mImageUpdateHandleListener.onTakePicCancelled();
        }
    }

    public void fetchProfilePicBitmap(boolean crop, Intent data) {
        if (canProcess()) {
            ImageType imageType = ImageType.PROFILE_PIC;
            Uri profilePicUri = getImageLocation();
//            Uri profilePicUri = data.getExtras().get("data");
            if (profilePicUri == null) {
                crop = true;
            }

            Bitmap bitmap = null;

            try {
                bitmap = MediaStore.Images
                        .Media.getBitmap(mActivity.getContentResolver(), profilePicUri);
            } catch (IOException e) {
                e.printStackTrace();
            }

//            bitmap.compress(Bitmap.CompressFormat.PNG, 80, );
            /*if (bitmap != null) {
                if (bitmap.getByteCount() > 7242880) {
                    crop = true;
                }
            }*/
//            Log.e("ImageUpdateHandler", "Bitmap byte " + bitmap.getByteCount());
            if (crop) {
                shrinkImage(mImageType, profilePicUri, getImageLocation(), 0, "");
            } else {
                try {
                    mImageUpdateHandleListener.onTakePicSuccess(profilePicUri, MediaStore.Images
                            .Media.getBitmap(mActivity.getContentResolver(), profilePicUri));
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                    mImageUpdateHandleListener.onTakePicSuccess(profilePicUri, MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), profilePicUri));
            }
        }
    }

    public void shrinkImage(final ImageType imageType, final Uri imageUri, final Uri saveFileUri, final int position, final String caption) {
//        mImageUpdateHandleListener.onProcessingImage("Processing Image");
        new AsyncTask<String, String, Bitmap>() {

            @Override
            protected Bitmap doInBackground(String... params) {
                Bitmap bitmap = null;
                try {
                    // Decode image size
                    BitmapFactory.Options o = new BitmapFactory.Options();
                    o.inJustDecodeBounds = true;
                    ContentResolver contentResolver = mActivity.getContentResolver();
                    BitmapFactory.decodeStream(contentResolver.openInputStream(imageUri),
                            null, o);
                    int outWidth = o.outWidth;
                    int outHeight = o.outHeight;
                    int requiredWidth = IMAGE_WIDTH;
                    int requiredHeight = IMAGE_HEIGHT;
                    // Find the correct scale value. It should be the power of 2.
                    int scale = 1;
                    while (outWidth / scale / 2 >= requiredWidth &&
                            outHeight / scale / 2 >= requiredHeight) {
                        scale *= 2;
                    }

                    // Decode with inSampleSize
                    BitmapFactory.Options o2 = new BitmapFactory.Options();
                    o2.inSampleSize = scale;
                    bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(imageUri)
                            , null, o2);
                    Matrix mat = new Matrix();
                    ExifInterface exif = new ExifInterface(imageUri.getPath());
                    String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
                    int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
                    int rotateAngle = 0;
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotateAngle = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotateAngle = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotateAngle = 90;
                            break;
                    }
                    mat.setRotate(rotateAngle, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mat, true);
                    int originalWidth = bitmap.getWidth();
                    int originalHeight = bitmap.getHeight();
                    float ratio = Math.min((float) requiredWidth / originalWidth,
                            (float) requiredHeight / originalHeight);
                    requiredWidth = Math.round(ratio * originalWidth);
                    requiredHeight = Math.round(ratio * originalHeight);

                    bitmap = Bitmap.createScaledBitmap(bitmap, requiredWidth, requiredHeight, true);
                    File idCardFile = new File(saveFileUri.getPath());
                    idCardFile.createNewFile();
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 20, bos);
                    byte[] bitmapData = bos.toByteArray();
                    FileOutputStream fos = new FileOutputStream(idCardFile);
                    fos.write(bitmapData);
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    if (mImageUpdateHandleListener != null) {
                        mImageUpdateHandleListener.onTakePicFailed();
                    } else {
                        mImageCompressListener.onImageCompressFailed(position);
                    }
                }
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap s) {
                super.onPostExecute(s);
                onShrinkDone(imageType, saveFileUri, s, position, caption);
            }
        }.execute();
    }

    public void saveBitmap(final String filePath, final Bitmap bitmap) {
        new AsyncTask<String, String, Bitmap>() {
            @Override
            protected Bitmap doInBackground(String... params) {
                try {
                    File idCardFile = new File(filePath);
                    idCardFile.createNewFile();
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 20, bos);
                    byte[] bitmapData = bos.toByteArray();
                    FileOutputStream fos = new FileOutputStream(idCardFile);
                    fos.write(bitmapData);
                    fos.flush();
                    fos.close();
                    return bitmap;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }


        }.execute();
    }

    private void onShrinkDone(final ImageType imageType, final Uri imageUri, final Bitmap bitmap, int position, String caption) {
        if (mImageCompressListener != null) {
            mImageCompressListener.onImageCompressSuccess(imageUri, bitmap, position, caption, imageUri);
        } else if (mImageUpdateHandleListener != null) {
            mImageUpdateHandleListener.onTakePicSuccess(imageUri, bitmap);
        }
//                break;
//    }
    }

    public interface ImageUpdateHandleListener {

        void onTakePicSuccess(Uri picUri, Bitmap bitmap);

        void checkHasPermission(CheckPermissionListener checkPermissionListener);

        void onTakePicFailed();

        void onTakePicCancelled(String message);

    }

    public interface CheckPermissionListener {
        void onPermissionGrant();

        void onPermissionFailed();
    }

    public interface ImageCompressListener {

        void onImageCompressSuccess(Uri picUri, Bitmap bitmap, int position, String caption, Uri imageUri);

        void onImageCompressFailed(int position);

    }

}