package com.josh.driver.handler;


import com.josh.driver.BuildConfig;

/**
 * Created by Sugan on 14/3/17.
 */
public class Url {

    private static Url urlFactory = new Url();

    private String apiBaseUrl;
    private String googleMapBaseUrl;
    private String socketBaseUrl;
    private String cdnBaseUrl;

    private Url() {
        if(Env.LIVE == BuildConfig.ENV){
            apiBaseUrl = "http://api.josh.cab/";
            socketBaseUrl = "http://api.josh.cab/cab_io";
            cdnBaseUrl = "";
        }else if(Env.STAGE == BuildConfig.ENV){
            apiBaseUrl = "";
            socketBaseUrl = "";
            cdnBaseUrl = "";
        }else if(Env.DEV == BuildConfig.ENV){
            apiBaseUrl = "";
            socketBaseUrl = "";
            cdnBaseUrl = "";
        }else{
            apiBaseUrl = "http://apidev.josh.cab/";
            googleMapBaseUrl = "https://maps.googleapis.com/";
            socketBaseUrl = "http://apidev.josh.cab/cab_io?driver=";
            cdnBaseUrl = "";
        }
    }

    public static Url getFactory() {
        return urlFactory;
    }


    public String getUrl(UrlId id) {
        switch (id) {
            case LOGIN:
                return "user/login";
            case APP_DATA:
                return "app_data/user";
            case SOCKET:
                return socketBaseUrl;
            case BASE:
                return apiBaseUrl;
            case GOOGLE_MAP_API:
                return googleMapBaseUrl;
            case CDN:
                return cdnBaseUrl;
            case VERIFICATION_CODE:
                return apiBaseUrl + "user/verify_otp";
            case RESEND_VERIFICATION_CODE:
                return apiBaseUrl + "user/resend_otp";
            case ADDRESS_LIST:
                return apiBaseUrl + "address/list";
            case ADD_ADDRESS:
                return apiBaseUrl + "address";
            case UPDATE_PROFILE:
                return apiBaseUrl + "user";
            case ORDER_DETAILS:
                return apiBaseUrl + "order";
            case GEOCODE:
                return "https://maps.googleapis.com/maps/api/geocode/json";
            case PRODUCT_ACCESSORY_LIST:
                return apiBaseUrl + "productAccessory/list";
            case PLACE_ORDER:
                return apiBaseUrl + "order/place_order";
            case CANCEL_ORDER:
                return apiBaseUrl + "order/cancel";
            case PAST_ORDERS:
                return apiBaseUrl + "order/history";
            case FEEDBACK:
                return apiBaseUrl + "feedback";
            case NOTIFICATION_LIST:
                return apiBaseUrl + "noti/list";
            case NOTIFICATION_CLEAR:
                return apiBaseUrl + "noti/clear";
            case COUPON_CODE:
                return apiBaseUrl + "coupon/apply";
            case ORDER_AREA:
                return apiBaseUrl + "order/area";
        }

        return null;
    }

    public String getApiBaseUrl() {
        return apiBaseUrl;
    }

    public void setApiBaseUrl(String apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
    }

}
