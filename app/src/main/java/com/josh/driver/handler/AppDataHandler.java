package com.josh.driver.handler;

import com.hm.utils.CodeSnippet;
import com.hm.utils.CustomDateFormats;
import com.josh.driver.BuildConfig;
import com.josh.driver.Constants;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.Headers;
import com.josh.driver.dto.request.AppDataRequestDto;
import com.josh.driver.dto.response.BalanceSummaryResponseDto;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Sugan on 15/3/17.
 */

public class AppDataHandler implements ModelHandler.ModelHanlderListener<ApiStatusDto>{

    private  AppDataHandlerCallBack mAppDataHandlerCallBack;

    public AppDataHandler(AppDataHandlerCallBack appDataHandlerCallBack) {
        mAppDataHandlerCallBack = appDataHandlerCallBack;
    }

    public void fetchAppData() {
        String appKey = DataHandler.getInstance().retrieveAppKey();
        AppDataRequestDto appDataRequestDto = new AppDataRequestDto(appKey != null ? appKey : "empty");
        appDataRequestDto.setVersionNo(BuildConfig.VERSION_CODE);
        appDataRequestDto.setMode(Constants.AppKey.MODE);

//        ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().getAppDataRequest(appDataRequestDto));

    }

    public void fetchTransactionDetails() {

        String driverId = DataHandler.getInstance().retrieveDriverId();
        Headers headers = getTokens();

        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String currentDate = CodeSnippet.getDateStringFromDate(date, CustomDateFormats.DD_MMM_YYYY);

        ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().
                getTransactionDataRequest(driverId, currentDate, headers));
    }

    private Headers getTokens() {
        Headers headers = new Headers();
        headers.setAuthToken(DataHandler.getInstance().retrieveAuthToken());
        headers.setUserToken(DataHandler.getInstance().retrieveUserToken());
        headers.setAppToken(Constants.AppKey.APP_TOKEN);
        return headers;
    }

    @Override
    public void onSuccessApi(ApiStatusDto apiStatusDto) {
        if (apiStatusDto.isResultOk()) {
            BalanceSummaryResponseDto balanceSummaryResponseDto = CodeSnippet.getObjectFromMap(apiStatusDto.getData(),
                    BalanceSummaryResponseDto.class);

            DataHandler.getInstance().saveTransactionData(balanceSummaryResponseDto);
            mAppDataHandlerCallBack.onAppDatFetchSuccess();
        } else {
            mAppDataHandlerCallBack.onAppDataFetchFailed(apiStatusDto.getDescription());
        }
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mAppDataHandlerCallBack.onAppDataFetchFailed(errorMessage);

    }

    @Override
    public void onAuthorizationFailed() {

    }

    public void fetchBookingDetails(String bookingId) {
        String driverId = DataHandler.getInstance().retrieveDriverId();
        Headers headers = getTokens();

//        String bookingId = DataHandler.getInstance().retrieveBookingId();

        ModelHandler.getInstance().enqueueTask(new ModelHandler.ModelHanlderListener<ApiStatusDto>() {
            @Override
            public void onSuccessApi(ApiStatusDto response) {
                if (response.isResultOk()) {
                    TripDetailsResponseDto tripDetailsResponseDto = CodeSnippet.getObjectFromMap(response.getData(),
                            TripDetailsResponseDto.class);

                    DataHandler.getInstance().saveTripDetails(tripDetailsResponseDto);
                    mAppDataHandlerCallBack.onBookingDataFetchSuccess();
                } else {
                    mAppDataHandlerCallBack.onBookingDataFetchFailed(response.getDescription());
                }
            }

            @Override
            public void onFailureApi(int errorCode, String errorMessage) {
                mAppDataHandlerCallBack.onBookingDataFetchFailed(errorMessage);

            }

            @Override
            public void onAuthorizationFailed() {

            }
        }, RequestCreator.getInstance().
                getBookingDataRequest(driverId, bookingId, headers));

    }


    public interface AppDataHandlerCallBack {
        void onAppDatFetchSuccess();

        void onAuthorizationFailed();

        void onAppDataFetchFailed(String message);

        void onBookingDataFetchSuccess();

        void onBookingDataFetchFailed(String message);

        void onForceUpdate();
    }
}
