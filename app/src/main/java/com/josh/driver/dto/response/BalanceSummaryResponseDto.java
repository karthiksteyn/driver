package com.josh.driver.dto.response;


/**
 * Created by hmspl on 17/4/17.
 */

public class BalanceSummaryResponseDto {
    private float balance;
    private float mileage;
    private float onlineduration;
    private float revenue;
    private int totalrides;

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public float getMileage() {
        return mileage;
    }

    public void setMileage(float mileage) {
        this.mileage = mileage;
    }

    public float getOnlineduration() {
        return onlineduration;
    }

    public void setOnlineduration(float onlineduration) {
        this.onlineduration = onlineduration;
    }

    public float getRevenue() {
        return revenue;
    }

    public void setRevenue(float revenue) {
        this.revenue = revenue;
    }

    public int getTotalrides() {
        return totalrides;
    }

    public void setTotalrides(int totalrides) {
        this.totalrides = totalrides;
    }
}
