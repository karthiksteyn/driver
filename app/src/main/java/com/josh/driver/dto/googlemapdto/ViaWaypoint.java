package com.josh.driver.dto.googlemapdto;


import java.io.Serializable;

public class ViaWaypoint implements Serializable {

    private Location location;
    private Integer step_index;
    private Double step_interpolation;

    /**
     * @return The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return The step_index
     */
    public Integer getStep_index() {
        return step_index;
    }

    /**
     * @param step_index The step_index
     */
    public void setStep_index(Integer step_index) {
        this.step_index = step_index;
    }

    /**
     * @return The step_interpolation
     */
    public Double getStep_interpolation() {
        return step_interpolation;
    }

    /**
     * @param step_interpolation The step_interpolation
     */
    public void setStep_interpolation(Double step_interpolation) {
        this.step_interpolation = step_interpolation;
    }
}
