package com.josh.driver.dto.request;

import java.io.Serializable;

/**
 * Created by hmspl on 15/5/17.
 */

public class SendOtpRequestDto implements Serializable {
    private String mobile;

    public SendOtpRequestDto(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
