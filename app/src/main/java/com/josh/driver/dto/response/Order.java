package com.josh.driver.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by hmspl on 28/4/17.
 */

public class Order implements Serializable{
    @SerializedName("bookingid")
    @Expose
    private String bookingid;

    @SerializedName("pickupgeo")
    @Expose
    private List<Double> pickupgeo;

    @SerializedName("pickuploc")
    @Expose
    private String pickuploc;

    @SerializedName("eta")
    @Expose
    private String eta;

    @SerializedName("distance")
    @Expose
    private String distance;

    @SerializedName("currentgeo")
    @Expose
    private List<Double> currentgeo;

    @SerializedName("displaytime")
    @Expose
    private int displaytime;

    @SerializedName("status")
    @Expose
    private Boolean status;

    public String getBookingid() {
        return bookingid;
    }

    public void setBookingid(String bookingid) {
        this.bookingid = bookingid;
    }

    public List<Double> getPickupgeo() {
        return pickupgeo;
    }

    public void setPickupgeo(List<Double> pickupgeo) {
        this.pickupgeo = pickupgeo;
    }

    public String getPickuploc() {
        return pickuploc;
    }

    public void setPickuploc(String pickuploc) {
        this.pickuploc = pickuploc;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public List<Double> getCurrentgeo() {
        return currentgeo;
    }

    public void setCurrentgeo(List<Double> currentgeo) {
        this.currentgeo = currentgeo;
    }

    public int getDisplaytime() {
        return displaytime;
    }

    public void setDisplaytime(int displaytime) {
        this.displaytime = displaytime;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
