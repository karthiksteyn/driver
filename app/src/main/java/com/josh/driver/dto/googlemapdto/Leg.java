package com.josh.driver.dto.googlemapdto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Leg implements Serializable {

    private Distance distance;
    private Duration duration;
    private String end_address;
    private EndLocation end_location;
    private String start_address;
    private StartLocation start_location;
    private List<Step> steps = new ArrayList<Step>();
    private List<ViaWaypoint> via_waypoint = new ArrayList<ViaWaypoint>();

    /**
     * @return The distance
     */
    public Distance getDistance() {
        return distance;
    }

    /**
     * @param distance The distance
     */
    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    /**
     * @return The duration
     */
    public Duration getDuration() {
        return duration;
    }

    /**
     * @param duration The duration
     */
    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    /**
     * @return The end_address
     */
    public String getEnd_address() {
        return end_address;
    }

    /**
     * @param end_address The end_address
     */
    public void setEnd_address(String end_address) {
        this.end_address = end_address;
    }

    /**
     * @return The end_location
     */
    public EndLocation getEnd_location() {
        return end_location;
    }

    /**
     * @param end_location The end_location
     */
    public void setEnd_location(EndLocation end_location) {
        this.end_location = end_location;
    }

    /**
     * @return The start_address
     */
    public String getStart_address() {
        return start_address;
    }

    /**
     * @param start_address The start_address
     */
    public void setStart_address(String start_address) {
        this.start_address = start_address;
    }

    /**
     * @return The start_location
     */
    public StartLocation getStart_location() {
        return start_location;
    }

    /**
     * @param start_location The start_location
     */
    public void setStart_location(StartLocation start_location) {
        this.start_location = start_location;
    }

    /**
     * @return The steps
     */
    public List<Step> getSteps() {
        return steps;
    }

    /**
     * @param steps The steps
     */
    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    /**
     * @return The via_waypoint
     */
    public List<ViaWaypoint> getVia_waypoint() {
        return via_waypoint;
    }

    /**
     * @param via_waypoint The via_waypoint
     */
    public void setVia_waypoint(List<ViaWaypoint> via_waypoint) {
        this.via_waypoint = via_waypoint;
    }

}
