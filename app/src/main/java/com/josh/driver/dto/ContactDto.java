package com.josh.driver.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ContactDto implements Parcelable {
    private String id;
    private String mbNo;
    @JsonIgnore
    private String name;
    public ContactDto() {
    }
    public ContactDto(String id, String mbNo) {
        this.id = id;
        this.mbNo = mbNo;
    }
    public ContactDto(String id, String mbNo, String name) {
        this.id = id;
        this.mbNo = mbNo;
        this.name = name;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getMbNo() {
        return mbNo;
    }
    public void setMbNo(String mbNo) {
        this.mbNo = mbNo;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.mbNo);
        dest.writeString(this.name);
    }
    protected ContactDto(Parcel in) {
        this.id = in.readString();
        this.mbNo = in.readString();
        this.name = in.readString();
    }
    public static final Parcelable.Creator<ContactDto> CREATOR = new Parcelable.Creator<ContactDto>() {
        @Override
        public ContactDto createFromParcel(Parcel source) {
            return new ContactDto(source);
        }
        @Override
        public ContactDto[] newArray(int size) {
            return new ContactDto[size];
        }
    };
}
