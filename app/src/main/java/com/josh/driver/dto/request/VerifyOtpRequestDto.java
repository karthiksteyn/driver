package com.josh.driver.dto.request;

import java.io.Serializable;

/**
 * Created by hmspl on 15/5/17.
 */

public class VerifyOtpRequestDto implements Serializable {
    private String mobile;

    private String otp;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
