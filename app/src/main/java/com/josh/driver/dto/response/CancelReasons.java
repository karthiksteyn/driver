package com.josh.driver.dto.response;

import java.io.Serializable;

/**
 * Created by hmspl on 17/4/17.
 */

public class CancelReasons implements Serializable {
    private String code;
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
