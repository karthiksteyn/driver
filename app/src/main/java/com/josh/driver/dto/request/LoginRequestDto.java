package com.josh.driver.dto.request;

import java.io.Serializable;

/**
 * Created by hmspl on 16/3/17.
 */

public class LoginRequestDto implements Serializable {
    private String mobile;
    private String password;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
