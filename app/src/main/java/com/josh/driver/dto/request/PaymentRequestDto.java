package com.josh.driver.dto.request;

import java.io.Serializable;

/**
 * Created by Sugan on 31/3/17.
 */

public class PaymentRequestDto implements Serializable {
    private String name;
    private String phone;
    private String amt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }
}
