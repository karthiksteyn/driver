package com.josh.driver.dto.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hmspl on 14/3/17.
 */

public class LoginResponseDto {

    @SerializedName("displayname")
    @Expose
    private String displayname;
    @SerializedName("profileimage")
    @Expose
    private String profileimage;
    @SerializedName("xaccesstoken")
    @Expose
    private String xaccesstoken;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("driverid")
    @Expose
    private String driverid;
    @SerializedName("xusertoken")
    @Expose
    private String xusertoken;
    @SerializedName("forcedpasswordchange")
    @Expose
    private Boolean forcedpasswordchange;
    @SerializedName("defaulttype")
    @Expose
    private String defaulttype;
    @SerializedName("currenttype")
    @Expose
    private String currenttype;
    @SerializedName("activebookingid")
    @Expose
    private String activebookingid;
    @SerializedName("dutystatus")
    @Expose
    private int dutystatus;
    @SerializedName("vehicledetails")
    @Expose
    private String vehicledetails;


    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public String getXaccesstoken() {
        return xaccesstoken;
    }

    public void setXaccesstoken(String xaccesstoken) {
        this.xaccesstoken = xaccesstoken;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDriverid() {
        return driverid;
    }

    public void setDriverid(String driverid) {
        this.driverid = driverid;
    }

    public String getXusertoken() {
        return xusertoken;
    }

    public void setXusertoken(String xusertoken) {
        this.xusertoken = xusertoken;
    }

    public Boolean getForcedpasswordchange() {
        return forcedpasswordchange;
    }

    public void setForcedpasswordchange(Boolean forcedpasswordchange) {
        this.forcedpasswordchange = forcedpasswordchange;
    }

    public String getDefaulttype() {
        return defaulttype;
    }

    public void setDefaulttype(String defaulttype) {
        this.defaulttype = defaulttype;
    }

    public String getCurrenttype() {
        return currenttype;
    }

    public void setCurrenttype(String currenttype) {
        this.currenttype = currenttype;
    }

    public String getActivebookingid() {
        return activebookingid;
    }

    public void setActivebookingid(String activebookingid) {
        this.activebookingid = activebookingid;
    }

    public int getDutystatus() {
        return dutystatus;
    }

    public void setDutystatus(int dutystatus) {
        this.dutystatus = dutystatus;
    }

    public String getVehicledetails() {
        return vehicledetails;
    }

    public void setVehicledetails(String vehicledetails) {
        this.vehicledetails = vehicledetails;
    }
}
