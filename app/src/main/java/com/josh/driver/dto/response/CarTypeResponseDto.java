package com.josh.driver.dto.response;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by hmspl on 17/4/17.
 */

public class CarTypeResponseDto {
    private List<CabType> cabTypeList = new ArrayList<CabType>();

    public List<CabType> getCabTypeList() {
        return cabTypeList;
    }

    public void setCabTypeList(List<CabType> cabTypeList) {
        this.cabTypeList = cabTypeList;
    }
}
