package com.josh.driver.dto.response;

import java.io.Serializable;

/**
 * Created by hmspl on 17/4/17.
 */

public class CabType implements Serializable {
    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
