package com.josh.driver.dto.response;



/**
 * Created by hmspl on 17/3/17.
 */

public class AppDataResponseDto {
    private String appKey;
    private Boolean isForceUpdate;

    /**
     *
     * @return
     *     The appKey
     */
    public String getAppKey() {
        return appKey;
    }

    /**
     *
     * @param appKey
     *     The appKey
     */
    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public Boolean getIsForceUpdate() {
        return isForceUpdate;
    }

    public void setIsForceUpdate(Boolean isForceUpdate) {
        this.isForceUpdate = isForceUpdate;
    }

}
