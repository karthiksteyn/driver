package com.josh.driver.dto.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hmspl on 20/3/17.
 */

public class TripDetailsResponseDto {

    private String crn;

    private int customerrating;

    private Customer customer;

    private List<Integer> discount;

    private int driverrating;

    private int freeduration;

    private int freemileage;

    private String from;

    private float grossamount;

    private float kyamount;

    private float mileage;

    private float nettamount;

    private String rid;

    private float sbamount;

    private int status;

    private String to;

    private int type;

    private List<Double> toloc;

    private List<Double> fromloc;

    private float totalmileage;

    private float totalduration;

    private float totalridefare;

    private float tax;

    private float totaldiscount;

    private float nettbillingamount;

    private String starttime;

    private String endtime;

    private CabType cabtype;

    private int expectedduration;

    private String eta;

    public String getCrn() {
        return crn;
    }

    public void setCrn(String crn) {
        this.crn = crn;
    }

    public int getCustomerrating() {
        return customerrating;
    }

    public void setCustomerrating(int customerrating) {
        this.customerrating = customerrating;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Integer> getDiscount() {
        return discount;
    }

    public void setDiscount(List<Integer> discount) {
        this.discount = discount;
    }

    public int getDriverrating() {
        return driverrating;
    }

    public void setDriverrating(int driverrating) {
        this.driverrating = driverrating;
    }

    public int getFreeduration() {
        return freeduration;
    }

    public void setFreeduration(int freeduration) {
        this.freeduration = freeduration;
    }

    public int getFreemileage() {
        return freemileage;
    }

    public void setFreemileage(int freemileage) {
        this.freemileage = freemileage;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public float getGrossamount() {
        return grossamount;
    }

    public void setGrossamount(float grossamount) {
        this.grossamount = grossamount;
    }

    public float getKyamount() {
        return kyamount;
    }

    public void setKyamount(float kyamount) {
        this.kyamount = kyamount;
    }

    public float getMileage() {
        return mileage;
    }

    public void setMileage(float mileage) {
        this.mileage = mileage;
    }

    public float getNettamount() {
        return nettamount;
    }

    public void setNettamount(float nettamount) {
        this.nettamount = nettamount;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public float getSbamount() {
        return sbamount;
    }

    public void setSbamount(float sbamount) {
        this.sbamount = sbamount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Double> getToloc() {
        return toloc;
    }

    public void setToloc(List<Double> toloc) {
        this.toloc = toloc;
    }

    public List<Double> getFromloc() {
        return fromloc;
    }

    public void setFromloc(List<Double> fromloc) {
        this.fromloc = fromloc;
    }

    public float getTotalmileage() {
        return totalmileage;
    }

    public void setTotalmileage(float totalmileage) {
        this.totalmileage = totalmileage;
    }

    public float getTotalridefare() {
        return totalridefare;
    }

    public void setTotalridefare(float totalridefare) {
        this.totalridefare = totalridefare;
    }

    public float getTotalduration() {
        return totalduration;
    }

    public void setTotalduration(float totalduration) {
        this.totalduration = totalduration;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public float getTotaldiscount() {
        return totaldiscount;
    }

    public void setTotaldiscount(float totaldiscount) {
        this.totaldiscount = totaldiscount;
    }

    public float getNettbillingamount() {
        return nettbillingamount;
    }

    public void setNettbillingamount(float nettbillingamount) {
        this.nettbillingamount = nettbillingamount;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public CabType getCabtype() {
        return cabtype;
    }

    public void setCabtype(CabType cabtype) {
        this.cabtype = cabtype;
    }

    public int getExpectedduration() {
        return expectedduration;
    }

    public void setExpectedduration(int expectedduration) {
        this.expectedduration = expectedduration;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }
}
