package com.josh.driver.dto;

import com.josh.driver.dto.response.LoginResponseDto;

import java.io.Serializable;

/**
 * Created by hmspl on 17/3/17.
 */

public class ApiStatusDto implements Serializable {
    private static final String RESULT_OK = "200";
    private static final String RESULT_NOT_OK = "999";
    private String status;
    private String description;
    private String error;
    private boolean internalerror;
    private Object data;

    public ApiStatusDto() {
    }

    public boolean isResultOk() {
        return this.status.equals(RESULT_OK);
    }

    public boolean isResultNotOk() {
        return this.status.equals(RESULT_NOT_OK);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isInternalerror() {
        return internalerror;
    }

    public void setInternalerror(boolean internalerror) {
        this.internalerror = internalerror;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
