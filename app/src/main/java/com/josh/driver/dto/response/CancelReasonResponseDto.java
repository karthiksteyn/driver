package com.josh.driver.dto.response;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by hmspl on 17/4/17.
 */

public class CancelReasonResponseDto {
    List<CancelReasons> CancelReasonList = new ArrayList<CancelReasons>();

    public List<CancelReasons> getCancelReasonList() {
        return CancelReasonList;
    }

    public void setCancelReasonList(List<CancelReasons> cancelReasonList) {
        CancelReasonList = cancelReasonList;
    }
}
