package com.josh.driver.dto.request;

import java.io.Serializable;

/**
 * Created by hmspl on 17/4/17.
 */

public class TypeChangeRequestDto implements Serializable {
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
