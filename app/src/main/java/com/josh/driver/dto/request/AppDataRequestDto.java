package com.josh.driver.dto.request;

import java.io.Serializable;

/**
 * Created by hmspl on 17/3/17.
 */

public class AppDataRequestDto implements Serializable {
    private String appKey;

    private Integer versionNo;

    private String mode;

    public AppDataRequestDto(String appKey) {
        this.appKey = appKey;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public Integer getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(Integer versionNo) {
        this.versionNo = versionNo;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
