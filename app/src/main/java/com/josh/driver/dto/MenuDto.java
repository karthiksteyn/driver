package com.josh.driver.dto;

/**
 * Created by hmspl on 18/6/15.
 */
public class MenuDto {

    private String title;

    private int iconSelectedResId;

    public MenuDto(int iconSelectedResId, String title) {
        this.iconSelectedResId = iconSelectedResId;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIconSelectedResId() {
        return iconSelectedResId;
    }

    public void setIconSelectedResId(int iconSelectedResId) {
        this.iconSelectedResId = iconSelectedResId;
    }

}
