package com.josh.driver.dto.googlemapdto;

import java.io.Serializable;

public class Polyline implements Serializable {

    private String points;

    /**
     * @return The points
     */
    public String getPoints() {
        return points;
    }

    /**
     * @param points The points
     */
    public void setPoints(String points) {
        this.points = points;
    }

}
