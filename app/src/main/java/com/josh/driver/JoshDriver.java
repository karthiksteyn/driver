package com.josh.driver;

import android.app.Application;
import android.content.Intent;

import com.crashlytics.android.Crashlytics;
import com.hm.HmLibrary;
import com.hm.utils.CodeSnippet;
import com.hm.utils.CustomDateFormats;
import com.josh.driver.service.SocketService;

import io.fabric.sdk.android.Fabric;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by Sugan on 15/3/17.
 */

public class JoshDriver extends Application {

    public static final String TAG = "JoshDriver";

    private static JoshDriver mJosh = null;

    public static JoshDriver getInstance() {
        return mJosh;
    }

    public void stopService() {
        stopService(new Intent(getApplicationContext(), SocketService.class));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mJosh = this;
        Locale.setDefault(Locale.ENGLISH);
        HmLibrary.init(this);
    }

    private SystemTimer mSystemTimer;

    private class SystemTimer extends Timer {

        private long mCurrentSystemTime;

        SystemTimer(long currentSystemTime) {
            mCurrentSystemTime = currentSystemTime;
        }

        void schedule() {
            super.schedule(new TimerTask() {
                @Override
                public void run() {
                    mCurrentSystemTime++;
                }
            }, 0, 1);

        }

        public void setTime(long currentSystemTime) {
            mCurrentSystemTime = currentSystemTime;
        }

        public long getSystemTime() {
            return mCurrentSystemTime;
        }

    }

    public void setSystemTime(long serverTimeInMs) {
        if (null == mSystemTimer) {
            mSystemTimer = new SystemTimer(serverTimeInMs);
            mSystemTimer.schedule();
        } else {
            mSystemTimer.setTime(serverTimeInMs);
        }
    }

    public long getServerClockSystemTime(){
        if(mSystemTimer != null){
            return mSystemTimer.getSystemTime();
        }
        return System.currentTimeMillis();
    }

    public void setServerTime(String serverTime) {
        long milliSecs = CodeSnippet.getDateFromDateString(serverTime,
                CustomDateFormats.HEADER_TIME_FORMAT, CustomDateFormats.TIME_ZONE_GMT).getTime();
        HmLibrary.getHmLibrary().setSharedLongData(Constants.SharedKey.SERVER_TIME, milliSecs);
        setSystemTime(milliSecs);
    }

    public long getServerTime() {
        return HmLibrary.getHmLibrary().getSharedLongData(Constants.SharedKey.SERVER_TIME);
    }

}
