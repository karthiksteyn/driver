package com.josh.driver.service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.hm.Log;
import com.hm.utils.CodeSnippet;
import com.hm.utils.CustomDateFormats;
import com.josh.driver.AppSnippet;
import com.josh.driver.Constants;
import com.josh.driver.dto.request.UpdateDriverLocRequest;
import com.josh.driver.dto.response.Order;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.handler.GPSHandler;
import com.josh.driver.socket.SocketHandler;
import com.josh.driver.socket.SocketKeys;
import com.josh.driver.socket.SocketListener;

public class SocketService extends Service implements GPSHandler.ConnectionListener, SocketListener{

    private static final String TAG = "SocketService";

    private static final float DISTANCE_METER = 6.0f;

    private GPSHandler mGpsHandler;

    private int mOneSecVal = 1000;

    private Location mFromLocation;

    private SocketHandler mSocketHandler;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service  Started");
        AppSnippet.generateNoteOnSD(getApplicationContext(), "JoshLogs.txt", "\nService  Started");
        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mSocketHandler = SocketHandler.getInstance(DataHandler.getInstance().retrieveDriverId(),
                SocketKeys.SocketListenerKeys.TRACKING, this, false, getApplicationContext());

        if(checkLocationPermission(getApplicationContext())) {
            if (mGpsHandler == null) {
                mGpsHandler = new GPSHandler(getApplicationContext(), this);
            }
            if (!mGpsHandler.isConnected() || !mGpsHandler.isNetworkEnabled(true)) {
                mGpsHandler.connect();
            }
        }

    }

    private boolean checkLocationPermission(Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.i(TAG, "Task removed");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Service  Destroyed");
        AppSnippet.generateNoteOnSD(getApplicationContext(), "JoshLogs.txt", "\nService  Destroyed");
        mGpsHandler.disconnect();
        mSocketHandler.disconnectSocket();
    }

    @Override
    public void onGetLastKnownLocation(Bundle bundle, Location location) {
        if (location != null) {
            onLocationChanged(location);
        }
        mGpsHandler.startContinuousLocationListening((10 * mOneSecVal), (5 * mOneSecVal), GPSHandler.DEFAULT, GPSHandler.DEFAULT);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        float accuracy = location.getAccuracy();
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        UpdateDriverLocRequest updateDriverLocRequest =new UpdateDriverLocRequest();
        updateDriverLocRequest.setLat(latitude);
        updateDriverLocRequest.setLon(longitude);

        Log.d(TAG,"----"+latitude+"------"+longitude+"----");
        DataHandler.getInstance().saveLastKnownLocation(updateDriverLocRequest);

        Intent intent = new Intent(Constants.Receiver.LOCATION_RECEIVER);
        intent.putExtra(Constants.BundleKey.LATITUDE, latitude);
        intent.putExtra(Constants.BundleKey.LONGITUDE, longitude);
        sendBroadcast(intent);

        float distanceInMeters = 0;

        if(mFromLocation != null)
        {
            distanceInMeters = mFromLocation.distanceTo(location);
        }

        if(mFromLocation == null || distanceInMeters > DISTANCE_METER ){
            updateDriverLocRequest.setDriver(DataHandler.getInstance().retrieveDriverId());
            updateDriverLocRequest.setAccuracy(accuracy);

            updateDriverLocRequest.setDts(CodeSnippet.getDateStringFromLong(System.currentTimeMillis(), CustomDateFormats.SERVER_DATE_FORMAT,
                    CustomDateFormats.TIME_ZONE_GMT));

            TripDetailsResponseDto tripDetailsResponseDto = DataHandler.getInstance().retrieveTripDetails();
            if(tripDetailsResponseDto != null) {
                if(tripDetailsResponseDto.getStatus() == Constants.TripStatus.WAIT_STOP){
                    updateDriverLocRequest.setBookingid(tripDetailsResponseDto.getRid());
                }
            }

            mSocketHandler.postLocation(updateDriverLocRequest);
            mFromLocation = location;
        }
    }

    @Override
    public void onSocketConnected() {

    }

    @Override
    public void onConnectError() {

    }

    @Override
    public void onSocketConnectionFailed() {

    }

    @Override
    public void onSocketDisconnected() {

    }

    @Override
    public void onOrderRequest(Order order) {

    }

    @Override
    public void onForceOrderRequest(TripDetailsResponseDto tripDetailsResponseDto) {

    }

    @Override
    public void onSignOutRequest() {

    }
}