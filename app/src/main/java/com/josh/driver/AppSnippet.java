package com.josh.driver;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hm.utils.CodeSnippet;
import com.hm.utils.CustomDateFormats;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by Sridevi on 6/3/17.
 */

public class AppSnippet {

    /**
     * @return True if walkthrough is completed, else return False
     */
    public static boolean isWalkThroughCompleted(){
        return false;
    }

    /**
     * @return True if profile setup is completed, else return False
     */
    public static boolean isProfileSetupCompleted(){
        return false;
    }

    /**
     * @return True if login is completed, else return False
     */
    public static boolean isLoginCompleted(){
        return false;
    }

    /**
     * @return True if otp verification is completed, else return False
     */
    public static boolean isOtpVerificationCompleted(){
        return false;
    }

    public static LayoutInflater getLayoutInflater(Context context){
        return LayoutInflater.from(context);
    }

    public static void setTheme(Activity activity){
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(getThemeFlags());
    }

    public static int getThemeFlags() {
        return
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
    }

    public static void hideBottomNavigation(Activity activity) {
        if (activity != null) {
            final View decorView = activity.getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }
    }

    public static AlertDialog showAlertDialog(Activity activity, String title, String content,
                                              String positiveTxt, View.OnClickListener positiveListener, String negativeTxt, View.OnClickListener negativeListener) {
        View view = activity.getLayoutInflater().inflate(R.layout.inflater_custom_alert_dialog, null);
        TextView titleTv = (TextView) view.findViewById(R.id.alert_dialog_title_tv);
        if (title != null) {
            titleTv.setText(title);
        } else {
            titleTv.setVisibility(View.GONE);
        }
        ((TextView) view.findViewById(R.id.alert_dialog_content_tv)).setText(content);
        TextView positive = (TextView) view.findViewById(R.id.alert_dialog_positive_tv);
        positive.setText(positiveTxt);
        positive.setOnClickListener(positiveListener);
        view.findViewById(R.id.alert_dialog_negative_layout).setVisibility(View.VISIBLE);
        TextView negative = (TextView) view.findViewById(R.id.alert_dialog_negative_tv);
        RelativeLayout negativeLayout = (RelativeLayout) view.findViewById(R.id.alert_dialog_negative_layout);
        if(negativeTxt != null) {
            negative.setText(negativeTxt);
            negative.setOnClickListener(negativeListener);
        }else{
            negativeLayout.setVisibility(View.GONE);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(view);
        builder.setCancelable(false);

        return builder.show();
    }

    public static String replaceEmpty(String value){
        return value.equals("")? "UnKnown" : value ;
    }

    public static void generateNoteOnSD(Context context, String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "JoshDriver");
            if (!root.exists()) {
                root.mkdirs();
            }
            File out;
            OutputStreamWriter outStreamWriter = null;
            FileOutputStream outStream = null;

            out = new File(root, sFileName);

            if ( out.exists() == false ){
                out.createNewFile();
            }

            outStream = new FileOutputStream(out, true) ;
            outStreamWriter = new OutputStreamWriter(outStream);

            outStreamWriter.append(sBody+ "----"+ CodeSnippet.getDateStringFromLong(System.currentTimeMillis(), CustomDateFormats.TEMP_DATE,
                    CustomDateFormats.INDIA_TIMEZONE));
            outStreamWriter.flush();

//            File gpxfile = new File(root, sFileName);
//
//            FileWriter writer = new FileWriter(gpxfile);
//            writer.append(sBody+ "----"+ CodeSnippet.getDateStringFromLong(System.currentTimeMillis(), CustomDateFormats.EEEE_DD_MMM_HH_MM_AA,
//                    CustomDateFormats.INDIA_TIMEZONE));
//            writer.flush();
//            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
