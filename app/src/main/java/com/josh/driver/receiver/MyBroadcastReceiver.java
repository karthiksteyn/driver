package com.josh.driver.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.josh.driver.Constants;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.modules.home.HomeActivity;
import com.josh.driver.service.SocketService;

public class MyBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case Constants.Receiver.RECEIVER_KEY:
                Intent requestIntent = new Intent(context, HomeActivity.class);
                requestIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                requestIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                if (intent.getStringExtra(Constants.BundleKey.REQUEST).equals(Constants.AppKey.WAITING_REQUEST)) {
                    requestIntent.putExtra(Constants.BundleKey.REQUEST, Constants.AppKey.WAITING_REQUEST);
                    requestIntent.putExtras(intent.getExtras());
                } else if (intent.getStringExtra(Constants.BundleKey.REQUEST).equals(Constants.AppKey.FORCE_REQUEST)){
                    requestIntent.putExtra(Constants.BundleKey.REQUEST, Constants.AppKey.FORCE_REQUEST);
                } else if (intent.getStringExtra(Constants.BundleKey.REQUEST).equals(Constants.AppKey.SIGNOUT_EXPIRY)){
                    requestIntent.putExtra(Constants.BundleKey.REQUEST, Constants.AppKey.SIGNOUT_EXPIRY);
                } else if (intent.getStringExtra(Constants.BundleKey.REQUEST).equals(Constants.AppKey.FORCE_TRIP_STATUS)){
                    requestIntent.putExtra(Constants.BundleKey.REQUEST, Constants.AppKey.FORCE_TRIP_STATUS);
                }
                context.startActivity(requestIntent);
                break;

            case Constants.Receiver.BOOT_COMPLETED:
                if (DataHandler.getInstance().retrieveProfileCreated()) {
                    Intent serviceIntent = new Intent(context, SocketService.class);
                    context.startService(serviceIntent);
                }
                break;
        }

    }

}