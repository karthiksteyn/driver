package com.josh.driver.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.maps.model.LatLng;
import com.hm.Log;
import com.josh.driver.Constants;


public class LocationBroadcastReceiver extends BroadcastReceiver {

    private MapLocationListener mMapLocationListener;

    private static final double DEFAULT_VALUE = 0.0;

    public LocationBroadcastReceiver(){

    }

    public LocationBroadcastReceiver(MapLocationListener mapLocationListener) {

            mMapLocationListener = mapLocationListener;
    }

    public static LocationBroadcastReceiver getInstance(MapLocationListener mapLocationListener) {
        return new LocationBroadcastReceiver(mapLocationListener);

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case Constants.Receiver.LOCATION_RECEIVER:
                double lat = intent.getDoubleExtra(Constants.BundleKey.LATITUDE, DEFAULT_VALUE);
                double lng = intent.getDoubleExtra(Constants.BundleKey.LONGITUDE, DEFAULT_VALUE);

                LatLng latLng = new LatLng(lat, lng);
                if(mMapLocationListener != null) {
                    Log.d("sugan","----"+lat+"------"+lng+"----");

                    mMapLocationListener.onChangeLocation(latLng);
                }
                break;
        }

    }

    public interface MapLocationListener {

        void onChangeLocation(LatLng currentLatLng);
    }

}