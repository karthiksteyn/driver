package com.josh.driver.modules.details.orderdetails;

import android.content.Context;
import android.os.Bundle;


import com.hm.presenter.BasePresenter;
import com.josh.driver.Constants;
import com.josh.driver.dto.response.TripDetailsResponseDto;

/**
 * Created by hmspl on 13/3/17.
 */

public class TripDetailsPresenterImpl extends BasePresenter implements TripDetailsPresenter,
        TripDetailsModelImpl.TripDetailsModelListener {

    private TripDetailsView mTripDetailsView;

    private TripDetailsModel mTripDetailsModel;

    public TripDetailsPresenterImpl(TripDetailsView tripDetailsView) {
        super(tripDetailsView);
        mTripDetailsView = tripDetailsView;
        mTripDetailsModel = TripDetailsModelImpl.getInstance(this);
    }

    public static TripDetailsPresenter getInstance(TripDetailsView tripDetailsView) {
        return new TripDetailsPresenterImpl(tripDetailsView);
    }

    @Override
    public void onTripDetailsPresenterCreate(Bundle bundle, Context context) {
        if (mTripDetailsView.hasNetworkConnection(Constants.Message.NO_INTERNET)) {
            mTripDetailsModel.fetchTripDetails(bundle, context);
        }
    }

    @Override
    public void onClickAdditionalSettings() {
        mTripDetailsView.navigateToAdditionalSettings(mTripDetailsModel.getTripDetails());
    }

    @Override
    public void onFetchFailed(String message) {
        mTripDetailsView.dismissProgressbar();
        mTripDetailsView.showMessage(message);
    }

    @Override
    public void onFetchSuccess(TripDetailsResponseDto tripDetailsResponseDto) {
        mTripDetailsView.setTripDetails(tripDetailsResponseDto);
    }
}
