package com.josh.driver.modules;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.josh.driver.AppSnippet;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.adapter.ContactAdapter;
import com.josh.driver.dto.ContactDto;

import java.util.List;

public class ContactFragment extends DialogFragment implements ContactAdapter.CountryCodeItemClickListener {
    private RecyclerView mCountryCodeList;
    private ContactAdapter mCountryCodeAdapter;
    private ContactSelectListener mContactSelectListener;
    private SearchView mContactSearchView;
    private String mEnteredText;

    public ContactFragment(){

    }

    public ContactFragment(ContactSelectListener listener) {
        mContactSelectListener = listener;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        mCountryCodeList = (RecyclerView) view.findViewById(R.id.fragment_contact_list);
        mContactSearchView = (SearchView) view.findViewById(R.id.fragment_contact_search);
        mContactSearchView.setFocusable(true);
        mContactSearchView.setIconified(false);
        mContactSearchView.setClickable(false);
        mContactSearchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (TextUtils.isEmpty(mEnteredText)) {
                            dismiss();
                        } else {
                            mContactSearchView.setQuery("", false);
                        }
                    }
                });
        setSearchListener();
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }
    private void setSearchListener() {
        mContactSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                mEnteredText = newText;
                mCountryCodeAdapter.setFilter(newText);
                return false;
            }
        });
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mCountryCodeList.setLayoutManager(linearLayoutManager);
        List<ContactDto> contactDtos = getArguments().getParcelableArrayList(Constants.BundleKey.CONTACT_LIST);
        mCountryCodeAdapter = new ContactAdapter(contactDtos, this);
        mCountryCodeList.setAdapter(mCountryCodeAdapter);
    }
    @Override
    public void onContactClick(int position, ContactDto countryListDto) {
        mContactSelectListener.onContactSelected(countryListDto);
        dismiss();
    }
    public interface ContactSelectListener {
        void onContactSelected(ContactDto countryListDto);
    }
}

