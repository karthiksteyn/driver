package com.josh.driver.modules.billpayment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.animation.PathInterpolatorCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hm.Log;
import com.hm.fragment.BaseFragment;
import com.josh.driver.AppSnippet;
import com.josh.driver.R;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.modules.home.HomeActivity;
import com.josh.driver.widget.OnSwipeTouchListener;

public class BillFragment extends BaseFragment implements BillView, View.OnClickListener {

    private static final String TAG = BillFragment.class.getSimpleName();
    private BillPresenter mBillPresenter;

    private TextView mRideAmountTv, mRideAmountDescTv, mTotalDistanceTv, mTripTimeTv, mTotalFareTv,
            mServiceTaxTv, mOfferTv, mCarTypeTv, mRideStartTimeTv, mRideEndTimeTv, mRideStartDateTv,
            mRideEndDateTv, mRideStartLocationTv, mRideEndLocation;
    private RelativeLayout mPayBtn, mExpandDetailLayout;
    private ImageView mBillStateImg, mCarTypeImg;
    private RelativeLayout mBillLayout;

    public BillFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBillPresenter = BillPresenterImpl.getInstance(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bill, container, false);

        mRideAmountTv = (TextView) view.findViewById(R.id.activity_bill_amount_tv);
        mRideAmountDescTv = (TextView) view.findViewById(R.id.activity_bill_amount_with_method_tv);
        mTotalDistanceTv = (TextView) view.findViewById(R.id.activity_bill_total_distance);
        mTripTimeTv = (TextView) view.findViewById(R.id.activity_bill_trip_time);
        mTotalFareTv = (TextView) view.findViewById(R.id.activity_bill_total_far);
        mServiceTaxTv = (TextView) view.findViewById(R.id.activity_bill_service_tax);
        mOfferTv = (TextView) view.findViewById(R.id.activity_bill_Offer);
        mCarTypeTv = (TextView) view.findViewById(R.id.activity_bill_ride_car_type_tv);
        mRideStartTimeTv = (TextView) view.findViewById(R.id.activity_bill_ride_start_time_tv);
        mRideEndTimeTv = (TextView) view.findViewById(R.id.activity_bill_ride_end_time_tv);
        mRideStartDateTv = (TextView) view.findViewById(R.id.activity_bill_ride_start_date_tv);
        mRideEndDateTv = (TextView) view.findViewById(R.id.activity_bill_ride_end_date_tv);
        mRideStartLocationTv = (TextView) view.findViewById(R.id.activity_bill_ride_start_location);
        mRideEndLocation = (TextView) view.findViewById(R.id.activity_bill_ride_end_location);
        mPayBtn = (RelativeLayout) view.findViewById(R.id.activity_bill_payment_btn);
        mBillStateImg = (ImageView) view.findViewById(R.id.activity_bill_expand_icon);
        mCarTypeImg = (ImageView) view.findViewById(R.id.activity_bill_ride_car_type_img);
        mExpandDetailLayout = (RelativeLayout) view.findViewById(R.id.activity_bill_details_layout);
        mBillLayout = (RelativeLayout) view.findViewById(R.id.activity_bill_payment_layout);
        mBillPresenter.onBillPresenterCreate(this);
        view.findViewById(R.id.activity_bill_collapsed_layout).setOnTouchListener(new OnSwipeTouchListener(getContext()) {
            @Override
            public void onSwipeBottom() {
                super.onSwipeBottom();
                Log.i(TAG, "Swipe Down");
                if (mExpandDetailLayout.getVisibility() == View.GONE) {
                    expandCollapse(mExpandDetailLayout, mBillStateImg, mBillLayout);
                }
            }
        });

        mBillStateImg.setOnClickListener(this);
        mPayBtn.setOnClickListener(this);

        mExpandDetailLayout.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
            @Override
            public void onSwipeTop() {
                super.onSwipeTop();
                Log.i(TAG, "Swipe Up");
                expandCollapse(mExpandDetailLayout, mBillStateImg, mBillLayout);
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_bill_expand_icon:
                expandCollapse(mExpandDetailLayout, mBillStateImg, mBillLayout);
                break;
            case R.id.activity_bill_payment_btn:
                mBillPresenter.onSubmitButtonClick();
                break;
        }
    }

    public static void expandCollapse(final View view, final ImageView billStateImg, final RelativeLayout billLayout) {

        final boolean expand = view.getVisibility() == View.GONE;
        Interpolator easeInOutQuart = PathInterpolatorCompat.create(0.77f, 0f, 0.175f, 1f);

        view.measure(
                View.MeasureSpec.makeMeasureSpec(((View) view.getParent()).getWidth(), View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        );

        final int height = view.getMeasuredHeight();
        int duration = (int) (height / view.getContext().getResources().getDisplayMetrics().density);
        duration = !expand&& !billStateImg.isSelected()?0:duration;
        final int finalDuration = duration;

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (expand) {
                    view.getLayoutParams().height = 1;
                    view.setVisibility(View.VISIBLE);
                    if (interpolatedTime == 1) {
                        view.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    } else {
                        view.getLayoutParams().height = (int) (height * interpolatedTime);
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            billLayout.setSelected(true);
                        }
                    }, finalDuration);
                    billStateImg.setSelected(true);
                    view.requestLayout();
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            billLayout.setSelected(false);
                        }
                    }, finalDuration);
                    billStateImg.setSelected(false);
                    if (interpolatedTime == 1) {
                        view.setVisibility(View.GONE);
                    } else {
                        view.getLayoutParams().height = height - (int) (height * interpolatedTime);
                        view.requestLayout();
                    }
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setInterpolator(easeInOutQuart);
        animation.setDuration(duration);
        view.startAnimation(animation);

    }

    @Override
    public void setTripDetails(TripDetailsResponseDto tripDetailsResponseDto) {
        mRideAmountTv.setText(String.valueOf(tripDetailsResponseDto.getNettbillingamount()));
        mTotalDistanceTv.setText(String.valueOf(tripDetailsResponseDto.getTotalmileage())+getString(R.string.km));
        mTripTimeTv.setText(String.valueOf(tripDetailsResponseDto.getTotalduration())+getString(R.string.minute));
        mTotalFareTv.setText(String.valueOf(tripDetailsResponseDto.getTotalridefare()));
        mServiceTaxTv.setText(String.valueOf(tripDetailsResponseDto.getTax()));
        mOfferTv.setText(String.valueOf(tripDetailsResponseDto.getTotaldiscount()));
        mCarTypeTv.setText(String.valueOf(tripDetailsResponseDto.getCabtype().getName()));
        mRideStartLocationTv.setText(String.valueOf(tripDetailsResponseDto.getFrom()));
        mRideEndLocation.setText(String.valueOf(AppSnippet.replaceEmpty(tripDetailsResponseDto.getTo())));
        mRideEndTimeTv.setText(mBillPresenter.getTime(tripDetailsResponseDto.getEndtime()));
        mRideStartTimeTv.setText(mBillPresenter.getTime(tripDetailsResponseDto.getStarttime()));
        mRideEndDateTv.setText(mBillPresenter.getDate(tripDetailsResponseDto.getEndtime()));
        mRideStartDateTv.setText(mBillPresenter.getDate(tripDetailsResponseDto.getStarttime()));

        expandCollapse(mExpandDetailLayout, mBillStateImg, mBillLayout);
        mBillStateImg.setSelected(false);
    }

    @Override
    public void navigateToHome() {
        dismissProgressbar();
        Activity activity = getActivity();
        if (activity != null) {
            ((HomeActivity) activity).showMyHome();
        }
    }
}
