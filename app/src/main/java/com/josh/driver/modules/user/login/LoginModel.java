package com.josh.driver.modules.user.login;

import android.content.Context;

import com.josh.driver.dto.request.LoginRequestDto;


/**
 * Created by hmspl on 13/3/17.
 */

public interface LoginModel {
    void sendLoginDetails(LoginRequestDto loginRequestDto, Context context);

    boolean isProfileCreated();
}
