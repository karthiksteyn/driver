package com.josh.driver.modules.user.profile;

/**
 * Created by hmspl on 13/3/17.
 */

public interface ProfilePresenter {
    void onProfilePresenterCreate();

    void onLogoutClick();

    void onTakeImageClick();

}
