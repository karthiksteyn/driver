package com.josh.driver.modules.home;

import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.model.LatLng;
import com.hm.Log;
import com.hm.model.BaseModel;
import com.hm.utils.CodeSnippet;
import com.hm.utils.CustomDateFormats;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.dto.Headers;
import com.josh.driver.dto.MenuDto;
import com.josh.driver.dto.request.UpdateDriverLocRequest;
import com.josh.driver.dto.response.BalanceSummaryResponseDto;
import com.josh.driver.dto.response.Order;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.handler.GPSHandler;
import com.josh.driver.receiver.LocationBroadcastReceiver;
import com.josh.driver.socket.SocketHandler;
import com.josh.driver.socket.SocketKeys;
import com.josh.driver.socket.SocketListener;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public class HomeModelImpl extends BaseModel implements HomeModel,/*GPSHandler.ConnectionListener,*/ SocketListener, LocationBroadcastReceiver.MapLocationListener {

    private static final String TAG = "HomeModel";

    private static final float DISTANCE_METER = 6.0f;

    private HomeModelListener mHomeModelListener;

    private GPSHandler mGpsHandler;

    private SocketHandler mSocketHandler;

    private LocationBroadcastReceiver mLocationBroadcastReceiver;

    private int mOneSecVal = 1000;

    private Location mFromLocation;

    public HomeModelImpl(HomeModelListener homeModelListener) {
        super(homeModelListener);

        mHomeModelListener = homeModelListener;
    }

    public static HomeModel getInstance(HomeModelListener homeModelListener) {
        return new HomeModelImpl(homeModelListener);

    }

    @Override
    public void onHomeModelCreate(Bundle bundle, Context context) {
//        mSocketHandler = SocketHandler.getInstance(DataHandler.getInstance().retrieveDriverId(), SocketKeys.SocketListenerKeys.TRACKING, this, false, context);

//        if (bundle != null) {
//            int currentPosition = bundle.getInt(Constants.BundleKey.CURRENT_POSITION);
//            String currentValue = bundle.getString(Constants.BundleKey.CURRENT_VALUE);
//            mHomeModelListener.onSetStartMenu(currentPosition, currentValue);
//        }

        mLocationBroadcastReceiver = LocationBroadcastReceiver.getInstance(this);


        fetchMenuDetails(context);
    }

    @Override
    public String retrieveDriverName() {
        return DataHandler.getInstance().retrieveDisplayName();
    }

    @Override
    public String retrieveDisplayPic() {
        return DataHandler.getInstance().retrieveDisplayPic();
    }

    @Override
    public float retrieveBalance() {
        return DataHandler.getInstance().retrieveBalance();
    }

    @Override
    public void onLocationPermissionEnabled(Context context) {
        /*if (mGpsHandler == null) {
            mGpsHandler = new GPSHandler(context, this);
        }
        if (!mGpsHandler.isConnected() || !mGpsHandler.isNetworkEnabled(true)) {
            mGpsHandler.connect();
        }*/

//        mHomeModelListener.getActivity().registerReceiver(mLocationBroadcastReceiver,
//                new IntentFilter(Constants.Receiver.LOCATION_RECEIVER));
    }

    @Override
    public boolean retrieveTripDetails() {
        return (DataHandler.getInstance().retrieveTripDetails() != null) ;

    }

    @Override
    public int getTripStatus() {
        return DataHandler.getInstance().retrieveTripDetails().getStatus();
    }

    @Override
    public void unRegisterReceiver() {

        mHomeModelListener.getActivity().unregisterReceiver(mLocationBroadcastReceiver);
    }

    @Override
    public boolean isProfileCreated() {
        return DataHandler.getInstance().retrieveProfileCreated();
    }

    @Override
    public void registerReceiver() {
        mHomeModelListener.getActivity().registerReceiver(mLocationBroadcastReceiver,
                new IntentFilter(Constants.Receiver.LOCATION_RECEIVER));
    }

    private Headers getTokens() {
        Headers headers = new Headers();
        headers.setAuthToken(DataHandler.getInstance().retrieveAuthToken());
        headers.setUserToken(DataHandler.getInstance().retrieveUserToken());
        headers.setAppToken(Constants.AppKey.APP_TOKEN);
        return headers;
    }

    private void fetchMenuDetails(Context context) {
        List<MenuDto> menuList = new ArrayList<>();
        Resources resources = context.getResources();
        TypedArray icons = resources.obtainTypedArray(R.array.menu_icons);
        String[] titles = resources.getStringArray(R.array.menu_titles);
        for (int i = 0; i < titles.length; i++) {
            menuList.add(new MenuDto(icons.getResourceId(i, -1), titles[i]));
        }
        icons.recycle();
        mHomeModelListener.onMenuListDataFetchSuccess(DataHandler.getInstance().retrieveDisplayName(), menuList);
    }

    /*@Override
    public void onGetLastKnownLocation(Bundle bundle, Location location) {
        if (location != null) {
            onLocationChanged(location);
        }
//        mGpsHandler.startContinuousLocationListening((10 * mOneSecVal), (5 * mOneSecVal), GPSHandler.DEFAULT, GPSHandler.DEFAULT);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        float accuracy = location.getAccuracy();
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        UpdateDriverLocRequest updateDriverLocRequest =new UpdateDriverLocRequest();
        updateDriverLocRequest.setLat(latitude);
        updateDriverLocRequest.setLon(longitude);

        DataHandler.getInstance().saveLastKnownLocation(updateDriverLocRequest);

        mHomeModelListener.onFetchLocationSuccess(latitude, longitude);

//        float distanceInMeters = 0;
//
//        if(mFromLocation != null)
//        {
//            distanceInMeters = mFromLocation.distanceTo(location);
//        }
//
//        if(mFromLocation == null || distanceInMeters > DISTANCE_METER ){
//            updateDriverLocRequest.setDriver(DataHandler.getInstance().retrieveDriverId());
//            updateDriverLocRequest.setAccuracy(accuracy);
//
//            updateDriverLocRequest.setDts(CodeSnippet.getDateStringFromLong(System.currentTimeMillis(), CustomDateFormats.SERVER_DATE_FORMAT,
//                    CustomDateFormats.TIME_ZONE_GMT));
//
//            TripDetailsResponseDto tripDetailsResponseDto = DataHandler.getInstance().retrieveTripDetails();
//            if(tripDetailsResponseDto != null) {
//                 if(tripDetailsResponseDto.getStatus() == Constants.TripStatus.WAIT_STOP){
//                    updateDriverLocRequest.setBookingid(tripDetailsResponseDto.getRid());
//                 }
//            }
//
//            mSocketHandler.postLocation(updateDriverLocRequest);
//            mFromLocation = location;
//        }


    }*/

    public void onSocketConnected() {
        Log.i(TAG, "Socket Connected");
    }

    public void onConnectError() {
        Log.i(TAG, "Socket Connect Error");
    }

    public void onSocketConnectionFailed() {
        Log.i(TAG, "Socket Connection Failed");
    }

    public void onSocketDisconnected() {
        Log.i(TAG, "Socket Disconnected");
    }

    @Override
    public void onOrderRequest(Order order) {
        mHomeModelListener.onRequestSuccess(order);
    }

    @Override
    public void onForceOrderRequest(TripDetailsResponseDto tripDetailsResponseDto) {
        mHomeModelListener.onForceRequestSuccess(tripDetailsResponseDto);
    }

    @Override
    public void onSignOutRequest() {
        mHomeModelListener.onSignOutSuccess();

    }

    @Override
    public void onChangeLocation(LatLng location) {
        double latitude = location.latitude;
        double longitude = location.longitude;

        mHomeModelListener.onFetchLocationSuccess(latitude, longitude);

    }

    interface HomeModelListener extends ModelListener {
        void onSetStartMenu(int currentPosition, String currentValue);

        void onMenuListDataFetchSuccess(String name, List<MenuDto> menuList);

        void onFetchLocationSuccess(double latitude, double longitude);

        void onRequestSuccess(Order order);

        void onForceRequestSuccess(TripDetailsResponseDto tripDetailsResponseDto);

        void onSignOutSuccess();
    }
}
