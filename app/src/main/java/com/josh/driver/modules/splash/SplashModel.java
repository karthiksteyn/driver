package com.josh.driver.modules.splash;


public interface SplashModel {

    boolean isWalkThroughCompleted();

    boolean isLoginCompleted();

    boolean isOtpVerificationCompleted();

    boolean isProfileSetupCompleted();

}
