package com.josh.driver.modules.user.login;

import android.content.Context;
import android.content.res.Resources;
import android.util.Patterns;

import com.hm.HmLibrary;
import com.hm.model.BaseModel;
import com.hm.utils.CodeSnippet;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.request.LoginRequestDto;
import com.josh.driver.dto.response.LoginResponseDto;
import com.josh.driver.handler.AppDataHandler;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

/**
 * Created by hmspl on 13/3/17.
 */

public class LoginModelImpl extends BaseModel implements LoginModel,
        ModelHandler.ModelHanlderListener<ApiStatusDto>, AppDataHandler.AppDataHandlerCallBack {

    private LoginModelListener mLoginModelListener;

    private Context mContext;

    private LoginRequestDto mLoginRequestDto;

    public LoginModelImpl(LoginModelListener loginModelListener) {
        super(loginModelListener);

        mLoginModelListener = loginModelListener;
    }

    public static LoginModel getInstance(LoginModelListener loginModelListener) {
        return new LoginModelImpl(loginModelListener);

    }

    private void doLogin(LoginRequestDto loginRequestDto) {
        mLoginRequestDto = loginRequestDto;
        ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().getLoginRequest(loginRequestDto));
    }

    @Override
    public void sendLoginDetails(LoginRequestDto loginRequestDto, Context context) {
        mContext = context;
        if (validate(loginRequestDto)) {
            doLogin(loginRequestDto);
        }
    }

    @Override
    public boolean isProfileCreated() {
        return DataHandler.getInstance().retrieveProfileCreated();
    }


    private boolean validate(LoginRequestDto loginRequestDto) {
        Resources resources = mContext.getResources();
        String value = loginRequestDto.getMobile();

        if (value.isEmpty()) {
            mLoginModelListener.onLoginFailed(resources.getString(R.string.phone_empty));
            return false;
        } else if (value.length() < 10 || !Patterns.PHONE.matcher(value).matches()) {
            mLoginModelListener.onLoginFailed(resources.getString(R.string.phone_validation));
            return false;
        }

        value = loginRequestDto.getPassword();
        if (value.isEmpty()) {
            mLoginModelListener.onLoginFailed(resources.getString(R.string.pwd_empty));
            return false;
        }
        return true;
    }

    @Override
    public void onSuccessApi(ApiStatusDto apiStatusDto) {
        if (apiStatusDto.isResultOk()) {
            LoginResponseDto loginResponseDto = CodeSnippet.getObjectFromMap(apiStatusDto.getData(), LoginResponseDto.class);
            DataHandler.getInstance().saveUserData(loginResponseDto, mLoginRequestDto);

            if(loginResponseDto.getActivebookingid() != null) {
                new AppDataHandler(this).fetchBookingDetails(loginResponseDto.getActivebookingid());
            }else{
                mLoginModelListener.onLoginSuccess();
            }
        } else {
            mLoginModelListener.onLoginFailed(apiStatusDto.getDescription());
        }
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mLoginModelListener.onLoginFailed(errorMessage);

    }

    @Override
    public void onAuthorizationFailed() {

    }

    @Override
    public void onAppDatFetchSuccess() {

    }

    @Override
    public void onAppDataFetchFailed(String message) {

    }

    @Override
    public void onBookingDataFetchSuccess() {
        mLoginModelListener.onLoginSuccess();
    }

    @Override
    public void onBookingDataFetchFailed(String message) {
        HmLibrary.getHmLibrary().clearAllData();
        mLoginModelListener.onLoginFailed(message);
    }

    @Override
    public void onForceUpdate() {

    }


    interface LoginModelListener extends ModelListener {

        void onLoginSuccess();

        void onLoginFailed(String msg);
    }
}
