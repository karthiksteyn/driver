package com.josh.driver.modules.billpayment;

import com.hm.activity.View;
import com.josh.driver.dto.response.TripDetailsResponseDto;

/**
 * Created by hmspl on 13/3/17.
 */

public interface BillView extends View {
    void setTripDetails(TripDetailsResponseDto tripDetailsResponseDto);

    void navigateToHome();
}
