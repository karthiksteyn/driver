package com.josh.driver.modules.recharge;

import android.content.res.Resources;

import com.hm.presenter.BasePresenter;
import com.josh.driver.R;
import com.josh.driver.dto.ContactDto;

import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public class TopupPresenterImpl extends BasePresenter implements TopupPresenter, TopupModelImpl.TopupModelListener {

    private TopupView mTopupView;

    private TopupModel mTopupModel;

    private Resources mResources;

    public TopupPresenterImpl(TopupView topupView) {
        super(topupView);
        mTopupView = topupView;
        mTopupModel = TopupModelImpl.getInstance(this);
    }

    public static TopupPresenter getInstance(TopupView topupView) {
        return new TopupPresenterImpl(topupView);
    }


    @Override
    public void onPaymentClick(TopupFragment topupFragment) {
        mResources = topupFragment.getContext().getResources();
        if (mTopupView.hasNetworkConnection(mResources.getString(R.string.no_internet))) {
            mTopupView.showProgressbar();
            mTopupModel.doPayment(mTopupView.getPaymentDetails(), topupFragment.getContext());
        }
    }

    @Override
    public void onPhoneBookPressed() {
        mTopupModel.fetchContact();
    }

    @Override
    public void onPhoneBookClick() {
        mTopupView.checkContactPermission();
    }

    @Override
    public void onPaymentSuccess(String message) {
        mTopupView.dismissProgressbar();
        mTopupView.showMessage(message);
        mTopupView.NavigateToPaytm();
    }

    @Override
    public void onPaymentFailed(String message) {
        mTopupView.dismissProgressbar();
        mTopupView.showMessage(message);
    }

    @Override
    public void onContactFetchSuccess(List<ContactDto> contactDtoList) {
        mTopupView.showContactListDialog(contactDtoList);
    }
}
