package com.josh.driver.modules.recharge;

import android.content.Context;

/**
 * Created by hmspl on 13/3/17.
 */

public interface TopupPresenter {
    void onPaymentClick(TopupFragment topupFragment);

    void onPhoneBookPressed();

    void onPhoneBookClick();
}
