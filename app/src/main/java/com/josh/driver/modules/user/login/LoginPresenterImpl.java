package com.josh.driver.modules.user.login;

import android.content.Context;
import android.content.res.Resources;

import com.hm.presenter.BasePresenter;
import com.josh.driver.R;

/**
 * Created by hmspl on 13/3/17.
 */

public class LoginPresenterImpl extends BasePresenter implements LoginPresenter, LoginModelImpl.LoginModelListener {

    private LoginView mLoginView;

    private LoginModel mLoginModel;

    private Resources mResources;

    public LoginPresenterImpl(LoginView loginView) {
        super(loginView);
        mLoginView = loginView;
        mLoginModel = LoginModelImpl.getInstance(this);
    }

    public static LoginPresenter getInstance(LoginView loginView) {
        return new LoginPresenterImpl(loginView);
    }

    @Override
    public void onLoginClick(Context context) {
        mResources = context.getResources();

        if (mLoginView.hasNetworkConnection(mResources.getString(R.string.no_internet))) {
            mLoginView.showProgressbar();
            mLoginModel.sendLoginDetails(mLoginView.getLoginDetails(), context);
        }
    }

    @Override
    public void onSignUpClick() {

    }

    @Override
    public boolean onIsProfileCreated() {
        return mLoginModel.isProfileCreated();
    }

    @Override
    public void onLoginSuccess() {
        mLoginView.navigateToHome();
    }

    @Override
    public void onLoginFailed(String msg) {
        mLoginView.dismissProgressbar();
        mLoginView.showMessage(msg);
    }
}
