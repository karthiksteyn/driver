package com.josh.driver.modules.details.orderdetails;

import android.content.Context;
import android.os.Bundle;

/**
 * Created by hmspl on 13/3/17.
 */

public interface TripDetailsPresenter {
    void onTripDetailsPresenterCreate(Bundle bundle, Context context);

    void onClickAdditionalSettings();
}
