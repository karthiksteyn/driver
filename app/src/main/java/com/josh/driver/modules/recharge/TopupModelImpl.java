package com.josh.driver.modules.recharge;

import android.content.Context;
import android.content.res.Resources;
import android.util.Patterns;

import com.hm.model.BaseModel;
import com.hm.utils.CodeSnippet;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.ContactDto;
import com.josh.driver.dto.Headers;
import com.josh.driver.dto.request.PaymentRequestDto;
import com.josh.driver.dto.response.PaymentResponseDto;
import com.josh.driver.handler.ContactsHandler;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public class TopupModelImpl extends BaseModel implements TopupModel, ModelHandler.ModelHanlderListener<ApiStatusDto> {

    private TopupModelListener mTopupModelListener;

    private Context mContext;

    public TopupModelImpl(TopupModelListener topupModelListener) {
        super(topupModelListener);

        mTopupModelListener = topupModelListener;
    }

    public static TopupModel getInstance(TopupModelListener topupModelListener) {
        return new TopupModelImpl(topupModelListener);

    }



    @Override
    public void doPayment(PaymentRequestDto paymentRequestDto, Context context) {

        mContext = context;
        String driverId = DataHandler.getInstance().retrieveDriverId();
        Headers headers = getTokens();
        if (validate(paymentRequestDto)) {
            ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().onVerifyMobile(driverId, headers, paymentRequestDto));
        }
    }

    @Override
    public void fetchContact() {
        List<ContactDto> contactDtos = ContactsHandler.getInstance().getPhoneContactList(mModelListener.getContext());
        if (contactDtos != null) {
            mTopupModelListener.onContactFetchSuccess(contactDtos);
        }
    }

    private boolean validate(PaymentRequestDto paymentRequestDto) {
        Resources resources = mContext.getResources();
        String value = paymentRequestDto.getName();
        if (value.isEmpty()) {
            mTopupModelListener.onPaymentFailed(resources.getString(R.string.name_empty));
            return false;
        }

        value = paymentRequestDto.getPhone();
        if (value.isEmpty()) {
            mTopupModelListener.onPaymentFailed(resources.getString(R.string.phone_empty));
            return false;
        } else if (value.length() < 10 || !Patterns.PHONE.matcher(value).matches()) {
            mTopupModelListener.onPaymentFailed(resources.getString(R.string.phone_validation));
            return false;
        }



        value = paymentRequestDto.getAmt();
        if (value.isEmpty()) {
            mTopupModelListener.onPaymentFailed(resources.getString(R.string.amt_empty));
            return false;
        }
        return true;
    }

    private Headers getTokens() {
        Headers headers = new Headers();
        headers.setAuthToken(DataHandler.getInstance().retrieveAuthToken());
        headers.setUserToken(DataHandler.getInstance().retrieveUserToken());
        headers.setAppToken(Constants.AppKey.APP_TOKEN);
        return headers;
    }

    @Override
    public void onSuccessApi(ApiStatusDto apiStatusDto) {
        if(apiStatusDto.isResultOk()){
            PaymentResponseDto paymentResponseDto = CodeSnippet.getObjectFromMap(apiStatusDto.getData(),
                    PaymentResponseDto.class);
            mTopupModelListener.onPaymentSuccess(apiStatusDto.getDescription());
        }else{
            mTopupModelListener.onPaymentFailed(apiStatusDto.getDescription());
        }
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mTopupModelListener.onPaymentFailed(errorMessage);
    }

    @Override
    public void onAuthorizationFailed() {

    }

    interface TopupModelListener extends ModelListener {

        void onPaymentSuccess(String message);

        void onPaymentFailed(String message);

        void onContactFetchSuccess(List<ContactDto> contactDtos);
    }
}
