package com.josh.driver.modules.ztest;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.hm.Log;
import com.hm.model.BaseModel;
import com.josh.driver.dto.response.Order;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.handler.GPSHandler;
import com.josh.driver.socket.SocketHandler;
import com.josh.driver.socket.SocketKeys;
import com.josh.driver.socket.SocketListener;

/**
 * Created by hmspl on 13/3/17.
 */

public class TestModelImpl extends BaseModel implements TestModel, GPSHandler.ConnectionListener, SocketListener {

    private GPSHandler mGpsHandler;

    private SocketHandler mSocketHandler;

    private TestModelListener mTestModelListener;
    private int mOneSecVal = 1000;


    public TestModelImpl(TestModelListener testModelListener) {
        super(testModelListener);

        mTestModelListener = testModelListener;
    }

    public static TestModel getInstance(TestModelListener testModelListener) {
        return new TestModelImpl(testModelListener);

    }

    @Override
    public void onLocationPermissionEnabled(Context context) {
        if (mGpsHandler == null) {
            mGpsHandler = new GPSHandler(mTestModelListener.getContext(), this);
        }
        if (!mGpsHandler.isConnected() || !mGpsHandler.isNetworkEnabled(true)) {
            mGpsHandler.connect();
        }
    }

    @Override
    public void onTrackingModelCreate() {

    }

    @Override
    public void onGetLastKnownLocation(Bundle bundle, Location location) {
        if (location != null) {
            onLocationChanged(location);
        }
        mGpsHandler.startContinuousLocationListening((5 * mOneSecVal), (5 * mOneSecVal), GPSHandler.DEFAULT, GPSHandler.DEFAULT);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

//        System.out.println("Sugan:  "+latitude+" "+longitude);
//
        mTestModelListener.onSuccess(String.valueOf(("Sugan:  "+latitude+" "+longitude)), latitude, longitude);
//
//
//        mSocketHandler.postLocation(String.valueOf(longitude), String.valueOf(latitude));

//        Log.e("LatLng", String.valueOf(latitude)+String.valueOf(longitude));
    }

    @Override
    public void onSocketConnected() {
        Log.i("Socket", "Socket Connected");

    }

    @Override
    public void onConnectError() {

    }

    @Override
    public void onSocketConnectionFailed() {

    }

    @Override
    public void onSocketDisconnected() {

    }

    @Override
    public void onOrderRequest(Order order) {

    }

    @Override
    public void onForceOrderRequest(TripDetailsResponseDto tripDetailsResponseDto) {

    }

    @Override
    public void onSignOutRequest() {

    }

    interface TestModelListener extends ModelListener {

        void onSuccess(String msg, double latitude, double longitude);
    }
}
