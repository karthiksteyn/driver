package com.josh.driver.modules.myhome;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hm.fragment.BaseFragment;
import com.hm.handler.PermissionHandler;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.dto.response.BalanceSummaryResponseDto;
import com.josh.driver.modules.home.HomeActivity;
import com.josh.driver.widget.CabSelectionFragment;

public class MyHomeFragment extends BaseFragment implements MyHomeView, OnMapReadyCallback, View.OnClickListener,
        CabSelectionFragment.CabSelectionListener {

    private static final int NAVIGATION_REQUEST_CODE = 1;

    private MyHomePresenter mMyHomePresenter;

    private GoogleMap mMap;

    private Marker mMarker;

    private LatLng mSource;

    public PermissionHandler mPermissionHandler;

    private TextView mTotRidesText, mMileageText,
            mOnlineDurationText, mRevenueText;

    private RelativeLayout mCarTypeLayout;

    private ImageView mCarTypeImg;

    private TextView mCarTypeText;

    private LinearLayout mOffDutyLayout, mOnDutyLayout, mGoHomeLayout;

    private ImageView mOffDutyImg, mOnDutyImg, mGoHomeImg;

    private TextView mOffDutyText, mOnDutyText, mGoHomeText;

    private CabSelectionFragment mCabSelectionFragment;

    private boolean mIsBackConfirm = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMyHomePresenter = MyHomePresenterImpl.getInstance(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_home, container, false);

        mTotRidesText = (TextView) view.findViewById(R.id.booking_count_tv);
        mMileageText = (TextView) view.findViewById(R.id.tot_km_tv);
        mOnlineDurationText = (TextView) view.findViewById(R.id.active_hrs_tv);
        mRevenueText = (TextView) view.findViewById(R.id.revenue_tv);
        mCarTypeText = (TextView) view.findViewById(R.id.car_type_tv);
        mCarTypeImg = (ImageView) view.findViewById(R.id.car_type_iv);
        mCarTypeLayout = (RelativeLayout) view.findViewById(R.id.car_type_layout);
        mCarTypeLayout.setOnClickListener(this);

        mOffDutyLayout = (LinearLayout) view.findViewById(R.id.off_duty_layout);
        mOnDutyLayout = (LinearLayout) view.findViewById(R.id.on_duty_layout);
        mGoHomeLayout = (LinearLayout) view.findViewById(R.id.go_home_layout);
        mOffDutyImg = (ImageView) view.findViewById(R.id.off_duty_iv);
        mOnDutyImg = (ImageView) view.findViewById(R.id.on_duty_iv);
        mGoHomeImg = (ImageView) view.findViewById(R.id.go_home_iv);
        mOffDutyText = (TextView) view.findViewById(R.id.off_duty_tv);
        mOnDutyText = (TextView) view.findViewById(R.id.on_duty_tv);
        mGoHomeText = (TextView) view.findViewById(R.id.go_home_tv);
        mOffDutyLayout.setTag(Constants.AppKey.OFF_DUTY);
        mOnDutyLayout.setTag(Constants.AppKey.ON_DUTY);
        mGoHomeLayout.setTag(Constants.AppKey.GO_HOME);
        mOffDutyLayout.setOnClickListener(this);
        mOnDutyLayout.setOnClickListener(this);
//        mGoHomeLayout.setOnClickListener(this);

        view.findViewById(R.id.fragment_my_home_current_address_iv).setOnClickListener(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMyHomePresenter.onMyHomePresenterCreate();

        mCabSelectionFragment = (CabSelectionFragment) getChildFragmentManager().findFragmentById(R.id.fragment_my_home_cab_type_list_view);

        mCabSelectionFragment.setDialogBgView(findViewById(R.id.fragment_my_home_dialog_bg_view));
        mCabSelectionFragment.setListener(this);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                .target(Constants.AppKey.CHENNAI_LATLNG)
                .zoom(10)
                .bearing(0)
                .tilt(45)
                .build()));

    }


    @Override
    public void setTransactionDetails(BalanceSummaryResponseDto balanceSummaryResponseDto, String currentCarType, int currentDutyStatus) {
        mTotRidesText.setText(String.valueOf(balanceSummaryResponseDto.getTotalrides()));
        mMileageText.setText(String.valueOf(balanceSummaryResponseDto.getMileage()));
        mOnlineDurationText.setText(String.valueOf(balanceSummaryResponseDto.getOnlineduration()));
        mRevenueText.setText(String.valueOf(balanceSummaryResponseDto.getRevenue()));

        Activity activity = getActivity();
        if (activity != null) {
            ((HomeActivity)activity).updateWallet(String.valueOf(balanceSummaryResponseDto.getBalance()));
        }
        

        setDutyControl(currentDutyStatus);
        onTypeChangeSuccess(currentCarType);
    }

    @Override
    public void setDutyControl(int currentDutyStatus) {
        dismissProgressbar();
        resetControlData();

        switch(currentDutyStatus) {
            case Constants.AppKey.OFF_DUTY:
                mOffDutyImg.setImageResource(R.drawable.ic_offdutyselect);
                mOffDutyText.setAlpha(1.0f);
                mOffDutyLayout.setClickable(false);

                break;
            case Constants.AppKey.ON_DUTY:
                mOnDutyImg.setImageResource(R.drawable.ic_ondutyselect);
                mOnDutyText.setAlpha(1.0f);
                mOnDutyLayout.setClickable(false);
                break;
            case Constants.AppKey.GO_HOME:
                mGoHomeImg.setImageResource(R.drawable.ic_gohomeselect);
                mGoHomeText.setAlpha(1.0f);
                mGoHomeLayout.setClickable(false);
                break;
        }

    }

    @Override
    public void setMapDetails(double lat, double lng) {
        if(mMap != null){
            LatLng currLatLng = new LatLng(lat, lng);

            if(mSource == null) {
                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_navigationarrow);

                final MarkerOptions mo1 = new MarkerOptions().position(currLatLng).title("Current").anchor(0.5f, 0.5f).icon(icon);
                mMarker = mMap.addMarker(mo1);
                mSource = mo1.getPosition();

                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .target(currLatLng)
                        .zoom(16)
                        .bearing(0)
                        .tilt(45)
                        .build()));
            }else {

                mSource = mMarker.getPosition();

                final LatLng startPosition = mSource;

                final LatLng finalPosition = currLatLng;

                mSource = startPosition;

                final Handler handler = new Handler();
                final long start = SystemClock.uptimeMillis();
                final Interpolator interpolator = new AccelerateDecelerateInterpolator();
                final float durationInMs = 1000;
                final boolean hideMarker = false;

                handler.post(new Runnable() {
                    long elapsed;
                    float t;
                    float v;

                    @Override
                    public void run() {
                        // Calculate progress using interpolator
                        elapsed = SystemClock.uptimeMillis() - start;
                        t = elapsed / durationInMs;
                        v = interpolator.getInterpolation(t);

                        LatLng currentPosition = new LatLng(
                                startPosition.latitude * (1 - t) + finalPosition.latitude * t,
                                startPosition.longitude * (1 - t) + finalPosition.longitude * t);

                        mMarker.setPosition(currentPosition);

                        // Repeat till progress is complete.
                        if (t < 1) {
                            // Post again 16ms later.
                            handler.postDelayed(this, 16);
                        } else {
                            if (hideMarker) {
                                mMarker.setVisible(false);
                            } else {
                                mMarker.setVisible(true);
                            }
                        }
                    }
                });
            }
        }
    }

   /* @Override
    public void showRequest(Order order) {
        Activity activity = getActivity();
        if (activity != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BundleKey.ORDER, order);

            Intent intent = new Intent(getActivity(), RequestActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, NAVIGATION_REQUEST_CODE);
        }
    }*/

    private void resetControlData() {
        mOffDutyImg.setImageResource(R.drawable.ic_offdutyunselect);
        mOnDutyImg.setImageResource(R.drawable.ic_ondutyunselect);
        mGoHomeImg.setImageResource(R.drawable.ic_gohomeunselect);
        mOffDutyText.setAlpha(0.4f);
        mOnDutyText.setAlpha(0.4f);
        mGoHomeText.setAlpha(0.4f);
        mOffDutyLayout.setClickable(true);
        mOnDutyLayout.setClickable(true);
        mGoHomeLayout.setClickable(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.car_type_layout:
                mCabSelectionFragment.onCarTypeClick();
                break;
            case R.id.off_duty_layout:
            case R.id.on_duty_layout:
                mMyHomePresenter.onDutyControlClick(this, (int)v.getTag());
                break;
            case R.id.go_home_layout:
                Activity activity = getActivity();
                if (activity != null) {

                }
                break;
            case R.id.fragment_my_home_current_address_iv:
                onCurrentLocationClick();
                break;
        }
    }

    private void onCurrentLocationClick() {
        if(mMap != null && mSource != null){
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                    .target(mSource)
                    .zoom(16)
                    .bearing(0)
                    .tilt(45)
                    .build()));
        }
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Activity activity = getActivity();
        if(activity != null) {
            if (requestCode == NAVIGATION_REQUEST_CODE && resultCode == activity.RESULT_OK) {
                ((HomeActivity) activity).showNavigation(false);
            }
        }
    }*/

    @Override
    public void onTypeChangeSuccess(String currentCarType) {
        if(currentCarType != null) {
            if (currentCarType.equals(getString(R.string.short_co))) {
                mCarTypeText.setText(getString(R.string.co));
                mCarTypeImg.setBackgroundResource(R.drawable.ic_minicarfill);
            } else if (currentCarType.equals(getString(R.string.short_se))) {
                mCarTypeText.setText(getString(R.string.se));
                mCarTypeImg.setBackgroundResource(R.drawable.ic_sudencarfil);

            } else if (currentCarType.equals(getString(R.string.short_sp))) {
                mCarTypeText.setText(getString(R.string.sp));
                mCarTypeImg.setBackgroundResource(R.drawable.ic_suvcarfill);
            }
        }
    }

    @Override
    public void onSetCabSelectionBack() {
        mIsBackConfirm = true;
    }


    public boolean isCabSelectionVisible() {
        return mIsBackConfirm;
    }

    public void onCloseCabSelection() {
        mIsBackConfirm = false;
        mCabSelectionFragment.closeCarTypesParentView();
    }
}
