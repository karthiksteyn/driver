package com.josh.driver.modules.ztest;

import android.content.Context;

/**
 * Created by hmspl on 13/3/17.
 */

public interface TestPresenter {
    void onLocationPermissionEnabled(Context context);

    void onTrackingPresenterCreate();
}
