package com.josh.driver.modules.user.resetpassword;

import android.content.Context;
import android.content.res.Resources;
import com.hm.presenter.BasePresenter;
import com.josh.driver.R;

/**
 * Created by hmspl on 28/4/17 at 10:58 AM.
 */

public class ResetPasswordPresenterImpl extends BasePresenter implements ResetPasswordPresenter, ResetPasswordModelImpl.RestPasswordModelListener {

    private ResetPasswordView mResetPasswordView;
    private ResetPasswordModel mResetPasswordModel;

    private Resources mResources;

    private ResetPasswordPresenterImpl(ResetPasswordView view) {
        super(view);
        mResetPasswordView = view;
        mResetPasswordModel = ResetPasswordModelImpl.getInstance(this);
    }

    public static ResetPasswordPresenter getInstance(ResetPasswordView resetPasswordView) {
        return new ResetPasswordPresenterImpl(resetPasswordView);
    }

    @Override
    public void onSaveNewPasswordClick(String newPassword, String confirmPassword, Context context) {
        mResources = context.getResources();
        mResetPasswordModel.ValidateFields(newPassword, confirmPassword, context);
    }


    public void onKeyDownPressed() {
        mResetPasswordView.hideSoftKeyboard();
        mResetPasswordView.hideBottomNavigation();
    }

    @Override
    public void checkScreen(String type) {
        mResetPasswordView.setScreenType(type);
    }

    @Override
    public void onFieldValidated(String field) {
        switch (field) {
            case ResetPasswordModelImpl.RestPasswordModelListener.SUCCESS:
                if (mResetPasswordView.hasNetworkConnection(mResources.getString(R.string.no_internet))) {
                    mResetPasswordView.showProgressbar();
                    mResetPasswordModel.saveNewPassword();
                }
                break;
            default:
                mResetPasswordView.showError(field);
                break;

        }
    }

    @Override
    public void onResetPasswordSuccess(String message) {
        mResetPasswordView.dismissProgressbar();
//        mResetPasswordView.showMessage(message);
        mResetPasswordView.navigateToLogin();
    }

    @Override
    public void onResetPasswordFailed(String message) {
        mResetPasswordView.dismissProgressbar();
        mResetPasswordView.showMessage(message);
    }
}
