package com.josh.driver.modules.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.hm.presenter.BasePresenter;
import com.josh.driver.dto.MenuDto;
import com.josh.driver.dto.response.Order;
import com.josh.driver.dto.response.TripDetailsResponseDto;

import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public class HomePresenterImpl extends BasePresenter implements HomePresenter, HomeModelImpl.HomeModelListener {

    private HomeView mHomeView;

    private HomeModel mHomeModel;

    public HomePresenterImpl(HomeView homeView) {
        super(homeView);
        mHomeView = homeView;
        mHomeModel = HomeModelImpl.getInstance(this);
    }

    public static HomePresenter getInstance(HomeView homeView) {
        return new HomePresenterImpl(homeView);
    }

    @Override
    public void onHomePresenterCreate(Bundle bundle, Context context) {
        mHomeModel.onHomeModelCreate(bundle, context);
    }

    @Override
    public String onGetDriverName() {
        return mHomeModel.retrieveDriverName();
    }

    @Override
    public String onGetDisplayPic() {
        return mHomeModel.retrieveDisplayPic();
    }

    @Override
    public float onGetBalance() {
        return mHomeModel.retrieveBalance();
    }

    @Override
    public void onLocationPermissionEnabled(Context context) {
        mHomeModel.onLocationPermissionEnabled(context);

    }

    @Override
    public boolean onGetTripDetails() {
        return mHomeModel.retrieveTripDetails();
    }

    @Override
    public int onGetStatus() {
        return mHomeModel.getTripStatus();
    }

    @Override
    public void onReceiverUnRegister() {
        mHomeModel.unRegisterReceiver();
    }

    @Override
    public boolean onIsProfileCreated() {
        return mHomeModel.isProfileCreated();
    }

    @Override
    public void onReceiverRegister() {
        mHomeModel.registerReceiver();

    }

    @Override
    public void onSetStartMenu(int currentPosition, String currentValue) {
        mHomeView.setStartMenu(currentPosition, currentValue);
    }

    @Override
    public void onMenuListDataFetchSuccess(String name, List<MenuDto> menuList) {
        mHomeView.setMenuData(name, menuList);

    }

    @Override
    public void onFetchLocationSuccess(double latitude, double longitude) {
        mHomeView.setMapDetails(latitude, longitude);
    }

    @Override
    public void onRequestSuccess(Order order) {
        Activity activity = getActivity();
        if(activity instanceof HomeActivity) {
            mHomeView.showRequest(order);
        }
    }

    @Override
    public void onForceRequestSuccess(TripDetailsResponseDto tripDetailsResponseDto) {
        mHomeView.showNavigation(true);
    }

    @Override
    public void onSignOutSuccess() {
        mHomeView.sessionExpired();
    }
}
