package com.josh.driver.modules.user.forgotpassword;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hm.activity.BaseActivity;
import com.josh.driver.R;
import com.josh.driver.dto.request.PaymentRequestDto;
import com.josh.driver.dto.request.SendOtpRequestDto;
import com.josh.driver.modules.user.otpverification.OtpVerificationActivity;

public class ForgotPasswordActivity extends BaseActivity implements ForgotPasswordView, View.OnClickListener {
    private ForgotPasswordPresenter mForgotPasswordPresenter;

    private Button mSendOtpBtn;

    private EditText mForgotPhoneEdit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        setupToolbar();

        mForgotPhoneEdit = (EditText) findViewById(R.id.activity_fragment_phone_number_et);
        mSendOtpBtn = (Button) findViewById(R.id.activity_forget_pwd_send_otp_btn);
        mSendOtpBtn.setOnClickListener(this);

        mForgotPasswordPresenter = ForgotPasswordPresenterImpl.getInstance(this);
    }

    private void setupToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageView optionIv = (ImageView) toolbar.findViewById(R.id.action_iv);
        ImageView profileIv = (ImageView) toolbar.findViewById(R.id.user_dp_iv);
        TextView titleTextView = (TextView) toolbar.findViewById(R.id.toolbar_title_tv);
        TextView balanceTextView = (TextView) toolbar.findViewById(R.id.toolbar_balance_tv);
        RelativeLayout walletLayout = (RelativeLayout) toolbar.findViewById(R.id.wallet_layout);

        optionIv.setImageResource(R.drawable.ic_back);
        optionIv.setOnClickListener(this);

        walletLayout.setVisibility(View.GONE);

        titleTextView.setText(getString(R.string.forgot_pass));
        profileIv.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_forget_pwd_send_otp_btn:
                mForgotPasswordPresenter.onSendClick(this);
                break;
            case R.id.action_iv:
                onBackPressed();
                break;
        }

    }

    @Override
    public void navigateToOtpVerification() {
        dismissProgressbar();
        Intent intent = new Intent(this, OtpVerificationActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public SendOtpRequestDto getPhoneNumber() {
        SendOtpRequestDto sendOtpRequestDto = new SendOtpRequestDto(getTrimmedText(mForgotPhoneEdit));
        return sendOtpRequestDto;
    }
}
