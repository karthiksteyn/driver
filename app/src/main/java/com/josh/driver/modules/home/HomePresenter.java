package com.josh.driver.modules.home;

import android.content.Context;
import android.os.Bundle;

/**
 * Created by hmspl on 13/3/17.
 */

public interface HomePresenter {
    void onHomePresenterCreate(Bundle bundle, Context context);

    String onGetDriverName();

    String onGetDisplayPic();

    float onGetBalance();

    void onLocationPermissionEnabled(Context context);

    boolean onGetTripDetails();

    int onGetStatus();

    void onReceiverUnRegister();

    boolean onIsProfileCreated();

    void onReceiverRegister();
}
