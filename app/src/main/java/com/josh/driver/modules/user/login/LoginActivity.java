package com.josh.driver.modules.user.login;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hm.activity.BaseActivity;
import com.josh.driver.AppSnippet;
import com.josh.driver.R;
import com.josh.driver.dto.request.LoginRequestDto;
import com.josh.driver.modules.home.HomeActivity;
import com.josh.driver.modules.user.forgotpassword.ForgotPasswordActivity;

public class LoginActivity extends BaseActivity implements LoginView,
        View.OnClickListener, TextView.OnEditorActionListener {

    private LoginPresenter mLoginPresenter;

    private EditText mPhNumberEditText;
    private EditText mPwdEditText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLoginPresenter = LoginPresenterImpl.getInstance(this);
        if(mLoginPresenter.onIsProfileCreated()){
            navigateToHome();
        }else {

            AppSnippet.setTheme(this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                this.getWindow().setStatusBarColor(getResourceColor(R.color.transparent_80));
            }

            setContentView(R.layout.activity_login);

            mPhNumberEditText = (EditText) findViewById(R.id.phone_edit);
            mPwdEditText = (EditText) findViewById(R.id.password_edit);
            mPhNumberEditText.setOnEditorActionListener(this);
            mPwdEditText.setOnEditorActionListener(this);
            final Button signInBtn = (Button) findViewById(R.id.sign_in_btn);
            signInBtn.setOnClickListener(this);

            final TextView forgotPwdText = (TextView) findViewById(R.id.forgot_pwd_tv);
            forgotPwdText.setOnClickListener(this);

        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.sign_in_btn:
                mLoginPresenter.onLoginClick(this);
                break;
            case R.id.forgot_pwd_tv:
                navigateToForgotPwd();
                break;
        }

    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                || (actionId == EditorInfo.IME_ACTION_DONE)) {

            mLoginPresenter.onLoginClick(this);

        }else if (actionId == EditorInfo.IME_ACTION_NEXT){
            mLoginPresenter.onLoginClick(this);
        }
        return false;
    }

    @Override
    public LoginRequestDto getLoginDetails() {
        LoginRequestDto loginRequestDto = new LoginRequestDto();
        loginRequestDto.setMobile(getTrimmedText(mPhNumberEditText));
        loginRequestDto.setPassword(getTrimmedText(mPwdEditText));
        return loginRequestDto;
    }

    @Override
    public void navigateToHome() {
        dismissProgressbar();
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToForgotPwd() {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

}
