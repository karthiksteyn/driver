package com.josh.driver.modules.splash;


import android.os.Handler;

import com.hm.presenter.BasePresenter;

public class SplashPresenterImpl extends BasePresenter implements SplashPresenter,
        SplashModelImpl.SplashModelListener {

    private final long SPLASH_DELAY_TIME = 3000;

    private SplashView mSplashView;

    private SplashModel mSplashModel;

    private SplashPresenterImpl(SplashView splashView) {
        super(splashView);
        mSplashView = splashView;
        mSplashModel = SplashModelImpl.getInstance(this);
    }

    public static SplashPresenter getInstance(SplashView splashView) {
        return new SplashPresenterImpl(splashView);
    }

    @Override
    public void onCreateSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                proceedForNavigation();
            }
        }, SPLASH_DELAY_TIME);
    }

    /**
     * Conditions for navigation to next screen is checked here and navigation to
     * respective screen is called from here
     */
    private void proceedForNavigation() {

    }
}
