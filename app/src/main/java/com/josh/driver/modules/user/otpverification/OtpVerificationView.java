package com.josh.driver.modules.user.otpverification;

import com.hm.activity.View;

/**
 * Created by hmspl on 13/3/17.
 */

public interface OtpVerificationView extends View {
    void setPhoneNumber(String s);

    String getVerificationCode();

    void NavigateToResetPwd();

    void startTimer();
}
