package com.josh.driver.modules.ztest;

import android.content.Context;

import com.hm.presenter.BasePresenter;

/**
 * Created by hmspl on 13/3/17.
 */

public class TestPresenterImpl extends BasePresenter implements TestPresenter, TestModelImpl.TestModelListener {

    private TestView mTestView;

    private TestModel mTestModel;

    public TestPresenterImpl(TestView testView) {
        super(testView);
        mTestView = testView;
        mTestModel = TestModelImpl.getInstance(this);
    }

    public static TestPresenter getInstance(TestView testView) {
        return new TestPresenterImpl(testView);
    }

    @Override
    public void onLocationPermissionEnabled(Context context) {
        mTestModel.onLocationPermissionEnabled(context);

    }

    @Override
    public void onTrackingPresenterCreate() {
        mTestModel.onTrackingModelCreate();

    }

    @Override
    public void onSuccess(String msg, double latitude, double longitude) {
        mTestView.setMapDetails(latitude, longitude);
//        mTestView.showMessage(msg);
    }
}
