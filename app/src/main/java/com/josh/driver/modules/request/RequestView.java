package com.josh.driver.modules.request;

import com.google.android.gms.maps.model.LatLng;
import com.hm.activity.View;
import com.josh.driver.dto.response.Order;

/**
 * Created by hmspl on 13/3/17.
 */

public interface RequestView extends View {
    void setRequestDetails(Order order);

    void navigateToNavigation();

    void setMapDetails(LatLng currLatLng, LatLng pickUpLatLng);

    void navigateToHome();
}
