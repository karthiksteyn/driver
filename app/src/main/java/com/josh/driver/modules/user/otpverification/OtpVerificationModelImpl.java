package com.josh.driver.modules.user.otpverification;

import android.content.Context;

import com.hm.model.BaseModel;
import com.josh.driver.R;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.request.SendOtpRequestDto;
import com.josh.driver.dto.request.VerifyOtpRequestDto;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;


/**
 * Created by hmspl on 13/3/17.
 */

public class OtpVerificationModelImpl extends BaseModel implements OtpVerificationModel, ModelHandler.ModelHanlderListener<ApiStatusDto> {

    private OtpVerificationModelListener mOtpVerificationModelListener;
    private String mPhoneNumber;

    private String mVerificationCode;

    private Context mContext;

    public OtpVerificationModelImpl(OtpVerificationModelListener otpVerificationModelListener) {
        super(otpVerificationModelListener);

        mOtpVerificationModelListener = otpVerificationModelListener;
    }

    public static OtpVerificationModel getInstance(OtpVerificationModelListener otpVerificationModelListener) {
        return new OtpVerificationModelImpl(otpVerificationModelListener);

    }

    @Override
    public String getPhoneNumber() {
        //Retrieve phone number
        return DataHandler.getInstance().retrieveDriverPhone();
    }

    @Override
    public void verifyUser(String verificationCode, Context context) {
        mContext = context;

        mVerificationCode = verificationCode;

        if (hasValidVerificationCode(verificationCode)) {
            VerifyOtpRequestDto verifyOtpRequestDto = new VerifyOtpRequestDto();
            verifyOtpRequestDto.setMobile(DataHandler.getInstance().retrieveDriverPhone());
            verifyOtpRequestDto.setOtp(verificationCode);

            ModelHandler.getInstance().enqueueTask(this,
                    RequestCreator.getInstance().getVerificationRequest(verifyOtpRequestDto));
        }
    }

    @Override
    public void resendVerificationCode() {
        ModelHandler.getInstance().enqueueTask(new ModelHandler.ModelHanlderListener<ApiStatusDto>() {
                                                   @Override
                                                   public void onSuccessApi(ApiStatusDto apiStatusDto) {
                                                       mOtpVerificationModelListener.onResendVerificationCodeReply(apiStatusDto.getDescription());

                                                   }

                                                   @Override
                                                   public void onFailureApi(int errorCode, String errorMessage) {
                                                       mOtpVerificationModelListener.onResendVerificationCodeReply(errorMessage);
                                                   }

                                                   @Override
                                                   public void onAuthorizationFailed() {
                                                       OtpVerificationModelImpl.this.onAuthorizationFailed();
                                                   }
                                               }
                , RequestCreator.getInstance().onSendOtp(new SendOtpRequestDto(DataHandler.getInstance().retrieveDriverPhone())));
    }

    @Override
    public void savePhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    private boolean hasValidVerificationCode(String verificationCode) {
        if (verificationCode.isEmpty()) {
            mOtpVerificationModelListener.onVerificationFailed(mContext.getString(R.string.verification_empty));
            return false;
        } else if (verificationCode.length() < 4) {
            mOtpVerificationModelListener.onVerificationFailed(mContext.getString(R.string.verification_failed));
            return false;
        }
        return true;
    }

    @Override
    public void onSuccessApi(ApiStatusDto response) {
        if (response.isResultOk()) {
            DataHandler dataHandler = DataHandler.getInstance();

            dataHandler.saveOtp(mVerificationCode);
            mOtpVerificationModelListener.onVerificationSuccess();
        } else {
            mOtpVerificationModelListener
                    .onVerificationFailed(response.getDescription());
        }
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mOtpVerificationModelListener
                .onVerificationFailed(errorMessage);
    }

    @Override
    public void onAuthorizationFailed() {

    }

    interface OtpVerificationModelListener extends ModelListener {

        void onVerificationFailed(String msg);

        void onVerificationSuccess();

        void onResendVerificationCodeReply(String msg);
    }
}
