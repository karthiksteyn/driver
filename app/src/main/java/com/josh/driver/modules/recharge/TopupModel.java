package com.josh.driver.modules.recharge;

import android.content.Context;

import com.josh.driver.dto.request.PaymentRequestDto;

/**
 * Created by hmspl on 13/3/17.
 */

public interface TopupModel {

    void doPayment(PaymentRequestDto paymentRequestDto, Context context);

    void fetchContact();
}
