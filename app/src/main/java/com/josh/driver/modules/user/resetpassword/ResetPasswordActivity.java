package com.josh.driver.modules.user.resetpassword;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hm.activity.BaseActivity;
import com.hm.widgets.CustomEditText;
import com.josh.driver.AppSnippet;
import com.josh.driver.R;

public class ResetPasswordActivity extends BaseActivity implements ResetPasswordView, CustomEditText.CustomEditTextKeyboardListener, View.OnClickListener, TextView.OnEditorActionListener {

    private ResetPasswordPresenter mResetPasswordPresenter;
    private CustomEditText mNewPasswordEt;
    private CustomEditText mConfirmPasswordEt;
    private Button mSavePasswordBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        mResetPasswordPresenter = ResetPasswordPresenterImpl.getInstance(this);

        mNewPasswordEt = (CustomEditText) findViewById(R.id.activity_reset_new_password_et);
        mConfirmPasswordEt = (CustomEditText) findViewById(R.id.activity_reset_confirm_password_et);
        mSavePasswordBtn = (Button) findViewById(R.id.activity_reset_pwd_save_btn);

//        mNewPasswordEt.setCustomKeyBoardListener(this);
//        mConfirmPasswordEt.setCustomKeyBoardListener(this);
        mSavePasswordBtn.setOnClickListener(this);
        mNewPasswordEt.setOnEditorActionListener(this);
        mConfirmPasswordEt.setOnEditorActionListener(this);

        mResetPasswordPresenter.checkScreen(/*getIntent().getExtras().getString(Constants.ScreenType.CHANGE_PASSWORD_SCREEN)*/"");
    }

    @Override
    public void setScreenType(String type) {
        setupToolbar();
//        if (type.equals(Constants.Title.RESET_PASSWORD)) {
        /*mOldPassword.setVisibility(View.GONE);
        findViewById(R.id.activity_reset_old_password_header).setVisibility(View.GONE);*/
//        }
    }

    @Override
    public void navigateToLogin() {
        finish();
    }

    private void setupToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageView optionIv = (ImageView) toolbar.findViewById(R.id.action_iv);
        ImageView profileIv = (ImageView) toolbar.findViewById(R.id.user_dp_iv);
        TextView titleTextView = (TextView) toolbar.findViewById(R.id.toolbar_title_tv);
        TextView balanceTextView = (TextView) toolbar.findViewById(R.id.toolbar_balance_tv);
        RelativeLayout walletLayout = (RelativeLayout) toolbar.findViewById(R.id.wallet_layout);

        optionIv.setImageResource(R.drawable.ic_back);
        optionIv.setOnClickListener(this);

        walletLayout.setVisibility(View.GONE);

        titleTextView.setText(getString(R.string.reset_pwd));
        profileIv.setVisibility(View.GONE);

    }

    @Override
    protected void onStart() {
        super.onStart();
//        AppSnippet.hideBottomNavigation(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_reset_pwd_save_btn:
                mResetPasswordPresenter.onSaveNewPasswordClick(mNewPasswordEt.getText().toString(), mConfirmPasswordEt.getText().toString(), this);
                break;
        }
    }

    private void setNotFillAnimation(View view) {
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
        view.startAnimation(shake);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            mResetPasswordPresenter.onKeyDownPressed();
            mResetPasswordPresenter.onSaveNewPasswordClick(mNewPasswordEt.getText().toString(), mConfirmPasswordEt.getText().toString(), this);
            return true;
        }
        return false;
    }

    @Override
    public void showError(String field) {
        switch (field) {
            case ResetPasswordModelImpl.RestPasswordModelListener.NEW_PASSWORD_ERROR:
                setNotFillAnimation(mNewPasswordEt);
                break;
            case ResetPasswordModelImpl.RestPasswordModelListener.CONFIRM_PASSWORD_ERROR:
                setNotFillAnimation(mConfirmPasswordEt);
                break;
        }
    }

    @Override
    public void hideBottomNavigation() {
//        AppSnippet.hideBottomNavigation(this);
    }

    @Override
    public void onKeyDownPressed() {
        mResetPasswordPresenter.onKeyDownPressed();
    }
}
