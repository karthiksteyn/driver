package com.josh.driver.modules.navigation;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.hm.fragment.BaseFragment;
import com.hm.presenter.BasePresenter;
import com.josh.driver.R;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.googlemap.PolyLineDto;
import com.josh.driver.modules.home.HomeModel;
import com.josh.driver.modules.home.HomeModelImpl;
import com.josh.driver.modules.home.HomePresenterImpl;
import com.josh.driver.modules.home.HomeView;

import java.util.List;

/**
 * Created by hmspl on 30/3/17.
 */

public class NavigationPresenterImpl extends BasePresenter implements NavigationPresenter, NavigationModelImpl.NavigationModelListener {

    private NavigationView mNavigationView;

    private NavigationModel mNavigationModel;

    private Context mContext;

    public NavigationPresenterImpl(NavigationView navigationView) {
        super(navigationView);
        mNavigationView = navigationView;
        mNavigationModel = NavigationModelImpl.getInstance(this);
    }
    public static NavigationPresenter getInstance(NavigationView navigationView) {
        return new NavigationPresenterImpl(navigationView);

    }

    @Override
    public void onNavigationPresenterCreate(NavigationFragment navigationFragment) {
        mContext = navigationFragment.getContext();
        mNavigationModel.onNavigationModelCreate(mContext);
    }

    @Override
    public void onCancelClick() {
        if(mNavigationView.hasNetworkConnection(mContext.getString(R.string.no_internet))){
            mNavigationView.showProgressbar();
//            mNavigationModel.fetchCancelReason();
            mNavigationModel.cancelTrip();
        }

    }

    @Override
    public void onMorphingClick() {
        if(mNavigationView.hasNetworkConnection(mContext.getString(R.string.no_internet))){
            mNavigationView.showProgressbar();
            mNavigationModel.processTrip();
        }
    }

    @Override
    public void onCallClick() {
        if(mNavigationView.hasNetworkConnection(mContext.getString(R.string.no_internet))){
            mNavigationView.showProgressbar();
            mNavigationModel.makeCall();
        }
    }

    @Override
    public LatLng onGetCoOrdinates() {
       return mNavigationModel.getCoOrdinates();

    }

    @Override
    public void onNavigationClick() {
        if(mNavigationView.hasNetworkConnection(mContext.getString(R.string.no_internet))){
            mNavigationView.showProgressbar();
            mNavigationModel.fetchRoute(mContext.getResources().getString(R.string.google_api_key));
        }
    }

    @Override
    public void onChangeMarker() {
        mNavigationModel.getChangeMarker();
    }

    @Override
    public String onGetDropLocation() {
        return mNavigationModel.fetchDropAddress();
    }

    @Override
    public void onSetTripDetails(TripDetailsResponseDto mTripDetailsResponseDto) {
        mNavigationView.setTripDetails(mTripDetailsResponseDto);
    }

    @Override
    public void onCancelSuccess() {
        mNavigationView.onNavigateHome();
    }

    @Override
    public void onFailed(String message) {
        mNavigationView.dismissProgressbar();
        mNavigationView.showMessage(message);
    }

    @Override
    public void onChangeStatusSuccess(int status) {
        mNavigationView.onSetMorphingButtonData(status);
    }

    @Override
    public void onFetchCoOrdinatesSuccess(double lat, double lng) {
        mNavigationView.setMapDetails(lat, lng);

    }

    @Override
    public void setDirections(List<PolyLineDto> polyLineDtos) {
        mNavigationView.dismissProgressbar();
        mNavigationView.drawDirections(polyLineDtos);
    }

    @Override
    public void onNoRoutesAvailable() {
        mNavigationView.dismissProgressbar();
        mNavigationView.removeRoutes();
    }

    @Override
    public void onFetchSuccess(LatLng toLatLng) {
        mNavigationView.dismissProgressbar();
        mNavigationView.showMapNavigator(toLatLng);
    }

    @Override
    public void onCloseProgress() {
        mNavigationView.dismissProgressbar();
    }
}
