package com.josh.driver.modules.myhome;


import android.content.Context;

import com.josh.driver.dto.ProfileDto;
import com.josh.driver.dto.UserDto;

/**
 * Created by hmspl on 13/3/17.
 */

public interface MyHomeModel {

    void onMyHomeModelCreate();

    void changeControl(int tag);
}
