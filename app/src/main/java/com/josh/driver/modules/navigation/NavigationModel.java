package com.josh.driver.modules.navigation;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.hm.model.Model;

/**
 * Created by hmspl on 30/3/17.
 */

public interface NavigationModel extends Model {
    void onNavigationModelCreate(Context context);

    void fetchCancelReason();

    void cancelTrip();

    void processTrip();

    void makeCall();

    LatLng getCoOrdinates();

    String fetchDropAddress();

    void fetchRoute(String apiKey);

    void getChangeMarker();
}
