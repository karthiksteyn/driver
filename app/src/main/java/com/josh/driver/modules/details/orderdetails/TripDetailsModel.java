package com.josh.driver.modules.details.orderdetails;

import android.content.Context;
import android.os.Bundle;

import com.josh.driver.dto.response.TripDetailsResponseDto;

/**
 * Created by hmspl on 13/3/17.
 */

public interface TripDetailsModel {
    void fetchTripDetails(Bundle bundle, Context context);

    TripDetailsResponseDto getTripDetails();
}
