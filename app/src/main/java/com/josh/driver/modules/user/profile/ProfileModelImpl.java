package com.josh.driver.modules.user.profile;

import com.hm.HmLibrary;
import com.hm.model.BaseModel;
import com.josh.driver.Constants;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.Headers;
import com.josh.driver.dto.ProfileDto;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.socket.SocketHandler;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public class ProfileModelImpl extends BaseModel implements ProfileModel, ModelHandler.ModelHanlderListener<ApiStatusDto> {

    private ProfileModelListener mProfileModelListener;

    private ProfileDto mProfileDto;

    private String mProfilePicPath;

    private boolean mProfilePicUpload;

    private long mProfilePicUpdatedTime;

    private SocketHandler mSocketHandler;


    public ProfileModelImpl(ProfileModelListener profileModelListener) {
        super(profileModelListener);

        mProfileModelListener = profileModelListener;
    }

    public static ProfileModel getInstance(ProfileModelListener profileModelListener) {
        return new ProfileModelImpl(profileModelListener);

    }

    @Override
    public ProfileDto getProfileData() {
        mProfileDto = DataHandler.getInstance().retrieveProfileDetails();
        return mProfileDto;
    }

    @Override
    public void clearCommonData() {
        String driverId = DataHandler.getInstance().retrieveDriverId();
        Headers headers = getTokens();

        ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().getLogOutRequest(driverId, headers));
    }

    private Headers getTokens() {
        Headers headers = new Headers();
        headers.setAuthToken(DataHandler.getInstance().retrieveAuthToken());
        headers.setUserToken(DataHandler.getInstance().retrieveUserToken());
        headers.setAppToken(Constants.AppKey.APP_TOKEN);
        return headers;
    }

    @Override
    public void onSuccessApi(ApiStatusDto response) {
        if (response.isResultOk()) {
            HmLibrary.getHmLibrary().clearAllData();
            mSocketHandler = SocketHandler.getInstance();
            if(mSocketHandler != null){
                mSocketHandler.disconnectSocket();
            }

            mProfileModelListener.onLogOutSuccess();

        } else {
            mProfileModelListener.onLogOutFailed(response.getDescription());
        }
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mProfileModelListener.onLogOutFailed(errorMessage);

    }

    @Override
    public void onAuthorizationFailed() {

    }

    interface ProfileModelListener extends ModelListener {

        void onLogOutSuccess();

        void onLogOutFailed(String message);

    }
}
