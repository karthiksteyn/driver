package com.josh.driver.modules.user.login;

import android.content.Context;

/**
 * Created by hmspl on 13/3/17.
 */

public interface LoginPresenter {

    void onLoginClick(Context context);

    void onSignUpClick();

    boolean onIsProfileCreated();
}
