package com.josh.driver.modules.details.pastorders;

/**
 * Created by hmspl on 13/3/17.
 */

public interface PastTripsModel {
    void fetchPastTrips();
}
