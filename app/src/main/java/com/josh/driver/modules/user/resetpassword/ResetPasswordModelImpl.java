package com.josh.driver.modules.user.resetpassword;

import android.content.Context;

import com.hm.model.BaseModel;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.request.ResetPwdRequestDto;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

/**
 * Created by hmspl on 28/4/17 at 10:58 AM.
 */

public class ResetPasswordModelImpl extends BaseModel implements ResetPasswordModel {

    private RestPasswordModelListener mRestPasswordModelListener;
    private String mNewPassword, mOldPassword;

    private Context mContext;

    public ResetPasswordModelImpl(RestPasswordModelListener modelListener) {
        super(modelListener);
        mRestPasswordModelListener = modelListener;
    }

    public static ResetPasswordModel getInstance(RestPasswordModelListener listener) {
        return new ResetPasswordModelImpl(listener);
    }

    @Override
    public void ValidateFields(String newPassword, String confirmPassword, Context context) {
        mNewPassword = newPassword;
        mContext = context;
        if (newPassword.length() < 8) {
            mRestPasswordModelListener.onFieldValidated(RestPasswordModelListener.NEW_PASSWORD_ERROR);
        } else if (!confirmPassword.equals(newPassword)) {
            mRestPasswordModelListener.onFieldValidated(RestPasswordModelListener.CONFIRM_PASSWORD_ERROR);
        } else {
            mRestPasswordModelListener.onFieldValidated(RestPasswordModelListener.SUCCESS);
        }
    }

    @Override
    public void saveNewPassword() {
        DataHandler dataHandler = DataHandler.getInstance();
        ResetPwdRequestDto resetPwdRequestDto = new ResetPwdRequestDto();
        resetPwdRequestDto.setMobile(dataHandler.retrieveDriverPhone());
        resetPwdRequestDto.setOtp(dataHandler.retrieveOtp());
        resetPwdRequestDto.setPassword(mNewPassword);

        ModelHandler.getInstance().enqueueTask(new ModelHandler.ModelHanlderListener<ApiStatusDto>() {
                                                   @Override
                                                   public void onSuccessApi(ApiStatusDto apiStatusDto) {
                                                       if(apiStatusDto.isResultOk()) {
                                                           mRestPasswordModelListener.onResetPasswordSuccess(apiStatusDto.getDescription());
                                                       }else{
                                                           mRestPasswordModelListener.onResetPasswordFailed(apiStatusDto.getDescription());
                                                       }
                                                   }

                                                   @Override
                                                   public void onFailureApi(int errorCode, String errorMessage) {
                                                       mRestPasswordModelListener.onResetPasswordFailed(errorMessage);
                                                   }

                                                   @Override
                                                   public void onAuthorizationFailed() {
                                                       mModelListener.onAuthorizationFailed();
                                                   }
                                               }
                , RequestCreator.getInstance().getResetPwdRequest(resetPwdRequestDto));
    }

    public interface RestPasswordModelListener extends ModelListener {

        String OLD_PASSWORD_ERROR = "oldPasswordError";
        String NEW_PASSWORD_ERROR = "newPassword";
        String CONFIRM_PASSWORD_ERROR = "confirmPassword";
        String SUCCESS = "success";

        void onFieldValidated(String field);

        void onResetPasswordSuccess(String message);

        void onResetPasswordFailed(String message);
    }
}
