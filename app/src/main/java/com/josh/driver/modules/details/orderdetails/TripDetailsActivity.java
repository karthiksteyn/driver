package com.josh.driver.modules.details.orderdetails;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.hm.activity.BaseActivity;
import com.josh.driver.R;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.modules.AdditionalSettingsActivity;

public class TripDetailsActivity extends BaseActivity implements TripDetailsView, View.OnClickListener {

    private TripDetailsPresenter mTripDetailsPresenter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_details);

        mTripDetailsPresenter = TripDetailsPresenterImpl.getInstance(this);

        mTripDetailsPresenter.onTripDetailsPresenterCreate(getIntent().getExtras(), this);

    }

    @Override
    public void setTripDetails(TripDetailsResponseDto tripDetailsResponseDto) {
        //Update the view with Trip Details

    }

    @Override
    public void navigateToAdditionalSettings(TripDetailsResponseDto tripDetailsResponseDto) {
        Intent intent = new Intent(getContext(), AdditionalSettingsActivity.class);
        /**
         * get Additional details and put into bundle
         */
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        mTripDetailsPresenter.onClickAdditionalSettings();
    }
}
