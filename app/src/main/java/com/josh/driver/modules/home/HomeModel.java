package com.josh.driver.modules.home;

import android.content.Context;
import android.os.Bundle;

/**
 * Created by hmspl on 13/3/17.
 */

public interface HomeModel {
    void onHomeModelCreate(Bundle bundle, Context context);

    String retrieveDriverName();

    String retrieveDisplayPic();

    float retrieveBalance();

    void onLocationPermissionEnabled(Context context);

    boolean retrieveTripDetails();

    int getTripStatus();

    void unRegisterReceiver();

    boolean isProfileCreated();

    void registerReceiver();
}
