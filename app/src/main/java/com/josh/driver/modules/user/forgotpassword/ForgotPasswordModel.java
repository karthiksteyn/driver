package com.josh.driver.modules.user.forgotpassword;

import android.content.Context;
import android.os.Bundle;

import com.josh.driver.dto.request.PaymentRequestDto;
import com.josh.driver.dto.request.SendOtpRequestDto;
import com.josh.driver.dto.response.TripDetailsResponseDto;

/**
 * Created by hmspl on 13/3/17.
 */

public interface ForgotPasswordModel {

    void sendDetails(SendOtpRequestDto sendOtpRequestDto, Context context);
}
