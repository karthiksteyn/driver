package com.josh.driver.modules.billpayment;

import android.content.Context;

import com.hm.presenter.BasePresenter;
import com.josh.driver.R;
import com.josh.driver.dto.response.TripDetailsResponseDto;

/**
 * Created by hmspl on 13/3/17.
 */

public class BillPresenterImpl extends BasePresenter implements BillPresenter, BillModelImpl.BillModelListener {

    private BillView mBillView;

    private BillModel mBillModel;

    private Context mContext;

    public BillPresenterImpl(BillView billView) {
        super(billView);
        mBillView = billView;
        mBillModel = BillModelImpl.getInstance(this);
    }

    public static BillPresenter getInstance(BillView billView) {
        return new BillPresenterImpl(billView);
    }

    @Override
    public void onBillPresenterCreate(BillFragment billFragment) {
        mContext = billFragment.getContext();
        mBillModel.onBillModelCreate(mContext);
    }

    @Override
    public void onSubmitButtonClick() {
        if(mBillView.hasNetworkConnection(mContext.getString(R.string.no_internet))){
            mBillView.showProgressbar();
            mBillModel.completeTrip();
        }
    }

    @Override
    public String getTime(String time) {
        return mBillModel.extractTime(time);
    }

    @Override
    public String getDate(String time) {
        return mBillModel.extractDate(time);
    }

    @Override
    public void onSetTripDetails(TripDetailsResponseDto mTripDetailsResponseDto) {
        mBillView.setTripDetails(mTripDetailsResponseDto);
    }

    @Override
    public void onSuccess() {
        mBillView.navigateToHome();
    }

    @Override
    public void onFailed(String message) {
        mBillView.dismissProgressbar();
        mBillView.showMessage(message);
    }
}
