package com.josh.driver.modules.user.forgotpassword;

import android.content.Context;
import android.content.res.Resources;

import com.hm.presenter.BasePresenter;
import com.josh.driver.R;

/**
 * Created by hmspl on 13/3/17.
 */

public class ForgotPasswordPresenterImpl extends BasePresenter implements ForgotPasswordPresenter,
        ForgotPasswordModelImpl.ForgotPasswordModelListener {

    private ForgotPasswordView mForgotPasswordView;

    private ForgotPasswordModel mForgotPasswordModel;

    private Resources mResources;

    public ForgotPasswordPresenterImpl(ForgotPasswordView forgotPasswordView) {
        super(forgotPasswordView);
        mForgotPasswordView = forgotPasswordView;
        mForgotPasswordModel = ForgotPasswordModelImpl.getInstance(this);
    }

    public static ForgotPasswordPresenter getInstance(ForgotPasswordView forgotPasswordView) {
        return new ForgotPasswordPresenterImpl(forgotPasswordView);
    }

    @Override
    public void onFailed(String message) {
        mForgotPasswordView.dismissProgressbar();
        mForgotPasswordView.showMessage(message);
    }

    @Override
    public void onSuccess(String message) {
        mForgotPasswordView.navigateToOtpVerification();
    }

    @Override
    public void onSendClick(Context context) {
        mResources = context.getResources();

        if (mForgotPasswordView.hasNetworkConnection(mResources.getString(R.string.no_internet))) {
            mForgotPasswordView.showProgressbar();
            mForgotPasswordModel.sendDetails(mForgotPasswordView.getPhoneNumber(), context);
        }
    }
}
