package com.josh.driver.modules.user.forgotpassword;

import com.hm.activity.View;
import com.josh.driver.dto.request.PaymentRequestDto;
import com.josh.driver.dto.request.SendOtpRequestDto;
import com.josh.driver.dto.response.TripDetailsResponseDto;

/**
 * Created by hmspl on 13/3/17.
 */

public interface ForgotPasswordView extends View {

    void navigateToOtpVerification();

    SendOtpRequestDto getPhoneNumber();
}
