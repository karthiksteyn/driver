package com.josh.driver.modules.user.profile;

import com.hm.presenter.BasePresenter;
import com.josh.driver.R;

/**
 * Created by hmspl on 13/3/17.
 */

public class ProfilePresenterImpl extends BasePresenter implements ProfilePresenter, ProfileModelImpl.ProfileModelListener {

    private ProfileView mProfileView;

    private ProfileModel mProfileModel;

    public ProfilePresenterImpl(ProfileView profileView) {
        super(profileView);
        mProfileView = profileView;
        mProfileModel = ProfileModelImpl.getInstance(this);
    }

    public static ProfilePresenter getInstance(ProfileView profileView) {
        return new ProfilePresenterImpl(profileView);
    }

    @Override
    public void onProfilePresenterCreate() {
        mProfileView.setDetails(mProfileModel.getProfileData());
    }

    @Override
    public void onLogoutClick() {
        if (mProfileView.hasNetworkConnection(getContext().getString(R.string.no_internet))) {
            mProfileView.showProgressbar();
            mProfileModel.clearCommonData();
        }
    }

    @Override
    public void onTakeImageClick() {
        mProfileView.checkStoragePermission();
    }

    @Override
    public void onLogOutSuccess() {
        mProfileView.navigateToLogin();
    }

    @Override
    public void onLogOutFailed(String message) {
        mProfileView.dismissProgressbar();
        mProfileView.showMessage(message);
    }
}
