package com.josh.driver.modules.details.pastorders;

import com.hm.presenter.BasePresenter;
import com.josh.driver.Constants;
import com.josh.driver.dto.PastTripDto;

import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public class PastTripsPresenterImpl extends BasePresenter implements PastTripsPresenter, PastTripsModelImpl.PastTripsModelListener {

    private PastTripsView mPastTripsView;

    private PastTripsModel mPastTripsModel;

    public PastTripsPresenterImpl(PastTripsView pastTripsView) {
        super(pastTripsView);
        mPastTripsView = pastTripsView;
        mPastTripsModel = PastTripsModelImpl.getInstance(this);
    }

    public static PastTripsPresenter getInstance(PastTripsView pastTripsView) {
        return new PastTripsPresenterImpl(pastTripsView);
    }

    @Override
    public void onPastTripsPresenterCreate() {
        if (mPastTripsView.hasNetworkConnection(Constants.Message.NO_INTERNET)) {
            mPastTripsModel.fetchPastTrips();
        }
    }

    @Override
    public void onTripsFetchSuccess(List<PastTripDto> pastTripList) {
        mPastTripsView.setPastTrips(pastTripList);

    }

    @Override
    public void onTripsFetchFailed(String message) {
        mPastTripsView.dismissProgressbar();
        mPastTripsView.showMessage(message);
    }
}
