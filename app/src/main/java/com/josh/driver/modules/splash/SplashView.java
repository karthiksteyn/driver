package com.josh.driver.modules.splash;


import com.hm.activity.View;

public interface SplashView extends View {

    void navigateToWalkThrough();

    void navigateToLogin();

    void navigateToOtpVerification();

    void navigateToProfileSetup();

    void navigateToMenu();


}
