package com.josh.driver.modules.recharge;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hm.Log;
import com.hm.fragment.BaseFragment;
import com.hm.handler.PermissionHandler;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.dto.ContactDto;
import com.josh.driver.dto.request.PaymentRequestDto;
import com.josh.driver.handler.InputFilterMinMax;
import com.josh.driver.modules.ContactFragment;

import java.util.ArrayList;
import java.util.List;

public class TopupFragment extends BaseFragment implements TopupView, View.OnClickListener,
        ContactFragment.ContactSelectListener {

    private static final String TAG = TopupFragment.class.getSimpleName();
    private TopupPresenter mTopupPresenter;

    private RelativeLayout mPaytmBtn;
    private EditText mNameEditText, mPhNumberEditText, mAmtEditText;
    private ImageView mPhoneBookImg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTopupPresenter = TopupPresenterImpl.getInstance(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_topup, container, false);

        mNameEditText = (EditText) view.findViewById(R.id.fragment_topup_name_edit);
        mPhNumberEditText = (EditText) view.findViewById(R.id.fragment_topup_phone_edit);
        mAmtEditText = (EditText) view.findViewById(R.id.fragment_topup_amt_edit);
        mPhoneBookImg = (ImageView) view.findViewById(R.id.fragment_topup_phone_book_iv);
        mPhoneBookImg.setOnClickListener(this);
        mPaytmBtn = (RelativeLayout) view.findViewById(R.id.paytm_btn);
        mPaytmBtn.setOnClickListener(this);

        mAmtEditText.setFilters(new InputFilter[]{ new InputFilterMinMax("1", "5000")});
        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.paytm_btn:
                mTopupPresenter.onPaymentClick(this);
                break;
            case R.id.fragment_topup_phone_book_iv:
                mTopupPresenter.onPhoneBookClick();
        }
    }

    @Override
    public PaymentRequestDto getPaymentDetails() {
        PaymentRequestDto paymentRequestDto = new PaymentRequestDto();
        paymentRequestDto.setName(getTrimmedText(mNameEditText));
        paymentRequestDto.setPhone(getTrimmedText(mPhNumberEditText));
        paymentRequestDto.setAmt(getTrimmedText(mAmtEditText));
        return paymentRequestDto;
    }

    @Override
    public void NavigateToPaytm() {
        final String appPackageName = "net.one97.paytm";

        Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(appPackageName);
        if (launchIntent != null) {
            startActivity(launchIntent);//null pointer check in case package name was not found
        } else {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
    }

    @Override
    public void showContactListDialog(List<ContactDto> contactDtoList) {
        ContactFragment contactFragment = new ContactFragment(this);
        Bundle bundle = new Bundle();
        ArrayList<ContactDto> contactDtos = new ArrayList<>(contactDtoList.size());
        contactDtos.addAll(contactDtoList);
        bundle.putParcelableArrayList(Constants.BundleKey.CONTACT_LIST, contactDtos);
        contactFragment.setArguments(bundle);
        contactFragment.show(getFragmentManager(), "ContactList");
    }

    @Override
    public void checkContactPermission() {
        getBaseActivity().mPermissionHandler = PermissionHandler.getInstance(new PermissionHandler.PermissionHandleListener() {
            @Override
            public void onPermissionGranted(int requestCode) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        mTopupPresenter.onPhoneBookPressed();
                    }
                });
            }

            @Override
            public void onPermissionDenied(int requestCode) {
                Log.e(TAG, "onPermissionDenied: ");
            }

            @Override
            public void onPermissionReqCancel() {
                Log.e(TAG, "onPermissionReqCancel: ");
            }
        });
        getBaseActivity().mPermissionHandler.checkPermission(getActivity(), Manifest.permission.READ_CONTACTS,
                1, getString(R.string.contact_permission_Content));
    }

    @Override
    public void onContactSelected(ContactDto countryListDto) {
        mNameEditText.setText(countryListDto.getName());
        mPhNumberEditText.setText(countryListDto.getMbNo());
    }
}
