package com.josh.driver.modules.details.pastorders;

import com.hm.model.BaseModel;
import com.hm.utils.CodeSnippet;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.PastTripDto;
import com.josh.driver.dto.request.PastTripsRequestDto;
import com.josh.driver.dto.response.PastTripsResponseDto;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public class PastTripsModelImpl extends BaseModel implements PastTripsModel, ModelHandler.ModelHanlderListener<ApiStatusDto> {

    private List<PastTripDto> mPastTripList;

    private PastTripsModelListener mPastTripsModelListener;

    public PastTripsModelImpl(PastTripsModelListener pastTripsModelListener) {
        super(pastTripsModelListener);

        mPastTripsModelListener = pastTripsModelListener;
    }

    public static PastTripsModel getInstance(PastTripsModelListener pastTripsModelListener) {
        return new PastTripsModelImpl(pastTripsModelListener);

    }

    @Override
    public void fetchPastTrips() {
        PastTripsRequestDto pastTripsRequestDto = new PastTripsRequestDto();

        ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().getPastTripsRequest(pastTripsRequestDto));
    }

    @Override
    public void onSuccessApi(ApiStatusDto apiStatusDto) {
        if(apiStatusDto.isResultOk()){
            PastTripsResponseDto pastTripsResponseDto = CodeSnippet.getObjectFromMap(apiStatusDto.getData(),
                    PastTripsResponseDto.class);
            //get trips and add it into list

            mPastTripsModelListener.onTripsFetchSuccess(mPastTripList);

        }else{
            mPastTripsModelListener.onTripsFetchFailed(apiStatusDto.getDescription());

        }
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mPastTripsModelListener.onTripsFetchFailed(errorMessage);

    }

    @Override
    public void onAuthorizationFailed() {

    }

    interface PastTripsModelListener extends ModelListener {

        void onTripsFetchSuccess(List<PastTripDto> pastTripList);

        void onTripsFetchFailed(String message);
    }
}
