package com.josh.driver.modules.details.orderdetails;

import com.hm.activity.View;
import com.josh.driver.dto.response.TripDetailsResponseDto;

/**
 * Created by hmspl on 13/3/17.
 */

public interface TripDetailsView extends View {
    void setTripDetails(TripDetailsResponseDto tripDetailsResponseDto);

    void navigateToAdditionalSettings(TripDetailsResponseDto tripDetailsResponseDto);
}
