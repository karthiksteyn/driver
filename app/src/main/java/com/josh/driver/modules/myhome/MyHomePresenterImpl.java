package com.josh.driver.modules.myhome;

import android.content.res.Resources;

import com.hm.presenter.BasePresenter;
import com.josh.driver.R;
import com.josh.driver.dto.response.BalanceSummaryResponseDto;
import com.josh.driver.dto.response.CarTypeResponseDto;
import com.josh.driver.dto.response.Order;

/**
 * Created by hmspl on 13/3/17.
 */

public class MyHomePresenterImpl extends BasePresenter implements MyHomePresenter, MyHomeModelImpl.MyHomeModelListener {

    private MyHomeView mMyHomeView;

    private MyHomeModel mMyHomeModel;

    private Resources mResources;

    public MyHomePresenterImpl(MyHomeView myHomeView) {
        super(myHomeView);
        mMyHomeView = myHomeView;
        mMyHomeModel = MyHomeModelImpl.getInstance(this);
    }

    public static MyHomePresenter getInstance(MyHomeView profileView) {
        return new MyHomePresenterImpl(profileView);
    }

    @Override
    public void onMyHomePresenterCreate() {
        mMyHomeModel.onMyHomeModelCreate();
    }

    @Override
    public void onDutyControlClick(MyHomeFragment myHomeFragment, int tag) {
        mResources = myHomeFragment.getContext().getResources();
        if (mMyHomeView.hasNetworkConnection(mResources.getString(R.string.no_internet))) {
            mMyHomeView.showProgressbar();
            mMyHomeModel.changeControl(tag);
        }
    }

    @Override
    public void onFetchedTransactionDetails(BalanceSummaryResponseDto balanceSummaryResponseDto, String currentCarType, int currentDutyStatus) {
        mMyHomeView.setTransactionDetails(balanceSummaryResponseDto, currentCarType, currentDutyStatus);
    }

    @Override
    public void onFetchDataFailed(String msg) {
        mMyHomeView.dismissProgressbar();
        mMyHomeView.showMessage(msg);
    }

    @Override
    public void onChangeSuccess(int currentDutyTag) {
        mMyHomeView.setDutyControl(currentDutyTag);
    }

}
