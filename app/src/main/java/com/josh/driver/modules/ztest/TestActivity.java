package com.josh.driver.modules.ztest;

import android.Manifest;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hm.activity.BaseActivity;
import com.hm.handler.PermissionHandler;
import com.josh.driver.R;
import com.josh.driver.modules.ResizeAnimation;

public class TestActivity extends BaseActivity implements TestView, OnMapReadyCallback, PermissionHandler.PermissionHandleListener {

    private TestPresenter mTestPresenter;

    private GoogleMap mMap;

    RelativeLayout ril1;
    Button btn;
    int initialHeight;
    int actualHeight;
    MapFragment mMapFragment;
    private boolean mMapViewExpanded = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        mMapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);


        mMapFragment.getMapAsync(this);

        mTestPresenter = TestPresenterImpl.getInstance(this);

        ril1 = (RelativeLayout)findViewById(R.id.rela);


    }


    @Override
    protected void onResume() {
        super.onResume();

//        checkLocationPermission();
    }

    private void checkLocationPermission() {
        mPermissionHandler = PermissionHandler.getInstance(this);
        mPermissionHandler.checkPermission(this, Manifest.permission.ACCESS_FINE_LOCATION, 0,
                "App Needs your location to allocate order for your location");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setTrafficEnabled(false);
        mMap.setIndoorEnabled(false);
        mMap.setBuildingsEnabled(false);

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        checkLocationPermission();

        mTestPresenter.onTrackingPresenterCreate();

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                animateMapView();

            }
        });

    }

    private void animateMapView() {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mMapFragment.getView().getLayoutParams();

        ResizeAnimation a = new ResizeAnimation(mMapFragment.getView());
        a.setDuration(250);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;

        if (!getMapViewStatus()) {
            mMapViewExpanded = true;
//            a.setParams(lp.height, dpToPx(getResources(), height));
            ril1.animate().alpha(0.0f).setDuration(2000);
        } else {
            mMapViewExpanded = false;
//            a.setParams(lp.height, dpToPx(getResources(), 100));
            ril1.animate().alpha(1.0f).setDuration(2000);
        }
//        mMapFragment.getView().startAnimation(a);
    }

    private boolean getMapViewStatus() {
        return mMapViewExpanded;
    }

    public int dpToPx(Resources res, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, res.getDisplayMetrics());
    }

    @Override
    public void onPermissionGranted(int requestCode) {
        mTestPresenter.onLocationPermissionEnabled(this);

    }

    @Override
    public void onPermissionDenied(int requestCode) {

    }

    @Override
    public void onPermissionReqCancel() {

    }

    @Override
    public void setMapDetails(double latitude, double longitude) {
        mMap.clear();
        LatLng currLatLng = new LatLng(latitude, longitude);
        final MarkerOptions mo1 = new MarkerOptions().position(currLatLng).title("Current");
        mMap.addMarker(mo1);

        System.out.println("saran "+mMap.getCameraPosition().zoom);

        if(mMap.getCameraPosition().zoom <= 3){
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                .target(currLatLng)
                .zoom(17)
                .bearing(0)
                .tilt(45)
                .build()));
        }

        mMap.animateCamera(CameraUpdateFactory.newLatLng(currLatLng));



    }
}
