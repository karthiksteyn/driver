package com.josh.driver.modules.myhome;


import com.hm.activity.View;
import com.josh.driver.dto.ProfileDto;
import com.josh.driver.dto.UserDto;
import com.josh.driver.dto.response.BalanceSummaryResponseDto;
import com.josh.driver.dto.response.CarTypeResponseDto;
import com.josh.driver.dto.response.Order;

/**
 * Created by hmspl on 13/3/17.
 */

public interface MyHomeView extends View {

    void setTransactionDetails(BalanceSummaryResponseDto balanceSummaryResponseDto, String currentCarType, int currentDutyStatus);

    void setDutyControl(int currentDutyTag);

    void setMapDetails(double lat, double lng);

//    void showRequest(Order order);
}
