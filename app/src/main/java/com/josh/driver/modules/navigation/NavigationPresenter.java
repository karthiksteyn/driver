package com.josh.driver.modules.navigation;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.hm.presenter.Presenter;

/**
 * Created by hmspl on 30/3/17.
 */

public interface NavigationPresenter extends Presenter {

    void onNavigationPresenterCreate(NavigationFragment navigationFragment);

    void onCancelClick();

    void onMorphingClick();

    void onCallClick();

    String onGetDropLocation();

    LatLng onGetCoOrdinates();

    void onNavigationClick();

    void onChangeMarker();
}
