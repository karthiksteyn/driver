package com.josh.driver.modules.splash;

import com.hm.model.BaseModel;
import com.josh.driver.AppSnippet;


public class SplashModelImpl extends BaseModel implements SplashModel {

    private SplashModelListener mSplashModelListener;

    private SplashModelImpl(SplashModelListener splashModelListener) {
        super(splashModelListener);
        mSplashModelListener = splashModelListener;
    }

    public static SplashModel getInstance(SplashModelListener splashModelListener) {
        return new SplashModelImpl(splashModelListener);
    }

    /**
     * @return True if walkthrough is completed, else return False
     */
    @Override
    public boolean isWalkThroughCompleted() {
        return AppSnippet.isWalkThroughCompleted();
    }

    /**
     * @return True if login is completed, else return False
     */
    @Override
    public boolean isLoginCompleted() {
        return AppSnippet.isLoginCompleted();
    }

    /**
     * @return True if otp verification is completed, else return False
     */
    @Override
    public boolean isOtpVerificationCompleted() {
        return AppSnippet.isOtpVerificationCompleted();
    }

    /**
     * @return True if profile setup is completed, else return False
     */
    @Override
    public boolean isProfileSetupCompleted() {
        return AppSnippet.isProfileSetupCompleted();
    }

    interface SplashModelListener extends ModelListener {

    }

}
