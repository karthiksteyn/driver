package com.josh.driver.modules.user.resetpassword;

import com.hm.activity.View;

/**
 * Created by hmspl on 28/4/17 at 10:23 AM.
 */

public interface ResetPasswordView extends View {

    void showError(String field);

    void hideBottomNavigation();

    void setScreenType(String type);

    void navigateToLogin();
}
