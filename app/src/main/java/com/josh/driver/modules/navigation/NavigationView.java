package com.josh.driver.modules.navigation;

import com.google.android.gms.maps.model.LatLng;
import com.hm.activity.View;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.googlemap.PolyLineDto;

import java.util.List;

/**
 * Created by hmspl on 30/3/17.
 */

public interface NavigationView extends View {
    void setTripDetails(TripDetailsResponseDto mTripDetailsResponseDto);

    void onNavigateHome();

    void onSetMorphingButtonData(int status);

    void setMapInfo(LatLng currLatLng, LatLng pickUpLatLng);

    void drawDirections(List<PolyLineDto> polyLineDtos);

    void removeRoutes();

    void setMapDetails(double lat, double lng);

    void showMapNavigator(LatLng toLatLng);
}
