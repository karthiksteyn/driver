package com.josh.driver.modules.ztest;

import com.hm.activity.View;

/**
 * Created by hmspl on 13/3/17.
 */

public interface TestView extends View {
    void setMapDetails(double latitude, double longitude);
}
