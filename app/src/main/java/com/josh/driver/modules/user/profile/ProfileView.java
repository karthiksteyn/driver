package com.josh.driver.modules.user.profile;

import com.hm.activity.View;
import com.josh.driver.dto.ProfileDto;
import com.josh.driver.dto.UserDto;

import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public interface ProfileView extends View {
    void setDetails(ProfileDto profileDto);

    void navigateToLogin();

    void checkStoragePermission();

}
