package com.josh.driver.modules.details.pastorders;

import com.hm.activity.View;
import com.josh.driver.dto.PastTripDto;

import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public interface PastTripsView extends View {
    void setPastTrips(List<PastTripDto> pastTripList);
}
