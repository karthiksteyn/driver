package com.josh.driver.modules.home;

import com.hm.activity.View;
import com.josh.driver.dto.MenuDto;
import com.josh.driver.dto.response.Order;

import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public interface HomeView extends View {
    void setStartMenu(int currentPosition, String currentValue);

    void setMenuData(String name, List<MenuDto> menuList);

    void setMapDetails(double latitude, double longitude);

    void showRequest(Order order);

    void showNavigation(boolean isAlert);

    void sessionExpired();
}
