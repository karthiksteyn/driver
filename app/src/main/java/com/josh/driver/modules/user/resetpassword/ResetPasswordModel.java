package com.josh.driver.modules.user.resetpassword;

import android.content.Context;

/**
 * Created by hmspl on 28/4/17 at 10:58 AM.
 */

public interface ResetPasswordModel {

    void ValidateFields(String newPassword, String confirmPassword, Context context);

    void saveNewPassword();
}
