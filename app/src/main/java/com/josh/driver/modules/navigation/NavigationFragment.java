package com.josh.driver.modules.navigation;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hm.fragment.BaseFragment;
import com.josh.driver.AppSnippet;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.googlemap.PolyLineDto;
import com.josh.driver.googlemap.PolyLinesCreator;
import com.josh.driver.modules.home.HomeActivity;

import java.util.ArrayList;
import java.util.List;

public class NavigationFragment extends BaseFragment implements NavigationView, OnMapReadyCallback, View.OnClickListener {

    private NavigationPresenter mNavigationPresenter;

    private TextView mAddressLabelText, mAddressText,
            mNameText, mCrnText, mEtaText;

    private Button mCancelBtn, mMorphingBtn;

    private ImageView mCallImg, mNavigationImg;

    private GoogleMap mMap;

    private PolyLinesCreator mPolyLinesCreator;

    private List<Marker> mMarkersList;

    private LatLng mSource;

    private boolean mIsMarkerRotating;

    private Marker mMarker, mDestinationMarker;

    public NavigationFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPolyLinesCreator = new PolyLinesCreator();

        mNavigationPresenter = NavigationPresenterImpl.getInstance(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_navigation, container, false);

        mAddressLabelText = (TextView) view.findViewById(R.id.fragment_navigation_address_label);
        mAddressText = (TextView) view.findViewById(R.id.fragment_navigation_address_tv);
        mNameText = (TextView) view.findViewById(R.id.fragment_navigation_name_tv);
        mCrnText = (TextView) view.findViewById(R.id.fragment_navigation_crn_tv);
        mEtaText = (TextView) view.findViewById(R.id.fragment_navigation_eta_tv);
        mCallImg = (ImageView) view.findViewById(R.id.fragment_navigation_call_iv);
        mNavigationImg = (ImageView) view.findViewById(R.id.fragment_navigation_iv);
        mCancelBtn = (Button) view.findViewById(R.id.cancel_btn);
        mMorphingBtn = (Button) view.findViewById(R.id.morphing_btn);
        mCallImg.setOnClickListener(this);
        mNavigationImg.setOnClickListener(this);
        mCancelBtn.setOnClickListener(this);
        mMorphingBtn.setOnClickListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mNavigationPresenter.onNavigationPresenterCreate(this);

        return view;
    }


    @Override
    public void onClick(View v) {
        ((HomeActivity)getActivity()).stopRinging();
        switch (v.getId()) {
            case R.id.cancel_btn:
                mNavigationPresenter.onCancelClick();
                break;
            case R.id.morphing_btn:
                mNavigationPresenter.onMorphingClick();
                break;
            case R.id.fragment_navigation_call_iv:
                mNavigationPresenter.onCallClick();
                break;
            case R.id.fragment_navigation_iv:
                mNavigationPresenter.onNavigationClick();
        }
    }

    @Override
    public void setTripDetails(TripDetailsResponseDto mTripDetailsResponseDto) {

        String address;
        if(mTripDetailsResponseDto.getStatus() < Constants.TripStatus.WAIT_START){
            address = mTripDetailsResponseDto.getFrom();
        }else{

            address = AppSnippet.replaceEmpty(mTripDetailsResponseDto.getTo());
        }
        mAddressText.setText(address);
        mNameText.setText((mTripDetailsResponseDto.getCustomer()).getName());
        mCrnText.setText(getString(R.string.crn) + mTripDetailsResponseDto.getCrn());
        mEtaText.setText(mTripDetailsResponseDto.getEta());

        onSetMorphingButtonData(mTripDetailsResponseDto.getStatus());
    }

    @Override
    public void onNavigateHome() {
        dismissProgressbar();
        Activity activity = getActivity();
        if (activity != null) {
            ((HomeActivity) activity).showMyHome();
        }
    }

    @Override
    public void onSetMorphingButtonData(int status) {
        dismissProgressbar();
        switch (status) {
            case Constants.TripStatus.WAIT_LOCATE:
                mMorphingBtn.setText(getString(R.string.locate));
                mAddressLabelText.setText(getString(R.string.pickup));
                break;
            case Constants.TripStatus.WAIT_VERIFY:
                mMorphingBtn.setText(getString(R.string.verify));
                mAddressLabelText.setText(getString(R.string.pickup));
                break;
            case Constants.TripStatus.WAIT_START:
                mMorphingBtn.setText(getString(R.string.start));
                mAddressLabelText.setText(getString(R.string.drop));
                mAddressText.setText(mNavigationPresenter.onGetDropLocation());
                mNavigationPresenter.onChangeMarker();
                if(mPolyLinesCreator != null){
                    mPolyLinesCreator.removeAllLineCoordinates();
                    mPolyLinesCreator.removeAllLinesAndMarkers();
                }
                break;
            case Constants.TripStatus.WAIT_STOP:
                mMorphingBtn.setText(getString(R.string.stop));
                mCancelBtn.setAlpha(0.5f);
                mCancelBtn.setClickable(false);
                mAddressLabelText.setText(getString(R.string.drop));
                break;
            case Constants.TripStatus.WAIT_COMPLETE:
                Activity activity = getActivity();
                if (activity != null) {
                    ((HomeActivity) activity).showBillDetails();
                }
                break;
        }
    }

    @Override
    public void setMapInfo(LatLng currLatLng, LatLng pickUpLatLng) {
        mMarkersList = new ArrayList<>();
        BitmapDescriptor pickupIcon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);
        BitmapDescriptor currIcon = BitmapDescriptorFactory.fromResource(R.drawable.carmap);

        final MarkerOptions pickupMO = new MarkerOptions().position(pickUpLatLng).anchor(0.5f, 0.5f).icon(pickupIcon);
        final MarkerOptions currMO = new MarkerOptions().position(currLatLng).anchor(0.5f, 0.5f).icon(currIcon);

        Marker marker = mMap.addMarker(pickupMO);
        mMarkersList.add(marker);
        marker = mMap.addMarker(currMO);
        mMarkersList.add(marker);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker boundMarker : mMarkersList) {
            builder.include(boundMarker.getPosition());
        }
        final LatLngBounds bounds = builder.build();
        int padding = 0; // offset from edges of the map in pixels
        final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.moveCamera(cameraUpdate);
            }
        });
//        mMap.animateCamera(cameraUpdate);

    }

    @Override
    public void drawDirections(List<PolyLineDto> polyLineDtoList) {
        mPolyLinesCreator.removeAllLineCoordinates();
        mPolyLinesCreator.removeAllLinesAndMarkers();
        mPolyLinesCreator.setAddMarkerListener(new PolyLinesCreator.AddMarkerListener() {
            @Override
            public void onStartMarkerAdded(Marker marker) {
//                mStartMarker = marker;
            }

            @Override
            public void onEndMarkerAdded(Marker marker) {
//                mEndMarker = marker;
            }
        });

        Resources resources = getResources();
        mPolyLinesCreator.setPathColor(getResourceColor(R.color.ebony_clay),
                getResourceColor(R.color.ebony_clay));
        mPolyLinesCreator.addPolyLinesInstantly(polyLineDtoList);
    }

    @Override
    public void removeRoutes() {
        mPolyLinesCreator.removeAllLineCoordinates();
        dismissProgressbar();
        showMessage(getString(R.string.no_route_avail));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mPolyLinesCreator.bindMap(googleMap);
        mMap = googleMap;
        mMap.setPadding(getResources().getDimensionPixelSize(R.dimen.dp_100), getResources().getDimensionPixelSize(R.dimen.dp_100),
                getResources().getDimensionPixelSize(R.dimen.dp_100), getResources().getDimensionPixelSize(R.dimen.dp_100));

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                .target(Constants.AppKey.CHENNAI_LATLNG)
                .zoom(10)
                .build()));
    }

    @Override
    public void setMapDetails(double latitude, double longitude) {
        if (mMap != null) {
            LatLng customerLatLng = mNavigationPresenter.onGetCoOrdinates();

            mMarkersList = new ArrayList<>();
            BitmapDescriptor pickupIcon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);

            LatLng currLatLng = new LatLng(latitude, longitude);

            if(mDestinationMarker != null) {
                mDestinationMarker.remove();
            }
            MarkerOptions customerMO = null;
            if(customerLatLng != null) {
                customerMO = new MarkerOptions().position(customerLatLng).anchor(0.5f, 0.5f).icon(pickupIcon);
                mDestinationMarker = mMap.addMarker(customerMO);
            }

            if (mSource == null) {
                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.carmap);

                final MarkerOptions mo1 = new MarkerOptions().position(currLatLng).title("Current").anchor(0.5f, 0.5f).icon(icon);
                mMarker = mMap.addMarker(mo1);
                mSource = mo1.getPosition();

            } else {

                mIsMarkerRotating = false;

                mSource = mMarker.getPosition();

                final LatLng startPosition = mSource;

                final LatLng finalPosition = currLatLng;
                rotateMarker(mMarker, (float) bearingBetweenLocations(startPosition, finalPosition));

                mSource = startPosition;

                final Handler handler = new Handler();
                final long start = SystemClock.uptimeMillis();
                final Interpolator interpolator = new AccelerateDecelerateInterpolator();
                final float durationInMs = 1000;
                final boolean hideMarker = false;

                handler.post(new Runnable() {
                    long elapsed;
                    float t;
                    float v;

                    @Override
                    public void run() {
                        // Calculate progress using interpolator
                        elapsed = SystemClock.uptimeMillis() - start;
                        t = elapsed / durationInMs;
                        v = interpolator.getInterpolation(t);

                        LatLng currentPosition = new LatLng(
                                startPosition.latitude * (1 - t) + finalPosition.latitude * t,
                                startPosition.longitude * (1 - t) + finalPosition.longitude * t);

                        mMarker.setPosition(currentPosition);

                        // Repeat till progress is complete.
                        if (t < 1) {
                            // Post again 16ms later.
                            handler.postDelayed(this, 16);
                        } else {
                            if (hideMarker) {
                                mMarker.setVisible(false);
                            } else {
                                mMarker.setVisible(true);
                            }
                        }
                    }
                });
            }

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            if(customerMO != null) {
                builder.include(customerMO.getPosition());
            }
            builder.include(currLatLng);
            final LatLngBounds bounds = builder.build();
            int padding = 0; // offset from edges of the map in pixels
            final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    mMap.animateCamera(cameraUpdate);
                }
            });
        }
    }

    @Override
    public void showMapNavigator(LatLng toLatLng) {
        if(toLatLng != null) {
            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + toLatLng.latitude + "," + toLatLng.longitude);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            getContext().startActivity(mapIntent);
        }
    }

    private void rotateMarker(final Marker marker, final float toRotation) {
        if (!mIsMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    mIsMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    marker.setRotation(-rot > 180 ? rot / 2 : rot);
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        mIsMarkerRotating = false;
                    }
                }
            });
        }
    }

    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }
}
