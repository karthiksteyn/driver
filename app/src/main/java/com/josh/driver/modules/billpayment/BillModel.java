package com.josh.driver.modules.billpayment;

import android.content.Context;

/**
 * Created by hmspl on 13/3/17.
 */

public interface BillModel {
    void onBillModelCreate(Context context);

    void completeTrip();

    String extractTime(String time);

    String extractDate(String time);
}
