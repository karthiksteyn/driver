package com.josh.driver.modules.user.profile;

import com.josh.driver.dto.ProfileDto;

/**
 * Created by hmspl on 13/3/17.
 */

public interface ProfileModel {
    ProfileDto getProfileData();

    void clearCommonData();

}
