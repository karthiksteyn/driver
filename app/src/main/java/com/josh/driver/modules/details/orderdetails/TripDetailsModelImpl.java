package com.josh.driver.modules.details.orderdetails;

import android.content.Context;
import android.os.Bundle;

import com.hm.model.BaseModel;
import com.hm.utils.CodeSnippet;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

/**
 * Created by hmspl on 13/3/17.
 */

public class TripDetailsModelImpl extends BaseModel implements TripDetailsModel, ModelHandler.ModelHanlderListener<ApiStatusDto> {

    private String mTripId;

    private TripDetailsModelListener mTripDetailsModelListener;

    private TripDetailsResponseDto mTripDetailsResponseDto;

    public TripDetailsModelImpl(TripDetailsModelListener tripDetailsModelListener) {
        super(tripDetailsModelListener);

        mTripDetailsModelListener = tripDetailsModelListener;
    }

    public static TripDetailsModel getInstance(TripDetailsModelListener tripDetailsModelListener) {
        return new TripDetailsModelImpl(tripDetailsModelListener);

    }

    @Override
    public void fetchTripDetails(Bundle bundle, Context context) {
        //get Trip id from bundle

        ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().getTripDetailsRequest(mTripId));

    }

    @Override
    public TripDetailsResponseDto getTripDetails() {
        return mTripDetailsResponseDto;
    }

    @Override
    public void onSuccessApi(ApiStatusDto apiStatusDto) {
        if(apiStatusDto.isResultOk()){
            TripDetailsResponseDto tripDetailsResponseDto = CodeSnippet.getObjectFromMap(apiStatusDto.getData(),
                    TripDetailsResponseDto.class);
            //get trips and add it into list

            mTripDetailsResponseDto = tripDetailsResponseDto;
            mTripDetailsModelListener.onFetchSuccess(tripDetailsResponseDto);

        }else{
            mTripDetailsModelListener.onFetchFailed(apiStatusDto.getDescription());

        }
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mTripDetailsModelListener.onFetchFailed(errorMessage);

    }

    @Override
    public void onAuthorizationFailed() {

    }

    interface TripDetailsModelListener extends ModelListener {

        void onFetchFailed(String message);

        void onFetchSuccess(TripDetailsResponseDto tripDetailsResponseDto);
    }
}
