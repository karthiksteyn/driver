package com.josh.driver.modules.user.otpverification;

import android.content.Context;

/**
 * Created by hmspl on 13/3/17.
 */

public interface OtpVerificationModel {
    String getPhoneNumber();

    void verifyUser(String verificationCode, Context context);

    void resendVerificationCode();

    void savePhoneNumber(String phoneNumber);
}
