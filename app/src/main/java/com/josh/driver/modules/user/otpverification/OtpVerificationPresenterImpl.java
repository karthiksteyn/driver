package com.josh.driver.modules.user.otpverification;

import android.content.Context;
import android.content.res.Resources;
import com.hm.presenter.BasePresenter;
import com.josh.driver.R;

/**
 * Created by hmspl on 13/3/17.
 */

public class OtpVerificationPresenterImpl extends BasePresenter implements OtpVerificationPresenter, OtpVerificationModelImpl.OtpVerificationModelListener {

    private OtpVerificationView mOtpVerificationView;

    private OtpVerificationModel mOtpVerificationModel;

    private Resources mResources;

    public OtpVerificationPresenterImpl(OtpVerificationView otpVerificationView) {
        super(otpVerificationView);
        mOtpVerificationView = otpVerificationView;
        mOtpVerificationModel = OtpVerificationModelImpl.getInstance(this);
    }

    public static OtpVerificationPresenter getInstance(OtpVerificationView testView) {
        return new OtpVerificationPresenterImpl(testView);
    }

    @Override
    public void onOtpVerificationPresenterCreate(String phoneNumber) {
        mOtpVerificationView.setPhoneNumber(mOtpVerificationModel.getPhoneNumber());
    }

    @Override
    public void onVerifyClick(Context context) {
        mResources = context.getResources();

        if (mOtpVerificationView.hasNetworkConnection(mResources.getString(R.string.no_internet))) {
            mOtpVerificationView.showProgressbar();
            mOtpVerificationModel.verifyUser(mOtpVerificationView.getVerificationCode(), context);
        }
    }

    @Override
    public void onResendVerificationCodeClick(Context context) {
        mResources = context.getResources();

        if (mOtpVerificationView.hasNetworkConnection(mResources.getString(R.string.no_internet))) {
            mOtpVerificationView.showProgressbar();
            mOtpVerificationModel.resendVerificationCode();
        }
    }

    @Override
    public void onVerificationFailed(String msg) {
        mOtpVerificationView.dismissProgressbar();
        mOtpVerificationView.showMessage(msg);
    }

    @Override
    public void onVerificationSuccess() {
        mOtpVerificationView.NavigateToResetPwd();
    }

    @Override
    public void onResendVerificationCodeReply(String msg) {
        mOtpVerificationView.dismissProgressbar();
        mOtpVerificationView.showMessage(msg);
        mOtpVerificationView.startTimer();
    }
}
