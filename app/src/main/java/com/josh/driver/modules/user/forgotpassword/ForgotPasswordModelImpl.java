package com.josh.driver.modules.user.forgotpassword;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Patterns;

import com.hm.model.BaseModel;
import com.hm.utils.CodeSnippet;
import com.josh.driver.R;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.Headers;
import com.josh.driver.dto.request.PaymentRequestDto;
import com.josh.driver.dto.request.SendOtpRequestDto;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

/**
 * Created by hmspl on 13/3/17.
 */

public class ForgotPasswordModelImpl extends BaseModel implements ForgotPasswordModel, ModelHandler.ModelHanlderListener<ApiStatusDto> {

    private String mTripId;

    private ForgotPasswordModelListener mForgotPasswordModelListener;

    private Context mContext;

    private SendOtpRequestDto mSendOtpRequestDto;


    public ForgotPasswordModelImpl(ForgotPasswordModelListener forgotPasswordModelListener) {
        super(forgotPasswordModelListener);

        mForgotPasswordModelListener = forgotPasswordModelListener;
    }

    public static ForgotPasswordModel getInstance(ForgotPasswordModelListener forgotPasswordModelListener) {
        return new ForgotPasswordModelImpl(forgotPasswordModelListener);

    }

    @Override
    public void onSuccessApi(ApiStatusDto apiStatusDto) {
        if(apiStatusDto.isResultOk()){
            DataHandler.getInstance().saveDriverPhone(mSendOtpRequestDto.getMobile());
            mForgotPasswordModelListener.onSuccess(apiStatusDto.getDescription());
        }else{
            mForgotPasswordModelListener.onFailed(apiStatusDto.getDescription());
        }
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mForgotPasswordModelListener.onFailed(errorMessage);
    }

    @Override
    public void onAuthorizationFailed() {

    }

    @Override
    public void sendDetails(SendOtpRequestDto sendOtpRequestDto, Context context) {
        mContext = context;
        if (validate(sendOtpRequestDto)) {
            mSendOtpRequestDto = sendOtpRequestDto;
            ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().onSendOtp(sendOtpRequestDto));
        }
    }


    private boolean validate(SendOtpRequestDto sendOtpRequestDto) {
        Resources resources = mContext.getResources();
        String value = sendOtpRequestDto.getMobile();

        if (value.isEmpty()) {
            mForgotPasswordModelListener.onFailed(resources.getString(R.string.phone_empty));
            return false;
        } else if (value.length() < 10 || !Patterns.PHONE.matcher(value).matches()) {
            mForgotPasswordModelListener.onFailed(resources.getString(R.string.phone_validation));
            return false;
        }
        return true;
    }

    interface ForgotPasswordModelListener extends ModelListener {

        void onSuccess(String message);

        void onFailed(String message);
    }
}
