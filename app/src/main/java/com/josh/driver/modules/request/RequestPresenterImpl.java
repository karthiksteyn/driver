package com.josh.driver.modules.request;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;
import com.hm.presenter.BasePresenter;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.dto.response.Order;
import com.josh.driver.dto.response.TripDetailsResponseDto;

/**
 * Created by hmspl on 13/3/17.
 */

public class RequestPresenterImpl extends BasePresenter implements RequestPresenter, RequestModelImpl.RequestModelListener {

    private RequestView mRequestView;

    private RequestModel mRequestModel;

    private Resources mResources;

    public RequestPresenterImpl(RequestView requestView) {
        super(requestView);
        mRequestView = requestView;
        mRequestModel = RequestModelImpl.getInstance(this);
    }

    public static RequestPresenter getInstance(RequestView requestView) {
        return new RequestPresenterImpl(requestView);
    }

    @Override
    public void onRequestPresenterCreate(Bundle bundle) {
        Order order = (Order) bundle.getSerializable(Constants.BundleKey.ORDER);
        mRequestModel.onRequestModelCreate(order);
        mRequestView.setRequestDetails(order);
    }

    @Override
    public void onAcceptClick(Context context) {
        mResources = context.getResources();
        if(mRequestView.hasNetworkConnection(mResources.getString(R.string.no_internet))){
            mRequestView.showProgressbar();
            mRequestModel.acceptRide();
        }
    }

    @Override
    public void onAcceptCancel() {
        mRequestModel.cancelAcceptRide();
    }

    @Override
    public void onSetupMapDetails() {
        mRequestModel.getCoOrdinates();
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onFailed(String message) {
        mRequestView.dismissProgressbar();
        mRequestView.showMessage(message);
        mRequestView.navigateToHome();

    }

    @Override
    public void onAcceptSuccess(TripDetailsResponseDto tripDetailsResponseDto) {
        mRequestView.navigateToNavigation();
    }

    @Override
    public void onFetchCoOrdinatesSuccess(LatLng currLatLng, LatLng pickUpLatLng) {
        mRequestView.setMapDetails(currLatLng, pickUpLatLng);
    }
}
