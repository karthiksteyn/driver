package com.josh.driver.modules;

import android.os.Bundle;
import android.view.View;

import com.hm.activity.BaseActivity;
import com.josh.driver.R;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.request.RatingRequestDto;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

public class RatingActivity extends BaseActivity implements View.OnClickListener, ModelHandler.ModelHanlderListener<ApiStatusDto> {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
    }

    @Override
    public void onClick(View v) {
        submitRating(setRatingDetail());
    }

    private void submitRating(RatingRequestDto ratingRequestDto) {
        ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().getRatingRequest(ratingRequestDto));
    }

    private RatingRequestDto setRatingDetail() {
        RatingRequestDto ratingRequestDto = null;
        /**
         * set rating details
         */
        
        return ratingRequestDto;
    }

    @Override
    public void onSuccessApi(ApiStatusDto response) {
        if(response.isResultOk()){
            showMessage(response.getDescription());
            finish();
        }else{
            showMessage(response.getDescription());
        }

    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        showMessage(errorMessage);
    }
}
