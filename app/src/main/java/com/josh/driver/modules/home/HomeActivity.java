package com.josh.driver.modules.home;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hm.activity.BaseActivity;
import com.hm.handler.PermissionHandler;
import com.josh.driver.AppSnippet;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.adapter.CommonAdapter;
import com.josh.driver.dto.MenuDto;
import com.josh.driver.dto.response.Order;
import com.josh.driver.modules.billpayment.BillFragment;
import com.josh.driver.modules.myhome.MyHomeFragment;
import com.josh.driver.modules.navigation.NavigationFragment;
import com.josh.driver.modules.recharge.TopupFragment;
import com.josh.driver.modules.request.RequestActivity;
import com.josh.driver.modules.user.login.LoginActivity;
import com.josh.driver.modules.user.profile.ProfileFragment;
import com.josh.driver.service.SocketService;
import com.josh.driver.widget.MenuHeaderView;

import java.util.List;

public class HomeActivity extends BaseActivity implements HomeView, AdapterView.OnItemClickListener,
        CommonAdapter.OnGetViewListener<MenuDto>, DrawerLayout.DrawerListener, View.OnClickListener {

    private static final int PROFILE = 0;

    private static final int MY_HOME = 1;

    private static final int TOPUP = 2;

    private static final int NAVIGATION = 3;

    private static final int BILL = 4;


    private static final int NAVIGATION_REQUEST_CODE = 1;

    private static final String FRAGMENT_TAG = "fragment";

    private DrawerLayout mDrawerLayout;

    private ListView mNavigationDrawerListView;

    private HomePresenter mHomePresenter;

    private MenuHeaderView mMenuHeaderView;

    private CommonAdapter<MenuDto> mMenuCommonAdapter;

    private int mCurrentPosition = 1;

    private int mPreviousPosition;

    private String mCurrentValue = "";

    private MediaPlayer mMediaPlayer;

    private AlertDialog mAlertDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_home_drawer_layout);
        mDrawerLayout.addDrawerListener(this);
        mNavigationDrawerListView = (ListView) findViewById(R.id.activity_home_left_drawer);
        mNavigationDrawerListView.setOnItemClickListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((ImageView) toolbar.findViewById(R.id.action_iv)).setOnClickListener(this);

        mHomePresenter = HomePresenterImpl.getInstance(this);
        mHomePresenter.onHomePresenterCreate(getIntent().getExtras(), this);

        screenKeepOn();

        String intentString = getIntent().getStringExtra(Constants.BundleKey.REQUEST);
        if(intentString != null){
            if(intentString.equals(Constants.AppKey.WAITING_REQUEST)) {
                showRequest((Order) getIntent().getExtras().getSerializable(Constants.BundleKey.ORDER));
            } else if(intentString.equals(Constants.AppKey.FORCE_REQUEST)) {
                showNavigation(true);
            } else if(intentString.equals(Constants.AppKey.SIGNOUT_EXPIRY)) {
                sessionExpired();
            }
        }

        //starting service
        startService(new Intent(this, SocketService.class));
    }

    private void screenKeepOn() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    @Override
    protected void onPause() {
        super.onPause();

        mHomePresenter.onReceiverUnRegister();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String intentString = intent.getStringExtra(Constants.BundleKey.REQUEST);
        if(intentString != null){
            if(intentString.equals(Constants.AppKey.WAITING_REQUEST)) {
                showRequest((Order) intent.getExtras().getSerializable(Constants.BundleKey.ORDER));
            } else if(intentString.equals(Constants.AppKey.FORCE_REQUEST)) {
                showNavigation(true);
            } else if(intentString.equals(Constants.AppKey.SIGNOUT_EXPIRY)) {
                sessionExpired();
            } else if(intentString.equals(Constants.AppKey.FORCE_TRIP_STATUS)) {
                getCurrentPosition();

                switchMenu();
                mPreviousPosition = mCurrentPosition;
                setMenuSelected();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mHomePresenter.onReceiverRegister();
        checkLocationPermission();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void checkLocationPermission() {

        mPermissionHandler = PermissionHandler.getInstance(new PermissionHandler.PermissionHandleListener() {
            @Override
            public void onPermissionGranted(int requestCode) {
//                mHomePresenter.onLocationPermissionEnabled(getContext());

                checkStoragePermission();

            }

            @Override
            public void onPermissionDenied(int requestCode) {

            }

            @Override
            public void onPermissionReqCancel() {

            }
        });
        mPermissionHandler.checkPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION, 0,
                "App Needs your location to allocate order for your location");
    }

    public void checkStoragePermission() {
        mPermissionHandler = PermissionHandler.getInstance(new PermissionHandler.PermissionHandleListener() {
            @Override
            public void onPermissionGranted(int requestCode) {
                mHomePresenter.onLocationPermissionEnabled(getContext());

            }

            @Override
            public void onPermissionDenied(int requestCode) {

            }

            @Override
            public void onPermissionReqCancel() {

            }
        });
        mPermissionHandler.checkPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, 0,
                Constants.PermissionMessages.STORAGE_DENIED);
    }

    @Override
    public void setStartMenu(int currentPosition, String currentValue) {
        mCurrentPosition = currentPosition;
        mCurrentValue = currentValue;
    }

    @Override
    public void setMenuData(String name, List<MenuDto> menuList) {

        mMenuHeaderView = (MenuHeaderView) getLayoutInflater().inflate(R.layout.inflater_menu_lv_header,
                mNavigationDrawerListView, false);
        mNavigationDrawerListView.addHeaderView(mMenuHeaderView, new MenuDto(R.drawable.menu_ic_profile, getString(R.string.bill)), true);
        setHeaderProfileData(name);
        mMenuCommonAdapter = new CommonAdapter<>(this,
                R.layout.inflater_menu_lv_row, menuList, this);
        mNavigationDrawerListView.setAdapter(mMenuCommonAdapter);
        getCurrentPosition();

        switchMenu();
        mPreviousPosition = mCurrentPosition;
        setMenuSelected();

    }

    private void getCurrentPosition() {
        if (mHomePresenter.onGetTripDetails()) {
            switch (mHomePresenter.onGetStatus()) {
                case Constants.TripStatus.WAIT_COMPLETE:
                    mCurrentPosition = BILL;
                    break;

                case Constants.TripStatus.WAIT_LOCATE:
                case Constants.TripStatus.WAIT_VERIFY:
                case Constants.TripStatus.WAIT_START:
                case Constants.TripStatus.WAIT_STOP:
                    mCurrentPosition = NAVIGATION;
                    break;

            }
        }
    }

    public void setHeaderProfileData(String name) {
        mMenuHeaderView.setData(name);
    }

    private void setMenuSelected() {
        mNavigationDrawerListView.setItemChecked(mPreviousPosition, true);
        mNavigationDrawerListView.setSelection(mPreviousPosition);
    }

    @Override
    public void setMapDetails(double latitude, double longitude) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);

        if (currentFragment instanceof MyHomeFragment) {
            ((MyHomeFragment) currentFragment).setMapDetails(latitude, longitude);
        } else if (currentFragment instanceof NavigationFragment) {
            ((NavigationFragment) currentFragment).setMapDetails(latitude, longitude);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Activity activity = getActivity();
        if (activity != null) {
            if (requestCode == NAVIGATION_REQUEST_CODE && resultCode == activity.RESULT_OK) {
                showNavigation(false);
            }
        }
    }

    @Override
    public void showRequest(Order order) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BundleKey.ORDER, order);

        Intent intent = new Intent(getActivity(), RequestActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, NAVIGATION_REQUEST_CODE);
    }

    private void switchMenu() {
        switch (mCurrentPosition) {
            case PROFILE:
                mPreviousPosition = mCurrentPosition;
                invalidateOptionsMenu();
                showProfile();
                break;
            case MY_HOME:
                mPreviousPosition = mCurrentPosition;
                invalidateOptionsMenu();
                showMyHome();
                break;

            case NAVIGATION:
                mPreviousPosition = MY_HOME;
                invalidateOptionsMenu();
                showNavigation(false);
                break;

            case BILL:
                mPreviousPosition = MY_HOME;
                invalidateOptionsMenu();
                showBillDetails();
                break;
            case TOPUP:
                mPreviousPosition = mCurrentPosition;
                invalidateOptionsMenu();
                showTopup();
                break;
        }
    }

    private void changeToolbarContent(boolean isOptionBtn, String title, String imageUrl, boolean isWalletAvailable, float balance) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageView optionIv = (ImageView) toolbar.findViewById(R.id.action_iv);
        ImageView profileIv = (ImageView) toolbar.findViewById(R.id.user_dp_iv);
        TextView titleTextView = (TextView) toolbar.findViewById(R.id.toolbar_title_tv);
        TextView balanceTextView = (TextView) toolbar.findViewById(R.id.toolbar_balance_tv);
        RelativeLayout walletLayout = (RelativeLayout) toolbar.findViewById(R.id.wallet_layout);
        if (!isOptionBtn) {
            optionIv.setImageResource(R.drawable.menu_buttonyellow);
        }
        if (!isWalletAvailable) {
            walletLayout.setVisibility(View.GONE);
        } else {
            walletLayout.setVisibility(View.VISIBLE);
            balanceTextView.setText(String.valueOf(balance));
            walletLayout.setOnClickListener(this);
        }
        titleTextView.setText(title);
        if (imageUrl != null) {

        } else {
            profileIv.setVisibility(View.GONE);
        }

    }

    public void updateWallet(String balance) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_balance_tv)).setText(balance);
    }

    private void updateView(Fragment fragment, String tag, String title, String imageUrl,
                            boolean isOptionBtn, boolean isWalletAvail, float balance) {
        if (!isFinishing()) {
            mPreviousPosition = mCurrentPosition;
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.activity_content_layout, fragment, tag).commitAllowingStateLoss();
            changeToolbarContent(isOptionBtn, title, imageUrl, isWalletAvail, balance);
        }
    }

    public void showProfile() {
        mPreviousPosition = mCurrentPosition;
        mCurrentPosition = mPreviousPosition;
        updateView(new ProfileFragment(), FRAGMENT_TAG, getString(R.string.profile), null, true, false, mHomePresenter.onGetBalance());
    }

    public void showMyHome() {
        mCurrentPosition = mPreviousPosition;

        updateView(new MyHomeFragment(), FRAGMENT_TAG, mHomePresenter.onGetDriverName(), "sadfdas",
                true, true, mHomePresenter.onGetBalance());

    }


    public void showNavigation(boolean isAlert) {
        if (isAlert) {
            playRingTone();
        }
        mCurrentPosition = mPreviousPosition;
        updateView(new NavigationFragment(), FRAGMENT_TAG, getString(R.string.trip_navigation), null, true, false, mHomePresenter.onGetBalance());
    }

    @Override
    public void sessionExpired() {
        // Logout confirmation dialog

            mAlertDialog = AppSnippet.showAlertDialog(getActivity(), Constants.AlertMessage.LOG_IN_EXPIRED_TITLE
                    , Constants.AlertMessage.LOG_IN_EXPIRED_CONTENT, Constants.AlertMessage.YES, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAlertDialog.dismiss();

                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, null, null);
    }

    public void showBillDetails() {
        mCurrentPosition = mPreviousPosition;
        updateView(new BillFragment(), FRAGMENT_TAG, getString(R.string.bill), null, true, false, mHomePresenter.onGetBalance());
    }

    public void showTopup() {
        mCurrentPosition = mPreviousPosition;
        updateView(new TopupFragment(), FRAGMENT_TAG, getString(R.string.topup), null, true, false, mHomePresenter.onGetBalance());
    }

    public void playRingTone() {
        try {
            Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(getContext(), alert);
            final AudioManager audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_RING) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (Exception e) {
        }
    }

    public void stopRinging() {
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mCurrentPosition = position;
        if (mCurrentPosition == MY_HOME)
            getCurrentPosition();
        switchMenu();
        closeMenu();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
            stopRinging();
        } else if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_iv:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.wallet_layout:
                showTopup();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        stopRinging();
        if (mDrawerLayout.isDrawerOpen(mNavigationDrawerListView)) {
            closeMenu();
        } else {
            Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);

            if (currentFragment instanceof MyHomeFragment || currentFragment instanceof NavigationFragment
                    || currentFragment instanceof BillFragment) {

                if(currentFragment instanceof MyHomeFragment) {
                    if(((MyHomeFragment) currentFragment).isCabSelectionVisible()) {
                        ((MyHomeFragment) currentFragment).onCloseCabSelection();
                    }else{
                        super.onBackPressed();
                    }
                }else {
                    super.onBackPressed();
                }

            } else {
                mCurrentPosition = MY_HOME;
                getCurrentPosition();
                switchMenu();
                setMenuSelected();
            }
        }
    }

    private void closeMenu() {
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, MenuDto object, int viewId) {
        convertView = View.inflate(this, viewId, null);
        ((RelativeLayout) convertView.findViewById(R.id.inflater_menu_lv_row_icon_iv))
                .setBackgroundResource(object.getIconSelectedResId());
        ((TextView) convertView.findViewById(R.id.inflater_menu_lv_row_title_iv)).setText(object.getTitle());
        return convertView;
    }


}
