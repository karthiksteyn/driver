package com.josh.driver.modules.request;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hm.activity.BaseActivity;
import com.josh.driver.R;
import com.josh.driver.dto.response.Order;

import java.util.ArrayList;
import java.util.List;

public class RequestActivity extends BaseActivity implements RequestView, OnMapReadyCallback, View.OnClickListener {

    private RequestPresenter mRequestPresenter;

    private TextView mAwayTimeText, mPickupAddressText, mTimerText;

    private TextView mAcceptText;

    private GoogleMap mMap;

    private List<Marker> mMarkersList;

    private MediaPlayer mMediaPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        setupToolbar();

        mAwayTimeText = (TextView) findViewById(R.id.fragment_request_away_time_tv);
        mPickupAddressText = (TextView) findViewById(R.id.pickup_address_tv);
        mTimerText = (TextView) findViewById(R.id.priority_timer_tv);
        mAcceptText = (TextView) findViewById(R.id.request_accept_tv);
        mAcceptText.setOnClickListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mRequestPresenter = RequestPresenterImpl.getInstance(this);
        mRequestPresenter.onRequestPresenterCreate(getIntent().getExtras());

    }

    private void setupToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageView optionIv = (ImageView) toolbar.findViewById(R.id.action_iv);
        ImageView profileIv = (ImageView) toolbar.findViewById(R.id.user_dp_iv);
        TextView titleTextView = (TextView) toolbar.findViewById(R.id.toolbar_title_tv);
        TextView balanceTextView = (TextView) toolbar.findViewById(R.id.toolbar_balance_tv);
        RelativeLayout walletLayout = (RelativeLayout) toolbar.findViewById(R.id.wallet_layout);

        optionIv.setImageResource(R.drawable.menu_buttonyellow);
        optionIv.setOnClickListener(this);

        walletLayout.setVisibility(View.GONE);

        titleTextView.setText(getString(R.string.request));
        profileIv.setVisibility(View.GONE);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setPadding(getResources().getDimensionPixelSize(R.dimen.dp_150), getResources().getDimensionPixelSize(R.dimen.dp_150),
                getResources().getDimensionPixelSize(R.dimen.dp_150), getResources().getDimensionPixelSize(R.dimen.dp_350));
        mRequestPresenter.onSetupMapDetails();
    }

    @Override
    public void onClick(View v) {
        stopRinging();
        switch (v.getId()){
            case R.id.action_iv:
                break;
            case R.id.request_accept_tv:
                mRequestPresenter.onAcceptClick(this);
                break;
        }

    }

    @Override
    public void setRequestDetails(Order order) {

        if(mMediaPlayer == null || !mMediaPlayer.isPlaying()) {
            playRingTone();
        }

        mAwayTimeText.setText(order.getEta());
        mPickupAddressText.setText(order.getPickuploc());

        new CountDownTimer(order.getDisplaytime()*1000, 1000){

            @Override
            public void onTick(long millisUntilFinished) {
                mTimerText.setText("In: " + millisUntilFinished / 1000 +" Sec");
            }

            @Override
            public void onFinish()
            {
                dismissProgressbar();
                mRequestPresenter.onAcceptCancel();
                stopRinging();
                finish();
            }
        }.start();

        if(!order.getStatus()){
            dismissProgressbar();
            mRequestPresenter.onAcceptCancel();
            stopRinging();
            finish();
        }
    }

    private void playRingTone() {
        try {
            Uri alert =  RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(getContext(), alert);
            final AudioManager audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_RING) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        }
        catch(Exception e) {
        }
    }

    private void stopRinging() {
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)){
            stopRinging();
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        stopRinging();
        super.onBackPressed();
    }

    @Override
    public void navigateToNavigation() {
        dismissProgressbar();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void setMapDetails(LatLng currLatLng, LatLng pickUpLatLng) {
        mMarkersList = new ArrayList<>();
        BitmapDescriptor pickupIcon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);
        BitmapDescriptor currIcon = BitmapDescriptorFactory.fromResource(R.drawable.carmap);

        final MarkerOptions pickupMO = new MarkerOptions().position(pickUpLatLng).anchor(0.5f, 0.5f).icon(pickupIcon);
        final MarkerOptions currMO = new MarkerOptions().position(currLatLng).anchor(0.5f, 0.5f).icon(currIcon);

        Marker marker = mMap.addMarker(pickupMO);
        mMarkersList.add(marker);
        marker = mMap.addMarker(currMO);
        mMarkersList.add(marker);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker boundMarker : mMarkersList) {
            builder.include(boundMarker.getPosition());
        }
        final LatLngBounds bounds = builder.build();
        int padding = 0; // offset from edges of the map in pixels
        final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.moveCamera(cameraUpdate);
            }
        });
//        mMap.animateCamera(cameraUpdate);
    }

    @Override
    public void navigateToHome() {
        finish();
    }
}
