package com.josh.driver.modules.request;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.model.LatLng;
import com.hm.Log;
import com.hm.model.BaseModel;
import com.hm.utils.CodeSnippet;
import com.josh.driver.Constants;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.Headers;
import com.josh.driver.dto.request.UpdateDriverLocRequest;
import com.josh.driver.dto.response.Order;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.handler.GPSHandler;
import com.josh.driver.socket.SocketHandler;
import com.josh.driver.socket.SocketKeys;
import com.josh.driver.socket.SocketListener;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public class RequestModelImpl extends BaseModel implements RequestModel, ModelHandler.ModelHanlderListener<ApiStatusDto> {

    private RequestModelListener mRequestModelListener;

    private Order mOrder;

    private int mTaskId;

    public RequestModelImpl(RequestModelListener requestModelListener) {
        super(requestModelListener);

        mRequestModelListener = requestModelListener;
    }

    public static RequestModel getInstance(RequestModelListener requestModelListener) {
        return new RequestModelImpl(requestModelListener);

    }

    @Override
    public void onRequestModelCreate(Order order) {
        mOrder = order;
    }

    @Override
    public void acceptRide() {
        String driverId = DataHandler.getInstance().retrieveDriverId();
        Headers headers = getTokens();

        UpdateDriverLocRequest updateDriverLocRequest =DataHandler.getInstance().retrieveLastKnownLocation();
        updateDriverLocRequest.setLat(updateDriverLocRequest.getLat());
        updateDriverLocRequest.setLon(updateDriverLocRequest.getLon());

        String bookingId = mOrder.getBookingid();
        mTaskId = ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().getAcceptRideRequest(driverId, bookingId, headers, updateDriverLocRequest));
    }

    @Override
    public void cancelAcceptRide() {
        if (mTaskId != -1) {
            ModelHandler.getInstance().dequeueTask(mTaskId);
            mTaskId = -1;
        }
    }

    @Override
    public void getCoOrdinates() {
        List<Double> currentGeo = mOrder.getCurrentgeo();
        List<Double> pickUpGeo = mOrder.getPickupgeo();
        LatLng currLatLng = new LatLng(currentGeo.get(0), currentGeo.get(1));
        LatLng pickUpLatLng = new LatLng(pickUpGeo.get(0), pickUpGeo.get(1));

        mRequestModelListener.onFetchCoOrdinatesSuccess(currLatLng, pickUpLatLng);
    }

    private Headers getTokens() {
        Headers headers = new Headers();
        headers.setAuthToken(DataHandler.getInstance().retrieveAuthToken());
        headers.setUserToken(DataHandler.getInstance().retrieveUserToken());
        headers.setAppToken(Constants.AppKey.APP_TOKEN);
        return headers;
    }

    @Override
    public void onSuccessApi(ApiStatusDto response) {
        if(response.isResultOk()){
            TripDetailsResponseDto tripDetailsResponseDto = CodeSnippet.getObjectFromMap(response.getData(),
                    TripDetailsResponseDto.class);

            tripDetailsResponseDto.setEta(mOrder.getEta());
            DataHandler.getInstance().saveTripDetails(tripDetailsResponseDto);

            mRequestModelListener.onAcceptSuccess(tripDetailsResponseDto);
        }else{
            mRequestModelListener.onFailed(response.getDescription());
        }
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mRequestModelListener.onFailed(errorMessage);
    }

    @Override
    public void onAuthorizationFailed() {

    }


    interface RequestModelListener extends ModelListener {

        void onSuccess();

        void onFailed(String message);

        void onAcceptSuccess(TripDetailsResponseDto tripDetailsResponseDto);

        void onFetchCoOrdinatesSuccess(LatLng currLatLng, LatLng pickUpLatLng);
    }
}
