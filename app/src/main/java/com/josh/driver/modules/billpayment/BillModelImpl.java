package com.josh.driver.modules.billpayment;

import android.content.Context;
import android.content.Intent;

import com.hm.model.BaseModel;
import com.hm.utils.CodeSnippet;
import com.hm.utils.CustomDateFormats;
import com.josh.driver.Constants;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.Headers;
import com.josh.driver.dto.request.UpdateDriverLocRequest;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.handler.AppDataHandler;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

/**
 * Created by hmspl on 13/3/17.
 */

public class BillModelImpl extends BaseModel implements BillModel, ModelHandler.ModelHanlderListener<ApiStatusDto>,
        AppDataHandler.AppDataHandlerCallBack {

    private BillModelListener mBillModelListener;

    private TripDetailsResponseDto mTripDetailsResponseDto;

    private  int mTaskId;

    private Context mContext;

    public BillModelImpl(BillModelListener billModelListener) {
        super(billModelListener);

        mBillModelListener = billModelListener;
    }

    public static BillModel getInstance(BillModelListener billModelListener) {
        return new BillModelImpl(billModelListener);

    }

    @Override
    public void onBillModelCreate(Context context) {
        mContext = context;
        mTripDetailsResponseDto = DataHandler.getInstance().retrieveTripDetails();
        mBillModelListener.onSetTripDetails(mTripDetailsResponseDto);
    }

    @Override
    public void completeTrip() {
        String driverId = DataHandler.getInstance().retrieveDriverId();
        String bookingId ;
        Headers headers = getTokens();

        UpdateDriverLocRequest updateDriverLocRequest =DataHandler.getInstance().retrieveLastKnownLocation();
        updateDriverLocRequest.setLat(updateDriverLocRequest.getLat());
        updateDriverLocRequest.setLon(updateDriverLocRequest.getLon());

        bookingId = mTripDetailsResponseDto.getRid();

        mTaskId = ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().onCompleteTrip(driverId, bookingId, headers,
                updateDriverLocRequest));

    }

    @Override
    public String extractTime(String time) {
        return CodeSnippet.formatDateString(time, CustomDateFormats.SERVER_DATE_FORMAT, CustomDateFormats.HH_MM_AA,
                CustomDateFormats.TIME_ZONE_GMT, CustomDateFormats.INDIA_TIMEZONE);
    }

    @Override
    public String extractDate(String time) {
        return CodeSnippet.formatDateString(time, CustomDateFormats.SERVER_DATE_FORMAT, CustomDateFormats.MMM_DD_YYYY,
                CustomDateFormats.TIME_ZONE_GMT, CustomDateFormats.INDIA_TIMEZONE);
    }

    private Headers getTokens() {
        Headers headers = new Headers();
        headers.setAuthToken(DataHandler.getInstance().retrieveAuthToken());
        headers.setUserToken(DataHandler.getInstance().retrieveUserToken());
        headers.setAppToken(Constants.AppKey.APP_TOKEN);
        return headers;
    }

    @Override
    public void onSuccessApi(ApiStatusDto response) {
        if(response.isResultOk()){
            DataHandler.getInstance().saveTripDetails(null);
            mBillModelListener.onSuccess();
        }else if(response.isResultNotOk()){

            new AppDataHandler(this).fetchBookingDetails(mTripDetailsResponseDto.getRid());

        }else {
            mBillModelListener.onFailed(response.getDescription());
        }
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mBillModelListener.onFailed(errorMessage);
    }

    @Override
    public void onAppDatFetchSuccess() {

    }

    @Override
    public void onAuthorizationFailed() {

    }

    @Override
    public void onAppDataFetchFailed(String message) {

    }

    @Override
    public void onBookingDataFetchSuccess() {
        Intent intent = new Intent(Constants.Receiver.RECEIVER_KEY);
        intent.putExtra(Constants.BundleKey.REQUEST, Constants.AppKey.FORCE_TRIP_STATUS);
        mContext.sendBroadcast(intent);
    }

    @Override
    public void onBookingDataFetchFailed(String message) {
        mBillModelListener.onFailed(message);

    }

    @Override
    public void onForceUpdate() {

    }

    interface BillModelListener extends ModelListener {

        void onSetTripDetails(TripDetailsResponseDto mTripDetailsResponseDto);

        void onSuccess();

        void onFailed(String message);
    }
}
