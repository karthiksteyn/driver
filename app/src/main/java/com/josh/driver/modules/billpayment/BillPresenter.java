package com.josh.driver.modules.billpayment;

/**
 * Created by hmspl on 13/3/17.
 */

public interface BillPresenter {
    void onBillPresenterCreate(BillFragment billFragment);

    void onSubmitButtonClick();

    String getTime(String time);

    String getDate(String time);
}
