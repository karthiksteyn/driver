package com.josh.driver.modules.myhome;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.hm.model.BaseModel;
import com.hm.utils.CodeSnippet;
import com.hm.utils.CustomDateFormats;
import com.josh.driver.Constants;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.Headers;
import com.josh.driver.dto.request.UpdateDriverLocRequest;
import com.josh.driver.dto.response.BalanceSummaryResponseDto;
import com.josh.driver.dto.response.CarTypeResponseDto;
import com.josh.driver.dto.response.Order;
import com.josh.driver.dto.response.UpdateProfileResponseDto;
import com.josh.driver.handler.AppDataHandler;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.handler.GPSHandler;
import com.josh.driver.socket.SocketHandler;
import com.josh.driver.socket.SocketKeys;
import com.josh.driver.socket.SocketListener;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

import java.net.Socket;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by hmspl on 13/3/17.
 */

public class MyHomeModelImpl extends BaseModel implements MyHomeModel,
        ModelHandler.ModelHanlderListener<ApiStatusDto>, AppDataHandler.AppDataHandlerCallBack {

    private MyHomeModelListener mMyHomeModelListener;

    private Context mContext;

    private GPSHandler mGpsHandler;

    private int mCurrentDutyTag;

    private int mOneSecVal = 1000;

    private Location mFromLocation;

    private SocketHandler mSocketHandler;

    public MyHomeModelImpl(MyHomeModelListener myHomeModelListener) {
        super(myHomeModelListener);

        mMyHomeModelListener = myHomeModelListener;

    }

    public static MyHomeModel getInstance(MyHomeModelListener myHomeModelListener) {
        return new MyHomeModelImpl(myHomeModelListener);

    }

    @Override
    public void onMyHomeModelCreate() {
        fetchTransactionDetails();
    }

    @Override
    public void changeControl(int tag) {
        String driverId = DataHandler.getInstance().retrieveDriverId();
        Headers headers = getTokens();

        UpdateDriverLocRequest updateDriverLocRequest =DataHandler.getInstance().retrieveLastKnownLocation();
//        updateDriverLocRequest.setLat(updateDriverLocRequest.getLat());
//        updateDriverLocRequest.setLon(updateDriverLocRequest.getLon());

        mCurrentDutyTag = tag;
        switch(tag) {
            case Constants.AppKey.OFF_DUTY:
                ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().
                    onChangeOffDuty(driverId, headers, updateDriverLocRequest));
                break;
            case Constants.AppKey.ON_DUTY:
                ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().
                        onChangeOnDuty(driverId, headers, updateDriverLocRequest));
                break;
            case Constants.AppKey.GO_HOME:
                ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().
                        onChangeGoHome(driverId, headers, updateDriverLocRequest));
                break;
        }
    }

    private Headers getTokens() {
        Headers headers = new Headers();
        headers.setAuthToken(DataHandler.getInstance().retrieveAuthToken());
        headers.setUserToken(DataHandler.getInstance().retrieveUserToken());
        headers.setAppToken(Constants.AppKey.APP_TOKEN);
        return headers;
    }

    public void fetchTransactionDetails() {
        retrieveTransactionData();
        new AppDataHandler(this).fetchTransactionDetails();
    }

    private void retrieveTransactionData() {
        BalanceSummaryResponseDto balanceSummaryResponseDto = new BalanceSummaryResponseDto();
        DataHandler dataHandler = DataHandler.getInstance();
        balanceSummaryResponseDto.setBalance(dataHandler.retrieveBalance());
        balanceSummaryResponseDto.setMileage(dataHandler.retrieveMileage());
        balanceSummaryResponseDto.setOnlineduration(dataHandler.retrieveOnlineDuration());
        balanceSummaryResponseDto.setRevenue(dataHandler.retrieveRevenue());
        balanceSummaryResponseDto.setTotalrides(dataHandler.retrieveTotalRides());

        String currentCarType = dataHandler.retrieveCurrentType();
        int currentDutyStatus = dataHandler.retrieveDutyStatus();
        mMyHomeModelListener.onFetchedTransactionDetails(balanceSummaryResponseDto, currentCarType, currentDutyStatus);
    }

    @Override
    public void onSuccessApi(ApiStatusDto apiStatusDto) {
        if (apiStatusDto.isResultOk()) {
            DataHandler.getInstance().saveDutyStatus(mCurrentDutyTag);
            mMyHomeModelListener.onChangeSuccess(mCurrentDutyTag);
        } else {
            mMyHomeModelListener.onFetchDataFailed(apiStatusDto.getDescription());
        }
    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mMyHomeModelListener.onFetchDataFailed(errorMessage);
    }

    @Override
    public void onAppDatFetchSuccess() {
        retrieveTransactionData();

    }

    @Override
    public void onAuthorizationFailed() {

    }

    @Override
    public void onAppDataFetchFailed(String message) {
        retrieveTransactionData();
    }

    @Override
    public void onBookingDataFetchSuccess() {

    }

    @Override
    public void onBookingDataFetchFailed(String message) {

    }

    @Override
    public void onForceUpdate() {

    }


    interface MyHomeModelListener extends ModelListener {

        void onFetchedTransactionDetails(BalanceSummaryResponseDto balanceSummaryResponseDto, String currentCarType, int currentDutyStatus);

        void onFetchDataFailed(String msg);

        void onChangeSuccess(int currentDutyTag);

    }
}
