package com.josh.driver.modules.recharge;

import com.hm.activity.View;
import com.josh.driver.dto.ContactDto;
import com.josh.driver.dto.PastTripDto;
import com.josh.driver.dto.request.PaymentRequestDto;

import java.util.List;

/**
 * Created by hmspl on 13/3/17.
 */

public interface TopupView extends View {
    PaymentRequestDto getPaymentDetails();

    void NavigateToPaytm();

    void showContactListDialog(List<ContactDto> contactDtoList);

    void checkContactPermission();
}
