package com.josh.driver.modules.user.forgotpassword;

import android.content.Context;
import android.os.Bundle;

/**
 * Created by hmspl on 13/3/17.
 */

public interface ForgotPasswordPresenter {

    void onSendClick(Context context);
}
