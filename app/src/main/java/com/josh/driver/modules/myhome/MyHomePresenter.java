package com.josh.driver.modules.myhome;

import android.content.Context;

/**
 * Created by hmspl on 13/3/17.
 */

public interface MyHomePresenter {
    void onMyHomePresenterCreate();

    void onDutyControlClick(MyHomeFragment myHomeFragment, int tag);
}
