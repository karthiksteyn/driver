package com.josh.driver.modules.user.login;

import com.hm.activity.View;
import com.josh.driver.dto.request.LoginRequestDto;

/**
 * Created by Sugan on 30/3/17.
 */

public interface LoginView extends View{

    LoginRequestDto getLoginDetails();

    void navigateToHome();

    void navigateToForgotPwd();
}
