package com.josh.driver.modules.request;


import android.content.Context;
import android.os.Bundle;

/**
 * Created by hmspl on 13/3/17.
 */

public interface RequestPresenter {

    void onRequestPresenterCreate(Bundle extras);

    void onAcceptClick(Context context);

    void onAcceptCancel();

    void onSetupMapDetails();
}
