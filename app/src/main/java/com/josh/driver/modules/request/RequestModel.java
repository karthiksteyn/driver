package com.josh.driver.modules.request;

import android.content.Context;

import com.josh.driver.dto.response.Order;

/**
 * Created by hmspl on 13/3/17.
 */

public interface RequestModel {
    void onRequestModelCreate(Order order);

    void acceptRide();

    void cancelAcceptRide();

    void getCoOrdinates();
}
