package com.josh.driver.modules.details.pastorders;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.hm.fragment.BaseFragment;
import com.josh.driver.R;
import com.josh.driver.adapter.CommonAdapter;
import com.josh.driver.dto.PastTripDto;
import com.josh.driver.modules.details.orderdetails.TripDetailsActivity;

import java.util.List;


public class PastTripsFragment extends BaseFragment implements PastTripsView,
        CommonAdapter.OnGetViewListener<PastTripDto>, AdapterView.OnItemClickListener {


    private PastTripsPresenter mPastTripsPresenter;

    public PastTripsFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPastTripsPresenter = PastTripsPresenterImpl.getInstance(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_past_trips, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPastTripsPresenter.onPastTripsPresenterCreate();
    }


    @Override
    public void setPastTrips(List<PastTripDto> pastTripList) {
        // update the listview with trip list
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, PastTripDto object, int viewId) {
        return null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        dismissProgressbar();
        Intent intent = new Intent(getContext(), TripDetailsActivity.class);
        startActivity(intent);
    }
}
