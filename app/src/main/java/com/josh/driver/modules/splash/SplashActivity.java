package com.josh.driver.modules.splash;

import android.os.Bundle;

import com.hm.activity.BaseActivity;
import com.josh.driver.R;

public class SplashActivity extends BaseActivity implements SplashView {

    private SplashPresenter mSplashPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mSplashPresenter = SplashPresenterImpl.getInstance(this);
    }

    @Override
    public void navigateToWalkThrough() {

    }

    @Override
    public void navigateToLogin() {

    }

    @Override
    public void navigateToOtpVerification() {

    }

    @Override
    public void navigateToProfileSetup() {

    }

    @Override
    public void navigateToMenu() {

    }
}
