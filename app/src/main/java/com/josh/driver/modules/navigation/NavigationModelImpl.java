package com.josh.driver.modules.navigation;

import android.content.Context;
import android.content.Intent;

import com.google.android.gms.maps.model.LatLng;
import com.hm.model.BaseModel;
import com.hm.utils.CodeSnippet;
import com.josh.driver.AppSnippet;
import com.josh.driver.Constants;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.Headers;
import com.josh.driver.dto.request.CancelRequest;
import com.josh.driver.dto.request.UpdateDriverLocRequest;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.googlemap.DirectionsLibrary;
import com.josh.driver.googlemap.PolyLineDto;
import com.josh.driver.dto.googlemapdto.Route;
import com.josh.driver.handler.AppDataHandler;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

import java.util.List;

/**
 * Created by Sugan on 30/3/17.
 */

public class NavigationModelImpl extends BaseModel implements NavigationModel, ModelHandler.ModelHanlderListener<ApiStatusDto>,
        DirectionsLibrary.OnDirectionReceived, AppDataHandler.AppDataHandlerCallBack {

    private static final int CANCEL_CLICK = 1;
    private static final int MORPHING_CLICK = 2;
    private static final int CALL_CLICK = 3;

    private NavigationModelListener mNavigationModelListener;

    private  int mTaskId;

    private Context mContext;

    private  int mStatus;

    private int mClickedComponent;

    private TripDetailsResponseDto mTripDetailsResponseDto;

    private DirectionsLibrary mDirectionsLibrary;

    public NavigationModelImpl(NavigationModelListener navigationModelListener) {
        super(navigationModelListener);

        mDirectionsLibrary = new DirectionsLibrary(this);
        mNavigationModelListener = navigationModelListener;
    }

    public static NavigationModel getInstance(NavigationModelListener navigationModelListener) {
        return new NavigationModelImpl(navigationModelListener);
    }

    @Override
    public void onNavigationModelCreate(Context context) {
        mContext = context;
        mTripDetailsResponseDto = DataHandler.getInstance().retrieveTripDetails();
        mNavigationModelListener.onSetTripDetails(mTripDetailsResponseDto);
    }

    @Override
    public void cancelTrip() {
        mClickedComponent = CANCEL_CLICK;
        String driverId = DataHandler.getInstance().retrieveDriverId();
        String bookingId = mTripDetailsResponseDto.getRid();
        Headers headers = getTokens();

        CancelRequest cancelRequest = new CancelRequest();
        cancelRequest.setReason("Not Interested");

        mTaskId = ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().onCancelTrip(driverId, bookingId, cancelRequest, headers));
    }

    @Override
    public void processTrip() {
        mClickedComponent = MORPHING_CLICK;

        String driverId = DataHandler.getInstance().retrieveDriverId();
        String bookingId ;
        Headers headers = getTokens();

        UpdateDriverLocRequest updateDriverLocRequest =DataHandler.getInstance().retrieveLastKnownLocation();
        updateDriverLocRequest.setLat(updateDriverLocRequest.getLat());
        updateDriverLocRequest.setLon(updateDriverLocRequest.getLon());

        bookingId = mTripDetailsResponseDto.getRid();
        mStatus = mTripDetailsResponseDto.getStatus();

        switch(mStatus){
            case Constants.TripStatus.WAIT_LOCATE:
                mTaskId = ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().onLocatedTrip(driverId, bookingId, headers, updateDriverLocRequest));
                break;
            case Constants.TripStatus.WAIT_VERIFY:
                mTaskId = ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().onVerifiedTrip(driverId, bookingId, headers, updateDriverLocRequest));
                break;
            case Constants.TripStatus.WAIT_START:
                mTaskId = ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().onStartTrip(driverId, bookingId, headers, updateDriverLocRequest));
                break;
            case Constants.TripStatus.WAIT_STOP:
                mTaskId = ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().onStopTrip(driverId, bookingId, headers, updateDriverLocRequest));
                break;

        }
    }

    @Override
    public void makeCall() {
        mClickedComponent = CALL_CLICK;
        String driverId = DataHandler.getInstance().retrieveDriverId();
        String bookingId = mTripDetailsResponseDto.getRid();
        Headers headers = getTokens();

        mTaskId = ModelHandler.getInstance().enqueueTask(this, RequestCreator.getInstance().onCallCustomer(driverId, bookingId, headers));
    }

    @Override
    public LatLng getCoOrdinates() {
        LatLng customerLocation;

        if(mTripDetailsResponseDto.getStatus() < Constants.TripStatus.WAIT_START){
            customerLocation = new LatLng(mTripDetailsResponseDto.getFromloc().get(0), mTripDetailsResponseDto.getFromloc().get(1));
        }else {
            if(mTripDetailsResponseDto.getToloc() != null) {
                customerLocation = new LatLng(mTripDetailsResponseDto.getToloc().get(0), mTripDetailsResponseDto.getToloc().get(1));
            }else{
                customerLocation = null;
            }

        }
        return customerLocation;
    }

    @Override
    public String fetchDropAddress() {

        return AppSnippet.replaceEmpty((DataHandler.getInstance().retrieveTripDetails()).getTo());
    }

    @Override
    public void fetchRoute(String apiKey) {
        LatLng toLatLng = getCoOrdinates();
        UpdateDriverLocRequest updateDriverLocRequest = DataHandler.getInstance().retrieveLastKnownLocation();

//        String fromLatLngString = updateDriverLocRequest.getLat()+","+updateDriverLocRequest.getLon();
//        String toLatLngString = toLatLng.latitude+","+toLatLng.longitude;

        //Method for Google map Navigation
        mNavigationModelListener.onFetchSuccess(toLatLng);

        //Method for Draw polyline
        /*mDirectionsLibrary.initSearchDirections(fromLatLngString, toLatLngString, apiKey);*/
    }

    @Override
    public void getChangeMarker() {
        UpdateDriverLocRequest updateDriverLocRequest = DataHandler.getInstance().retrieveLastKnownLocation();
        mNavigationModelListener.onFetchCoOrdinatesSuccess(updateDriverLocRequest.getLat(), updateDriverLocRequest.getLon());
    }

    @Override
    public void fetchCancelReason() {
        String driverId = DataHandler.getInstance().retrieveDriverId();
        Headers headers = getTokens();

        mTaskId = ModelHandler.getInstance().enqueueTask(new ModelHandler.ModelHanlderListener<ApiStatusDto>() {

            @Override
            public void onSuccessApi(ApiStatusDto response) {

            }

            @Override
            public void onFailureApi(int errorCode, String errorMessage) {

            }

            @Override
            public void onAuthorizationFailed() {

            }
        }, RequestCreator.getInstance().getCancelReasons(driverId, headers));
    }

    private Headers getTokens() {
        Headers headers = new Headers();
        headers.setAuthToken(DataHandler.getInstance().retrieveAuthToken());
        headers.setUserToken(DataHandler.getInstance().retrieveUserToken());
        headers.setAppToken(Constants.AppKey.APP_TOKEN);
        return headers;
    }

    @Override
    public void onSuccessApi(ApiStatusDto response) {
        if(response.isResultOk()){
            switch(mClickedComponent){
                case CANCEL_CLICK:
                    DataHandler.getInstance().saveTripDetails(null);
                    mNavigationModelListener.onCancelSuccess();
                    break;
                case MORPHING_CLICK:
                    mStatus += 1;
                    if(mStatus == Constants.TripStatus.WAIT_COMPLETE){
                        mTripDetailsResponseDto = CodeSnippet.getObjectFromMap(response.getData(), TripDetailsResponseDto.class);
                    }else {
                        mTripDetailsResponseDto.setStatus(mStatus);
                    }
                    DataHandler.getInstance().saveTripDetails(mTripDetailsResponseDto);
                    mNavigationModelListener.onChangeStatusSuccess(mStatus);
                    break;
                case CALL_CLICK:
                    mNavigationModelListener.onFailed(response.getDescription());
                    break;
            }

        } else if(response.isResultNotOk()){

            new AppDataHandler(this).fetchBookingDetails(mTripDetailsResponseDto.getRid());

        } else{
            mNavigationModelListener.onFailed(response.getDescription());

        }

    }

    @Override
    public void onFailureApi(int errorCode, String errorMessage) {
        mNavigationModelListener.onFailed(errorMessage);

    }

    @Override
    public void onAppDatFetchSuccess() {

    }

    @Override
    public void onAuthorizationFailed() {

    }

    @Override
    public void onAppDataFetchFailed(String message) {

    }

    @Override
    public void onBookingDataFetchSuccess() {

        mNavigationModelListener.onCloseProgress();

        Intent intent = new Intent(Constants.Receiver.RECEIVER_KEY);
        intent.putExtra(Constants.BundleKey.REQUEST, Constants.AppKey.FORCE_TRIP_STATUS);
        mContext.sendBroadcast(intent);
    }

    @Override
    public void onBookingDataFetchFailed(String message) {
        mNavigationModelListener.onFailed(message);
    }

    @Override
    public void onForceUpdate() {

    }

    @Override
    public void getDirectionSuccess(List<Route> routeList, List<PolyLineDto> polyLineDtos) {

        if (routeList != null && routeList.size() > 0) {

            mNavigationModelListener.setDirections(polyLineDtos);
        } else {
            mNavigationModelListener.onNoRoutesAvailable();
        }
    }

    @Override
    public void onGetDirectionFailed() {
        mNavigationModelListener.onNoRoutesAvailable();
    }


    interface NavigationModelListener extends ModelListener {

        void onSetTripDetails(TripDetailsResponseDto tripDetailsResponseDto);

        void onCancelSuccess();

        void onFailed(String message);

        void onChangeStatusSuccess(int status);

        void onFetchCoOrdinatesSuccess(double lat, double lng);

        void setDirections(List<PolyLineDto> polyLineDtos);

        void onNoRoutesAvailable();

        void onFetchSuccess(LatLng toLatLng);

        void onCloseProgress();
    }
}
