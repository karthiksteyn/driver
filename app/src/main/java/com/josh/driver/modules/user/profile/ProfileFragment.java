package com.josh.driver.modules.user.profile;


import android.Manifest;
import android.animation.Animator;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.hm.fragment.BaseFragment;
import com.hm.handler.PermissionHandler;
import com.hm.widgets.CustomEditText;
import com.josh.driver.AppSnippet;
import com.josh.driver.Constants;
import com.josh.driver.JoshDriver;
import com.josh.driver.R;
import com.josh.driver.dto.ProfileDto;
import com.josh.driver.dto.UserDto;
import com.josh.driver.handler.ImageUpdateHandler;
import com.josh.driver.modules.user.login.LoginActivity;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment implements ProfileView, View.OnClickListener,
        CustomEditText.CustomEditTextKeyboardListener, ImageUpdateHandler.ImageUpdateHandleListener {

    private static final String TAG = ProfileFragment.class.getSimpleName();
    private ProfilePresenter mProfilePresenter;
    private CustomEditText mNameEt, mPhoneNumberEt, mDefaultCabEt, mSelectedCabEt,
            mVehicleEt;
    private Button mChPwdOrLoBtn;
    private ImageView mProfileImage;
    private ImageUpdateHandler mImageUpdateHandler;
    private Toolbar mToolbar;
    private RelativeLayout mParentLayout;
    private AlertDialog mAlertDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProfilePresenter = ProfilePresenterImpl.getInstance(this);
        mImageUpdateHandler = ImageUpdateHandler.getInstance(this, this, false, getContext());

    }

    @Override
    public void onStart() {
        super.onStart();
//        AppSnippet.hideBottomNavigation(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        mNameEt = (CustomEditText) view.findViewById(R.id.profile_fragment_name_et);
        mPhoneNumberEt = (CustomEditText) view.findViewById(R.id.profile_fragment_phone_et);
        mDefaultCabEt = (CustomEditText) view.findViewById(R.id.profile_fragment_default_cab_et);
        mSelectedCabEt = (CustomEditText) view.findViewById(R.id.profile_fragment_selected_cab_et);
        mVehicleEt = (CustomEditText) view.findViewById(R.id.profile_fragment_vehicle_et);

        mParentLayout = (RelativeLayout) view.findViewById(R.id.fragment_profile_layout);
        mProfileImage = (ImageView) view.findViewById(R.id.fragment_profile_img);
        mChPwdOrLoBtn = (Button) view.findViewById(R.id.profile_fragment_logout_change_pwd_btn);

        mProfileImage.setOnClickListener(this);
        mChPwdOrLoBtn.setOnClickListener(this);

        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mProfilePresenter.onProfilePresenterCreate();
    }

    @Override
    public void setDetails(ProfileDto profileDto) {
        // update the view by ProfileDto
        mNameEt.setText(profileDto.getName());
        mPhoneNumberEt.setText(profileDto.getPhone());
        mVehicleEt.setText(profileDto.getVehicle());
        mDefaultCabEt.setText(getCabTypeName(profileDto.getDefaultType()));
        mSelectedCabEt.setText(getCabTypeName(profileDto.getSelectedType()));

    }

    private String getCabTypeName(String type) {
        if(type.equals(getString(R.string.short_co))){
            return getString(R.string.co);
        }else if(type.equals(getString(R.string.short_se))){
            return getString(R.string.se);
        }else if(type.equals(getString(R.string.short_sp))){
            return getString(R.string.sp);
        }
        return getString(R.string.unknown);
    }

    @Override
    public void navigateToLogin() {
        dismissProgressbar();
        JoshDriver.getInstance().stopService();
        Intent intent = new Intent(getContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mImageUpdateHandler.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void checkStoragePermission() {
        getBaseActivity().mPermissionHandler = PermissionHandler.getInstance(new PermissionHandler.PermissionHandleListener() {
            @Override
            public void onPermissionGranted(int requestCode) {
                mImageUpdateHandler.onTakeProfilePic();
            }

            @Override
            public void onPermissionDenied(int requestCode) {

            }

            @Override
            public void onPermissionReqCancel() {

            }
        });
        getBaseActivity().mPermissionHandler.checkPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE,
                1, Constants.PermissionMessages.CAMERA_DENIED);
    }

    private void getLogOutConfirmation() {
        // Logout confirmation dialog
        mAlertDialog = AppSnippet.showAlertDialog(getActivity(), Constants.AlertMessage.LOG_OUT_TITLE
                , Constants.AlertMessage.LOG_OUT_CONTENT, Constants.AlertMessage.YES, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        AppSnippet.hideBottomNavigation(getActivity());
                        mAlertDialog.dismiss();
                        mProfilePresenter.onLogoutClick();
                    }
                }, Constants.AlertMessage.CANCEL, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        AppSnippet.hideBottomNavigation(getActivity());
                        mAlertDialog.dismiss();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_profile_img:
//                mProfilePresenter.onTakeImageClick();
                break;
            case R.id.profile_fragment_logout_change_pwd_btn:
                getLogOutConfirmation();
                break;
        }
    }


    @Override
    public void onKeyDownPressed() {
        hideSoftKeyboard();
//        AppSnippet.hideBottomNavigation(getActivity());
    }

    @Override
    public void onTakePicSuccess(Uri picUri, Bitmap bitmap) {
        Log.e(TAG, "onTakePicSuccess: " + picUri);
        Glide.with(getContext()).load(picUri).placeholder(R.drawable.ic_camera)
                .error(R.drawable.ic_camera)
                .bitmapTransform(new CropCircleTransformation(getContext()))
                .into(mProfileImage);
    }

    @Override
    public void checkHasPermission(ImageUpdateHandler.CheckPermissionListener checkPermissionListener) {
        if (PermissionHandler.getInstance(null)
                .isPermissionGranted(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            checkPermissionListener.onPermissionGrant();
        } else {
            checkPermissionListener.onPermissionFailed();
        }
    }

    @Override
    public void onTakePicFailed() {

    }

    @Override
    public void onTakePicCancelled(String message) {

    }
}
