package com.josh.driver.modules;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.josh.driver.R;

public class AdditionalSettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_settings);

        onSetAdditionalDetails(getIntent().getExtras());
    }

    private void onSetAdditionalDetails(Bundle bundle) {
        /**
         * get details from bundle
         * set the data to corresponding fields
         */
    }
}
