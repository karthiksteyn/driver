package com.josh.driver.modules.user.otpverification;

import android.content.Context;

/**
 * Created by hmspl on 13/3/17.
 */

public interface OtpVerificationPresenter {
    void onOtpVerificationPresenterCreate(String context);

    void onVerifyClick(Context context);

    void onResendVerificationCodeClick(Context context);

}
