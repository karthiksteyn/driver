package com.josh.driver.modules.user.otpverification;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hm.activity.BaseActivity;
import com.hm.widgets.CustomEditText;
import com.josh.driver.AppSnippet;
import com.josh.driver.Constants;
import com.josh.driver.R;
import com.josh.driver.modules.user.resetpassword.ResetPasswordActivity;

public class OtpVerificationActivity extends BaseActivity implements OtpVerificationView,
        View.OnClickListener, CustomEditText.CustomEditTextKeyboardListener {

    private OtpVerificationPresenter mOtpVerificationPresenter;
    private CustomEditText mOtpEt;
    private Button mResendOtpBtn, mResetPwdBtn;
    private TextView mContentTv;
    private TextView mTimer;
    private RelativeLayout mTimerHolder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);

        setupToolbar();

        mOtpVerificationPresenter = OtpVerificationPresenterImpl.getInstance(this);
        mOtpEt = (CustomEditText) findViewById(R.id.activity_otp_verification_et);
        mResendOtpBtn = (Button) findViewById(R.id.activity_otp_verification_resend_btn);
        mResetPwdBtn = (Button) findViewById(R.id.activity_otp_verification_reset_btn);
        mContentTv = (TextView) findViewById(R.id.activity_otp_verification_content_tv);
        mTimer = (TextView) findViewById(R.id.activity_otp_verification_time_count);
        mTimerHolder = (RelativeLayout) findViewById(R.id.activity_otp_verification_time_holder);

        mOtpVerificationPresenter.onOtpVerificationPresenterCreate(/*getIntent().getExtras().getString(Constants.BundleKey.PHONE_NUMBER)*/"9789194645");

        startTimer();

//        mOtpEt.setCustomKeyBoardListener(this);
        mResendOtpBtn.setOnClickListener(this);
        mResetPwdBtn.setOnClickListener(this);
    }

    private void setupToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageView optionIv = (ImageView) toolbar.findViewById(R.id.action_iv);
        ImageView profileIv = (ImageView) toolbar.findViewById(R.id.user_dp_iv);
        TextView titleTextView = (TextView) toolbar.findViewById(R.id.toolbar_title_tv);
        TextView balanceTextView = (TextView) toolbar.findViewById(R.id.toolbar_balance_tv);
        RelativeLayout walletLayout = (RelativeLayout) toolbar.findViewById(R.id.wallet_layout);

        optionIv.setImageResource(R.drawable.ic_back);
        optionIv.setOnClickListener(this);

        walletLayout.setVisibility(View.GONE);

        titleTextView.setText(getString(R.string.verification));
        profileIv.setVisibility(View.GONE);

    }

    @Override
    protected void onStart() {
        super.onStart();
//        AppSnippet.hideBottomNavigation(this);
    }

    @Override
    public void startTimer() {
        new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                mTimer.setText(Long.toString(millisUntilFinished / 1000));
            }

            public void onFinish() {
                mTimer.setText("0");
                mTimerHolder.setVisibility(View.INVISIBLE);
                mResendOtpBtn.setEnabled(true);
                mResendOtpBtn.setAlpha(1);
            }
        }.start();
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        String content = mContentTv.getText().toString();
        String newContent = content.replace(Constants.BundleKey.PHONE_NUMBER, phoneNumber);
        mContentTv.setText(newContent);
    }

    @Override
    public String getVerificationCode() {
        return mOtpEt.getText().toString();
    }

    @Override
    public void NavigateToResetPwd() {
        dismissProgressbar();
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_otp_verification_resend_btn:
                mOtpVerificationPresenter.onResendVerificationCodeClick(this);
                break;
            case R.id.activity_otp_verification_reset_btn:
                mOtpVerificationPresenter.onVerifyClick(this);
                break;
        }
    }

    @Override
    public void onKeyDownPressed() {
//        hideSoftKeyboard();
//        AppSnippet.hideBottomNavigation(this);
    }
}
