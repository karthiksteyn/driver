package com.josh.driver.notification;

import android.content.Context;

import java.util.Random;

/**
 * Created by Sugan on 14/3/17.
 */
public abstract class BaseNotification {

    public abstract void showNotification(Context context, String notificationData);

    protected int getNotificationId() {
        return new Random().nextInt(80 - 65) + 65;
    }

}