package com.josh.driver.notification;

/**
 * Created by Sugan on 14/3/17.
 */

public class NotificationFactory {

    public static BaseNotification getNotification(String notificationType) {
        switch (notificationType) {
            case NotificationKey.WELCOME:
                return new GeneralNotification();

        }
        return null;
    }
}