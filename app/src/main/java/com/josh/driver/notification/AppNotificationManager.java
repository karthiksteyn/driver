package com.josh.driver.notification;

import android.content.Context;

import com.hm.Log;

import java.util.Map;


/**
 * Created by Sugan on 14/3/17.
 */
public class AppNotificationManager {

    private static final String TAG = "AppNotificationManager";

    private static AppNotificationManager sNotificationManager = new AppNotificationManager();

    public static AppNotificationManager getNotificationManager() {
        return sNotificationManager;
    }

    public void handleNotification(Context context, Map<String, String> dataMap) {
        if (null != dataMap) {
            String type = NotificationKey.TYPE;
            Log.i(TAG, "NotificationCalled : " + type);
            if (dataMap.containsKey(type)) {
                String data = NotificationKey.DATA;
                if (dataMap.containsKey(data)) {
                    String bundleData = dataMap.get(data);
                    BaseNotification baseNotification = NotificationFactory
                            .getNotification(dataMap.get(type));
                    if (null != baseNotification) {
                        baseNotification.showNotification(context, bundleData);
                    }
                }
            }
        }
    }

    public void clearAllNotifications(){
        sNotificationManager.clearAllNotifications();
    }

}