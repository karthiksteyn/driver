package com.josh.driver.notification;

/**
 * Created by Sugan on 14/3/17.
 */
public interface NotificationKey {

    String TYPE = "key";
    String DATA = "value";
    String WELCOME = "welcome";

}
