package com.josh.driver.webservice;



import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.Headers;
import com.josh.driver.dto.UserDto;
import com.josh.driver.dto.request.CabTypeRequest;
import com.josh.driver.dto.request.CancelRequest;
import com.josh.driver.dto.request.LoginRequestDto;
import com.josh.driver.dto.request.PastTripsRequestDto;
import com.josh.driver.dto.request.PaymentRequestDto;
import com.josh.driver.dto.request.RatingRequestDto;
import com.josh.driver.dto.googlemapdto.DirectionsResponseDto;
import com.josh.driver.dto.request.ResetPwdRequestDto;
import com.josh.driver.dto.request.SendOtpRequestDto;
import com.josh.driver.dto.request.UpdateDriverLocRequest;
import com.josh.driver.dto.request.VerifyOtpRequestDto;

import retrofit2.Call;

/**
 * Created by Sridevi on 7/3/17.
 */

public class RequestCreator {

    private static RequestCreator sRequestCreator = null;

    public static RequestCreator getInstance(){
        if(sRequestCreator == null) {
            sRequestCreator = new RequestCreator();
        }
        return sRequestCreator;
    }

    //EndPointService Creators

    public Call<ApiStatusDto> getLoginRequest(LoginRequestDto loginRequestDto){
        return ApiHandler.getEndPointService().login(loginRequestDto);
    }

    public Call<ApiStatusDto> getTransactionDataRequest(String driverId, String currentDate, Headers headers) {

        return ApiHandler.getEndPointService().transactionData(driverId, currentDate, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken());
    }

    public Call<ApiStatusDto> getCabTypes(String driverId, Headers headers) {
        return ApiHandler.getEndPointService().cabTypes(driverId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken());
    }

    public Call<ApiStatusDto> onCabTypeChange(String driverId, Headers headers, CabTypeRequest cabTypeRequest) {
        return ApiHandler.getEndPointService().cabTypeChange(cabTypeRequest, driverId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken());
    }

    public Call<ApiStatusDto> getProfilePicUpdateApiRequest(String profilePicPath) {
        return ApiHandler.getCdnPointService().uploadImage();
    }

    public Call<ApiStatusDto> onChangeOffDuty(String driverId, Headers headers, UpdateDriverLocRequest updateDriverLocRequest) {
        return ApiHandler.getEndPointService().offDuty(driverId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken(), updateDriverLocRequest);
    }

    public Call<ApiStatusDto> onChangeOnDuty(String driverId, Headers headers, UpdateDriverLocRequest updateDriverLocRequest) {
        return ApiHandler.getEndPointService().onDuty(driverId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken(), updateDriverLocRequest);
    }

    public Call<ApiStatusDto> onChangeGoHome(String driverId, Headers headers, UpdateDriverLocRequest updateDriverLocRequest) {
        return ApiHandler.getEndPointService().goHome(driverId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken(), updateDriverLocRequest);
    }

    public Call<ApiStatusDto> getAcceptRideRequest(String driverId, String bookingId, Headers headers, UpdateDriverLocRequest updateDriverLocRequest) {
        return ApiHandler.getEndPointService().acceptRide(driverId, bookingId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken(), updateDriverLocRequest);
    }

    public Call<ApiStatusDto> getBookingDataRequest(String driverId, String bookingId, Headers headers) {
        return ApiHandler.getEndPointService().bookingData(driverId, bookingId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken());
    }

    public Call<ApiStatusDto> getCancelReasons(String driverId, Headers headers) {
        return ApiHandler.getEndPointService().cancelReasons(driverId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken());
    }

    public Call<ApiStatusDto> onCancelTrip(String driverId, String bookingId, CancelRequest cancelRequest, Headers headers) {
        return ApiHandler.getEndPointService().cancelTrip(driverId, bookingId, cancelRequest, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken());
    }

    public Call<ApiStatusDto> onLocatedTrip(String driverId, String bookingId, Headers headers, UpdateDriverLocRequest updateDriverLocRequest) {
        return ApiHandler.getEndPointService().customerLocated(driverId, bookingId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken(), updateDriverLocRequest);
    }

    public Call<ApiStatusDto> onVerifiedTrip(String driverId, String bookingId, Headers headers, UpdateDriverLocRequest updateDriverLocRequest) {
        return ApiHandler.getEndPointService().customerVerified(driverId, bookingId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken(), updateDriverLocRequest);
    }

    public Call<ApiStatusDto> onStartTrip(String driverId, String bookingId, Headers headers, UpdateDriverLocRequest updateDriverLocRequest) {
        return ApiHandler.getEndPointService().startTrip(driverId, bookingId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken(), updateDriverLocRequest);
    }

    public Call<ApiStatusDto> onStopTrip(String driverId, String bookingId, Headers headers, UpdateDriverLocRequest updateDriverLocRequest) {
        return ApiHandler.getEndPointService().stopTrip(driverId, bookingId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken(), updateDriverLocRequest);
    }

    public Call<ApiStatusDto> onCompleteTrip(String driverId, String bookingId, Headers headers, UpdateDriverLocRequest updateDriverLocRequest) {
        return ApiHandler.getEndPointService().completeTrip(driverId, bookingId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken(), updateDriverLocRequest);
    }

    public Call<ApiStatusDto> onCallCustomer(String driverId, String bookingId, Headers headers) {
        return ApiHandler.getEndPointService().callCustomer(driverId, bookingId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken());
    }

    public Call<ApiStatusDto> onVerifyMobile(String driverId, Headers headers, PaymentRequestDto paymentRequestDto) {
        return ApiHandler.getEndPointService().verifyMobile(driverId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken(), paymentRequestDto);
    }

    public Call<ApiStatusDto> onSendOtp(SendOtpRequestDto sendOtpRequestDto) {
        return ApiHandler.getEndPointService().sendOtp(sendOtpRequestDto);
    }

    public Call<ApiStatusDto> getVerificationRequest(VerifyOtpRequestDto verifyOtpRequestDto) {
        return ApiHandler.getEndPointService().verifyOtp(verifyOtpRequestDto);
    }

    public Call<ApiStatusDto> getResetPwdRequest(ResetPwdRequestDto resetPwdRequestDto) {
        return ApiHandler.getEndPointService().resetPwd(resetPwdRequestDto);
    }

    public Call<ApiStatusDto> getLogOutRequest(String driverId, Headers headers) {
        return ApiHandler.getEndPointService().logOut(driverId, headers.getAppToken(),
                headers.getAuthToken(), headers.getUserToken());
    }





    //GoogleDirectionPointService Creators

    public Call<DirectionsResponseDto> onRouteDirecion(String fromLatLng, String toLatLng, String apiKey) {
        return ApiHandler.getGoogleDirectionPointService().getDirection(fromLatLng, toLatLng, apiKey);
    }




    public Call<ApiStatusDto> getUpdateProfileRequest(UserDto userDto) {
        return ApiHandler.getEndPointService().updateProfile(userDto);
    }

    public Call<ApiStatusDto> postSosMessage() {
        return ApiHandler.getEndPointService().sendSos();
    }

    public Call<ApiStatusDto> getPastTripsRequest(PastTripsRequestDto pastTripsRequestDto) {
        return ApiHandler.getEndPointService().pastTrips(pastTripsRequestDto);

    }

    public Call<ApiStatusDto> getTripDetailsRequest(String tripId) {
        return ApiHandler.getEndPointService().tripDetails(tripId);
    }

    public Call<ApiStatusDto> getRatingRequest(RatingRequestDto ratingRequestDto) {
        return ApiHandler.getEndPointService().submitRating(ratingRequestDto);
    }

    public Call<ApiStatusDto> getPaymentRequest(PaymentRequestDto paymentRequestDto) {
        return ApiHandler.getEndPointService().payment(paymentRequestDto);
    }




    //GoogleLocationPointService Creators

    /*public Call<GoogleAutoResponse> getAutoCompleteForCountryRequest(String countryCode, String searchText){
        String country = "country:" + countryCode;
        return ApiHandler.getGoogleLocationPointService().autoCompleteForSpecificCountry(Constants.AppKeys
                .GOOGLE_API_KEY, country, searchText);
    }

    public Call<GoogleAutoResponse> getAutoCompleteRequest(String searchText){
        return ApiHandler.getGoogleLocationPointService().autoComplete(Constants.AppKeys.GOOGLE_API_KEY,
                searchText);
    }

    public Call<GoogleAddressResponse> getAddressFromLatLngRequest(Double lat, Double lng){
        String latLng = lat + "," + lng;
        return ApiHandler.getGoogleLocationPointService().getAddressFromLatLng(latLng, "true");
    }

    public Call<GoogleAddressResponse> getAddressFromPlaceIdRequest(String placeId){
        return ApiHandler.getGoogleLocationPointService().getAddressFromPlaceId(Constants.AppKeys.GOOGLE_API_KEY,
                placeId);
    }*/
}
