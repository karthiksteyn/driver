package com.josh.driver.webservice;



import com.josh.driver.Constants.Headers;
import com.josh.driver.dto.ApiStatusDto;
import com.josh.driver.dto.UserDto;
import com.josh.driver.dto.request.CabTypeRequest;
import com.josh.driver.dto.request.CancelRequest;
import com.josh.driver.dto.request.LoginRequestDto;
import com.josh.driver.dto.request.PastTripsRequestDto;
import com.josh.driver.dto.request.PaymentRequestDto;
import com.josh.driver.dto.request.RatingRequestDto;
import com.josh.driver.dto.request.ResetPwdRequestDto;
import com.josh.driver.dto.request.SendOtpRequestDto;
import com.josh.driver.dto.request.UpdateDriverLocRequest;
import com.josh.driver.dto.request.VerifyOtpRequestDto;
import com.josh.driver.dto.response.AppDataResponseDto;
import com.josh.driver.dto.response.PastTripsResponseDto;
import com.josh.driver.dto.response.PaymentResponseDto;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.dto.response.UpdateProfileResponseDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface EndPointService{

    @POST("drivers/signin")
    Call<ApiStatusDto> login(@Body LoginRequestDto loginRequestDto);

    @GET("drivers/{driverID}/transaction/daily/{searchdate}")
    Call<ApiStatusDto> transactionData(@Path("driverID") String driverId, @Path("searchdate") String currentDate,
                                             @Header(Headers.APP_AUTHORIZATION) String appToken,
                                             @Header(Headers.AUTHORIZATION) String authToken,
                                             @Header(Headers.USER_AUTHORIZATION) String userToken);

    @GET("drivers/{driverID}/cabtypes")
    Call<ApiStatusDto> cabTypes(@Path("driverID") String driverId,
                                @Header(Headers.APP_AUTHORIZATION) String appToken,
                                @Header(Headers.AUTHORIZATION) String authToken,
                                @Header(Headers.USER_AUTHORIZATION) String userToken);
    @POST("drivers/{driverID}/changetype")
    Call<ApiStatusDto> cabTypeChange(@Body CabTypeRequest cabTypeRequest, @Path("driverID") String driverId,
                                     @Header(Headers.APP_AUTHORIZATION) String appToken,
                                     @Header(Headers.AUTHORIZATION) String authToken,
                                     @Header(Headers.USER_AUTHORIZATION) String userToken);

    @POST("drivers/{driverID}/offduty")
    Call<ApiStatusDto> offDuty(@Path("driverID") String driverId,
                               @Header(Headers.APP_AUTHORIZATION) String appToken,
                               @Header(Headers.AUTHORIZATION) String authToken,
                               @Header(Headers.USER_AUTHORIZATION) String userToken,
                               @Body UpdateDriverLocRequest updateDriverLocRequest);

    @POST("drivers/{driverID}/onduty")
    Call<ApiStatusDto> onDuty(@Path("driverID") String driverId,
                              @Header(Headers.APP_AUTHORIZATION) String appToken,
                              @Header(Headers.AUTHORIZATION) String authToken,
                              @Header(Headers.USER_AUTHORIZATION) String userToken,
                              @Body UpdateDriverLocRequest updateDriverLocRequest);

    @POST("drivers/{driverID}/gohome")
    Call<ApiStatusDto> goHome(@Path("driverID") String driverId,
                              @Header(Headers.APP_AUTHORIZATION) String appToken,
                              @Header(Headers.AUTHORIZATION) String authToken,
                              @Header(Headers.USER_AUTHORIZATION) String userToken,
                              @Body UpdateDriverLocRequest updateDriverLocRequest);

    @POST("drivers/{driverID}/bookings/{bookingID}/accept")
    Call<ApiStatusDto> acceptRide(@Path("driverID") String driverId, @Path("bookingID") String bookingId,
                                  @Header(Headers.APP_AUTHORIZATION) String appToken,
                                  @Header(Headers.AUTHORIZATION) String authToken,
                                  @Header(Headers.USER_AUTHORIZATION) String userToken,
                                  @Body UpdateDriverLocRequest updateDriverLocRequest);

    @GET("drivers/{driverID}/bookings/{bookingID}")
    Call<ApiStatusDto> bookingData(@Path("driverID") String driverId, @Path("bookingID") String bookingId,
                                   @Header(Headers.APP_AUTHORIZATION) String appToken,
                                   @Header(Headers.AUTHORIZATION) String authToken,
                                   @Header(Headers.USER_AUTHORIZATION) String userToken);

    @GET("drivers/{driverID}/cancelreasons")
    Call<ApiStatusDto> cancelReasons(@Path("driverID") String driverId,
                                @Header(Headers.APP_AUTHORIZATION) String appToken,
                                @Header(Headers.AUTHORIZATION) String authToken,
                                @Header(Headers.USER_AUTHORIZATION) String userToken);

    @POST("drivers/{driverID}/bookings/{bookingID}/cancel")
    Call<ApiStatusDto> cancelTrip(@Path("driverID") String driverId, @Path("bookingID") String bookingId,
                                  @Body CancelRequest cancelRequest,
                                  @Header(Headers.APP_AUTHORIZATION) String appToken,
                                  @Header(Headers.AUTHORIZATION) String authToken,
                                  @Header(Headers.USER_AUTHORIZATION) String userToken);

    @POST("drivers/{driverID}/bookings/{bookingID}/customerlocated")
    Call<ApiStatusDto> customerLocated(@Path("driverID") String driverId, @Path("bookingID") String bookingId,
                                       @Header(Headers.APP_AUTHORIZATION) String appToken,
                                       @Header(Headers.AUTHORIZATION) String authToken,
                                       @Header(Headers.USER_AUTHORIZATION) String userToken,
                                       @Body UpdateDriverLocRequest updateDriverLocRequest);

    @POST("drivers/{driverID}/bookings/{bookingID}/customerverified")
    Call<ApiStatusDto> customerVerified(@Path("driverID") String driverId, @Path("bookingID") String bookingId,
                                        @Header(Headers.APP_AUTHORIZATION) String appToken,
                                        @Header(Headers.AUTHORIZATION) String authToken,
                                        @Header(Headers.USER_AUTHORIZATION) String userToken,
                                        @Body UpdateDriverLocRequest updateDriverLocRequest);

    @POST("drivers/{driverID}/bookings/{bookingID}/start")
    Call<ApiStatusDto> startTrip(@Path("driverID") String driverId, @Path("bookingID") String bookingId,
                                 @Header(Headers.APP_AUTHORIZATION) String appToken,
                                 @Header(Headers.AUTHORIZATION) String authToken,
                                 @Header(Headers.USER_AUTHORIZATION) String userToken,
                                 @Body UpdateDriverLocRequest updateDriverLocRequest);

    @POST("drivers/{driverID}/bookings/{bookingID}/stop")
    Call<ApiStatusDto> stopTrip(@Path("driverID") String driverId, @Path("bookingID") String bookingId,
                                @Header(Headers.APP_AUTHORIZATION) String appToken,
                                @Header(Headers.AUTHORIZATION) String authToken,
                                @Header(Headers.USER_AUTHORIZATION) String userToken,
                                @Body UpdateDriverLocRequest updateDriverLocRequest);

    @POST("drivers/{driverID}/bookings/{bookingID}/completed")
    Call<ApiStatusDto> completeTrip(@Path("driverID") String driverId, @Path("bookingID") String bookingId,
                                    @Header(Headers.APP_AUTHORIZATION) String appToken,
                                    @Header(Headers.AUTHORIZATION) String authToken,
                                    @Header(Headers.USER_AUTHORIZATION) String userToken,
                                    @Body UpdateDriverLocRequest updateDriverLocRequest);

    @POST("drivers/{driverID}/bookings/{bookingID}/callcustomer")
    Call<ApiStatusDto> callCustomer(@Path("driverID") String driverId, @Path("bookingID") String bookingId,
                                    @Header(Headers.APP_AUTHORIZATION) String appToken,
                                    @Header(Headers.AUTHORIZATION) String authToken,
                                    @Header(Headers.USER_AUTHORIZATION) String userToken);

    @POST("drivers/{driverID}/verifymobile")
    Call<ApiStatusDto> verifyMobile(@Path("driverID") String driverId,
                              @Header(Headers.APP_AUTHORIZATION) String appToken,
                              @Header(Headers.AUTHORIZATION) String authToken,
                              @Header(Headers.USER_AUTHORIZATION) String userToken,
                                    @Body PaymentRequestDto paymentRequestDto);

    @POST("drivers/resetpassword")
    Call<ApiStatusDto> sendOtp(@Body SendOtpRequestDto sendOtpRequestDto);

    @POST("drivers/verifyotp")
    Call<ApiStatusDto> verifyOtp(@Body VerifyOtpRequestDto verifyOtpRequestDto);

    @POST("drivers/password")
    Call<ApiStatusDto> resetPwd(@Body ResetPwdRequestDto resetPwdRequestDto);

    @POST("drivers/{driverID}/signout")
    Call<ApiStatusDto> logOut(@Path("driverID") String driverId,
                              @Header(Headers.APP_AUTHORIZATION) String appToken,
                              @Header(Headers.AUTHORIZATION) String authToken,
                              @Header(Headers.USER_AUTHORIZATION) String userToken);


    @PUT
    Call<ApiStatusDto> updateProfile(UserDto userDto);

    Call<ApiStatusDto> sendSos();

    Call<ApiStatusDto> pastTrips(PastTripsRequestDto pastTripsRequestDto);

    Call<ApiStatusDto> tripDetails(String tripId);

    Call<ApiStatusDto> submitRating(RatingRequestDto ratingRequestDto);

    Call<ApiStatusDto> payment(PaymentRequestDto paymentRequestDto);

}