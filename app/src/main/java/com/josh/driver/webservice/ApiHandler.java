package com.josh.driver.webservice;



import com.josh.driver.handler.Url;
import com.josh.driver.handler.UrlId;

import java.io.File;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sridevi on 7/3/17.
 */

public class ApiHandler {

    private static EndPointService sEndPointService = null;
    private static CdnPointService sCdnPointService = null;
    private static GoogleDirectionPointService sGoogleDirectionPointService = null;
//    private static GoogleLocationPointService sGoogleLocationPointService = null;


    public static EndPointService getEndPointService() {
        if (sEndPointService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .client(getClient())
                    .baseUrl(Url.getFactory().getUrl(UrlId.BASE))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            sEndPointService = retrofit.create(EndPointService.class);
        }
        return sEndPointService;
    }

    public static CdnPointService getCdnPointService() {
        if (sCdnPointService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .client(getClient())
                    .baseUrl(Url.getFactory().getUrl(UrlId.CDN))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            sCdnPointService = retrofit.create(CdnPointService.class);
        }
        return sCdnPointService;
    }

    public static GoogleDirectionPointService getGoogleDirectionPointService() {
        if (sGoogleDirectionPointService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .client(getClient())
                    .baseUrl(Url.getFactory().getUrl(UrlId.GOOGLE_MAP_API))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            sGoogleDirectionPointService = retrofit.create(GoogleDirectionPointService.class);
        }
        return sGoogleDirectionPointService;
    }

//    public static GoogleLocationPointService getGoogleLocationPointService() {
//        if (sGoogleLocationPointService == null) {
//            Retrofit retrofit = new Retrofit.Builder()
//                    .client(getClient())
//                    .baseUrl(Url.getInstance().getGoogleLocationUrl())
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//            sGoogleLocationPointService = retrofit.create(GoogleLocationPointService.class);
//        }
//        return sGoogleLocationPointService;
//    }

    public static MultipartBody.Part getImageBody(String filePath) {
        File file = new File(filePath);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        return body;
    }

    private static OkHttpClient getClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        // Can be Level.BASIC, Level.HEADERS, or Level.BODY
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addNetworkInterceptor(interceptor);

        return builder.build();
    }
}
