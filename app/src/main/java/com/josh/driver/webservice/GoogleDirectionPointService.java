package com.josh.driver.webservice;


import com.josh.driver.dto.googlemapdto.DirectionsResponseDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleDirectionPointService {


    @GET("maps/api/directions/json")
    Call<DirectionsResponseDto> getDirection(@Query("origin") String origin, @Query("destination") String destination, @Query("key") String google_maps_key);
}