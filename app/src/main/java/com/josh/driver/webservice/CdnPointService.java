package com.josh.driver.webservice;



import com.josh.driver.dto.ApiStatusDto;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;

public interface CdnPointService {

    @Multipart
    @POST("file")
    Call<ApiStatusDto> uploadImage(/*@Header("authorization") String authorization,
                                          @Query("type") String fileType, @Part MultipartBody.Part file*/);
}