package com.josh.driver.webservice;


import com.hm.Log;
import com.hm.model.Model;
import com.hm.utils.CodeSnippet;
import com.josh.driver.Constants;
import com.josh.driver.handler.DataHandler;

import org.json.JSONException;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sridevi on 7/3/17.
 */

public class ModelHandler<T> implements Constants {

    private static final String TAG = "ModelHandler";

    private static ModelHandler sModelHandler = null;

    private HashMap<Integer, Call<T>> mCallList = new HashMap<>();
    private Random random = new Random();

    public static ModelHandler getInstance(){
        if(sModelHandler == null) {
            sModelHandler = new ModelHandler();
        }
        return sModelHandler;
    }

    public int enqueueTask(final ModelHanlderListener<T> modelHanlderListener, Call<T> call){
        final int callId = generateCallId();
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                Log.d(TAG, "Api Response :: " + CodeSnippet.getJsonStringFromObject(response.body()));
                DataHandler.getInstance().setServerTime(getDateFromHeaders(response));
                int responseCode = response.code();
                if(responseCode == Model.RESULT_OK){
                    modelHanlderListener.onSuccessApi(response.body());
                }else if(responseCode == ErrorCode.SESSION_EXPIRED){
                    modelHanlderListener.onAuthorizationFailed();
                }else {
                    modelHanlderListener.onFailureApi(responseCode, response.message());
                }
                dequeueTask(callId);
            }

            @Override
            public void onFailure(Call<T> call, Throwable exception) {
                int errorCode;
                String message;
                if (exception instanceof SocketTimeoutException) {
                    errorCode = ErrorCode.TIME_OUT;
                    message = AlertMessage.TIME_OUT;
                } else if (exception instanceof JSONException) {
                    errorCode = ErrorCode.JSON_PARSING;
                    message = AlertMessage.JSON_PARSING;
                }  else if (exception instanceof UnknownHostException) {
                    errorCode = ErrorCode.SERVER_CONNECTION_FAILED;
                    message = AlertMessage.SERVER_CONNECTION_FAILED;
                } else {
                    errorCode = ErrorCode.SERVICE_UNAVAILABLE;
                    message = AlertMessage.SERVER_CONNECTION_FAILED;
                }
                modelHanlderListener.onFailureApi(errorCode, message);
                dequeueTask(callId);
            }
        });
        saveApiCalls(callId, call);
        return callId;
    }

    public Response<T> enqueueNonAsyncTask(Call<T> call){
        try {
            return call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getDateFromHeaders(Response<T> response) {
        return response.headers().get("Date");
    }

    private void saveApiCalls(int callId, Call<T> call) {
        mCallList.put(callId, call);
    }

    private void removeApiCalls(int callId){
        if(mCallList.containsKey(callId)){
            mCallList.remove(callId);
        }
    }

    public void dequeueTask(int... callIds){
        if(callIds.length > 0){
            for (int id : callIds) {
                clearApiCall(id);
            }
        }

    }

    private void clearApiCall(int callId) {
        if(mCallList.containsKey(callId)){
            Call<T> call = mCallList.get(callId);
            call.cancel();
            removeApiCalls(callId);
        }
    }

    private int generateCallId() {
        int taskId = random.nextInt(100);
        if (mCallList.containsKey(taskId)) {
            return generateCallId();
        }
        return taskId;
    }




    public interface ModelHanlderListener<T>{
        void onSuccessApi(T response);

        void onFailureApi(int errorCode, String errorMessage);

        void onAuthorizationFailed();
    }


}
