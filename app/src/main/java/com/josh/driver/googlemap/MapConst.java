package com.josh.driver.googlemap;

/**
 * Created by hmspl on 27/5/15.
 */
public interface MapConst {
    /**
     * Api key for web based google api
     */
    String WEB_API_KEY = "AIzaSyBtvnkVg5R9BPCGDVV-31tDIB0ehkK72iM";// Web Api key not android api key

    /**
     * url required for autocomplete address
     */
    String PLACES_API_URL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?components=country:in&key=" + WEB_API_KEY + "&input=";//For auto complete address

    /**
     * url required for directions api
     */
    String DIRECTIONS_API_URL_BASE = "https://maps.googleapis.com/maps/api/directions/json?key=" + WEB_API_KEY + "&alternatives=true";
    String FROM = "&origin=";
    String TO = "&destination=";
    String OPTIMIZATION = "&waypoints=optimize:true";

}
