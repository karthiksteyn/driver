package com.josh.driver.googlemap;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by hmspl on 5/7/15.
 */
public class BoundsmanagerForStaticMap {

    private static final double LN2 = 0.6931471805599453;
    private static final int WORLD_PX_HEIGHT = 256;
    private static final int WORLD_PX_WIDTH = 256;
    private static final int ZOOM_MAX = 21;

    public static int getBoundsZoomLevel(LatLngBounds bounds, int mapWidthPx, int mapHeightPx) {
        BoundsmanagerForStaticMap boundsmanagerForStaticMap = new BoundsmanagerForStaticMap();

        LatLng ne = bounds.northeast;
        LatLng sw = bounds.southwest;

        double latFraction = (boundsmanagerForStaticMap.latRad(ne.latitude) - boundsmanagerForStaticMap.latRad(sw.latitude)) / Math.PI;

        double lngDiff = ne.longitude - sw.longitude;
        double lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        double latZoom = boundsmanagerForStaticMap.zoom(mapHeightPx, WORLD_PX_HEIGHT, latFraction);
        double lngZoom = boundsmanagerForStaticMap.zoom(mapWidthPx, WORLD_PX_WIDTH, lngFraction);

        int result = Math.min((int) latZoom, (int) lngZoom);
        return Math.min(result, ZOOM_MAX);
    }

    private double latRad(double lat) {
        double sin = Math.sin(lat * Math.PI / 180);
        double radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }

    private double zoom(int mapPx, int worldPx, double fraction) {
        return Math.floor(Math.log(mapPx / worldPx / fraction) / LN2);
    }


}
