package com.josh.driver.googlemap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

/**
 * Created by hmspl on 26/5/15.
 */
public class StaticMapClass {

    private int zoom = 12;//deault
    private List<LatLng> path;
    private boolean sensor = false; //default
    private String BaseUrl = "http://maps.googleapis.com/maps/api/staticmap?";
    private String size = "400x400"; //default
    private String color;
    private List<MarkersPositionForWeb> markers;
    private Context mContext;
    private OnGetStaticMapListener mOnGetStaticMapListener;

    private StaticMapClass(Context context, List<LatLng> path, List<MarkersPositionForWeb> markers) {
        this.mContext = context;
        this.path = path;
        this.markers = markers;
        setPathColor(Color.RED);
    }

    public static StaticMapClass getNewInstance(Context context, List<LatLng> path, List<MarkersPositionForWeb> markers) {
        return new StaticMapClass(context, path, markers);
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public void setPathColor(int color) {
        this.color = "0xff" + new StringBuilder(Integer.toHexString(color).substring(2).toLowerCase()).reverse();
    }

    public void setSize(int width, int height) {
        this.size = width + "x" + height;
    }

    public void setSensor(boolean sensor) {
        this.sensor = sensor;
    }

    public void getStaticMap(OnGetStaticMapListener onGetStaticMapListener) {
        this.mOnGetStaticMapListener = onGetStaticMapListener;
        new LoadImage().execute(buildUrl());
    }

    public String buildUrl() {
        String completeUrl = BaseUrl;
        completeUrl = completeUrl + "size=" + size + "&sensor=" + sensor + "&zoom=" + zoom;

        //Adding paths
        if (path != null) {
            completeUrl = completeUrl + "&path=color:" + color + "|weight:5|";
            for (int i = 0; i < path.size(); i++) {
                if (i != 0) {
                    completeUrl = completeUrl + "|";
                }
                LatLng latLng = path.get(i);
                completeUrl = completeUrl + latLng.latitude + "," + latLng.longitude;
            }
        }

        //Adding markers
        if (markers != null) {
            for (int i = 0; i < markers.size(); i++) {
                completeUrl = completeUrl + "&markers=";
                MarkersPositionForWeb markersPositionForWeb = markers.get(i);
                String markerUrl = markersPositionForWeb.getIconUrl();
                if (markerUrl != "") {
                    completeUrl = completeUrl + "icon:" + markerUrl;
                }
                if (i < markers.size() && markerUrl != "") {
                    completeUrl = completeUrl + "|";
                }
                LatLng latLng = markersPositionForWeb.getLatLng();
                completeUrl = completeUrl + latLng.latitude + "," + latLng.longitude;
            }
        }
        Log.v("Static Map Url", completeUrl);
        return completeUrl;
    }


    public interface OnGetStaticMapListener {
        void gotBitMap(Bitmap bitmap);

        void failedToLoadBitmap();
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {

        }

        protected Bitmap doInBackground(String... args) {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if (image != null) {
                mOnGetStaticMapListener.gotBitMap(image);
            } else {
                mOnGetStaticMapListener.failedToLoadBitmap();
            }
        }
    }
}
