package com.josh.driver.googlemap;

import android.graphics.Color;
import android.location.Location;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hm.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by hmspl on 18/5/15.
 */
public class PolyLinesCreator {

    public final int RANDOM = -123;
    private List<Integer> selectedpath = new ArrayList<>();
    private String TAG = "MAP Library";
    private int selectionColor;
    private int nonSelectionColor;
    private boolean canSelectPath = false;
    private List<Integer> colors = getColors();
    private List<PolyLineDto> polyLineDtoList;
    private GMapLibrary gMapLibrary;
    private GoogleMap googleMap;

    private List<LatLng> firstLast = new ArrayList<>();
    private int final_count = 0;
    private List<Polyline> polylineList = new ArrayList<>();
    private AddMarkerListener mAddMarkerListener;


    /**
     * Initializing and adding lines
     */
    public PolyLinesCreator(@NonNull PolyLineDto polyLineDto) {
        initializeArrays();
        setColorIfRandom(polyLineDto);
        polyLineDtoList.add(polyLineDto);
    }

    public PolyLinesCreator(@NonNull List<PolyLineDto> polyLineDtos) {
        initializeArrays();
        setColorIfRandom(polyLineDtos);
        this.polyLineDtoList = polyLineDtos;
    }

    public PolyLinesCreator() {
        initializeArrays();
    }

    public void setAddMarkerListener(AddMarkerListener addMarkerListener) {
        mAddMarkerListener = addMarkerListener;
    }

    //Lines will be added to list, but will not be drawn on map untill "commitDraw" is called
    public void addPolyLine(@NonNull PolyLineDto polyLineDto) {
        setColorIfRandom(polyLineDto);
        polyLineDtoList.add(polyLineDto);
    }

    //Lines will be added to list, but will not be drawn on map untill "commitDraw" is called
    public void addPolyLines(@NonNull List<PolyLineDto> polyLineDtos) {
        setColorIfRandom(polyLineDtos);
        polyLineDtoList.addAll(polyLineDtos);
    }

    public void addPolyLinesByLatLngs(List<LatLng> latLngList) {
        PolyLineDto polyLineDto = new PolyLineDto();
        polyLineDto.addPoints(latLngList);
        polyLineDtoList.add(polyLineDto);
    }

    public void addPolyLinesByLatLngInstantly(List<LatLng> latLngList) {
        PolyLineDto polyLineDto = new PolyLineDto();
        polyLineDto.addPoints(latLngList);
        polyLineDtoList.add(polyLineDto);
        drawLineOnMap(polyLineDto, false);
    }


    //Lines will be added immediately
    public void addPolyLinesInstantly(@NonNull PolyLineDto polyLineDto) {
        setColorIfRandom(polyLineDto);
        polyLineDtoList.add(polyLineDto);
        List<PolyLineDto> polyLineDtoList = new ArrayList<>();
        polyLineDtoList.add(polyLineDto);
        drawLinesOnMap(polyLineDtoList);
    }

    //Lines will be added immediately
    public void addPolyLinesInstantly(@NonNull List<PolyLineDto> polyLineDtos) {
        setColorIfRandom(polyLineDtos);
        polyLineDtoList.addAll(polyLineDtos);
        drawLinesOnMap(polyLineDtos);
        onSelectPath(0);
    }

    private void initializeArrays() {
        polylineList = new ArrayList<>();
        polyLineDtoList = new ArrayList<>();
        Collections.shuffle(colors);
    }


    /**
     * Color setter methods
     */
    private void setColorIfRandom(PolyLineDto polyLineDto) {
        if (final_count == colors.size()) {
            final_count = 0;
        }
        if (polyLineDto != null)
            if (polyLineDto.getColor() == RANDOM) {
                polyLineDto.setColor(colors.get(final_count));
            }
        final_count++;
    }


    private void setColorIfRandom(List<PolyLineDto> polyLineDtos) {
        for (int i = 0; i < polyLineDtos.size(); i++) {
            setColorIfRandom(polyLineDtos.get(i));
        }
    }

    /**
     * Map works (Drawing, Redrawing and adding lines)
     */
    public void bindMapLibrary(@NonNull GMapLibrary gMapLibrary) {
        this.gMapLibrary = gMapLibrary;
    }

    //If commit is called 2nd time, the all the lines will be re-drawn
    public void commitDraw() {
        if (isMapAvailable()) {
            removeAllLinesAndMarkers();
            for (int i = 0; i < polyLineDtoList.size(); i++) {
                drawLineOnMap(polyLineDtoList.get(i), false);
            }
        }
    }

    private synchronized void drawLinesOnMap(List<PolyLineDto> polyLineDtos) {
        if (isMapAvailable()) {
            for (int i = 0; i < polyLineDtos.size(); i++) {
                drawLineOnMap(polyLineDtos.get(i), false);
            }

        }
    }

    private void drawLineOnMap(PolyLineDto polyLineDto, boolean selected) {
        if (isMapAvailable()) {
            if (selected) {
                polyLineDto.setColor(selectionColor);
            } else {
                polyLineDto.setColor(nonSelectionColor);
            }
            polylineList.add(googleMap.addPolyline(getPolylineOptionsFromDto(polyLineDto)));
         /*   LatLngBounds.Builder latBuilder = new LatLngBounds.Builder();
            List<LatLng> latLngs = polyLineDto.getAllPoints();
            for (int i = 0; i < latLngs.size(); i++) {
                latBuilder.include(latLngs.get(i));
            }*/
/*
            LatLngBounds latLngBounds = latBuilder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(latLngBounds, 200);
            gMapLibrary.getMap().animateCamera(cu);
            gMapLibrary.getMap().moveCamera(cu);*/
        }
    }

    public void removePolyLines() {

    }

    private void markerAdder(LatLng latLng, MarkerOptions markerOptions, boolean startMarker) {
        boolean b = true;
        for (int i = 0; i < firstLast.size(); i++) {
            if (latLng.equals(firstLast.get(i))) {
                b = false;
            }
        }
        if (b) {
            Marker marker = gMapLibrary.addMarker(markerOptions.position(latLng));
            if (mAddMarkerListener != null) {
                if (startMarker) {
                    mAddMarkerListener.onStartMarkerAdded(marker);
                } else {
                    mAddMarkerListener.onEndMarkerAdded(marker);
                }
            }
        }
    }

    private boolean isMapAvailable() {
        return (googleMap != null);
    }

    /**
     * Remove all lines from map
     */
    public void removeAllLinesAndMarkers() {
        firstLast.clear();
        polylineList.clear();
//        gMapLibrary.removeAllMarkers();
    }

    public void removeAllLineCoordinates() {
        for (Polyline polyline : polylineList) {
            polyline.remove();
        }
        if (polyLineDtoList != null) {
            polyLineDtoList.clear();
        }
    }

    /**
     * By calling this method, the path selection will be enabled (Selection and non selections colors)
     */
    public void setPathColor(int sColor, int nsColor) {
        this.canSelectPath = true;
        selectionColor = sColor;
        nonSelectionColor = nsColor;
    }

    /**
     * By calling this method, the path selection will be disabled
     */
    public void disablePathSelection() {
        this.canSelectPath = false;
    }

    public boolean isPathSelectable() {
        return this.canSelectPath;
    }

    public synchronized int onPathClick(LatLng point) {
        float[] distanceBetweenPoints = new float[1];
        selectedpath.clear();
        if (canSelectPath) {
            for (int i = 0; i < polyLineDtoList.size(); i++) {
                for (LatLng polyCoords : getPolylineOptionsFromDto(polyLineDtoList.get(i)).getPoints()) {
                    float[] results = new float[1];
                    Location.distanceBetween(point.latitude, point.longitude,
                            polyCoords.latitude, polyCoords.longitude, results);
                    Log.i(TAG, results[0] + "");
                    if (results[0] < ((150 * 20) / gMapLibrary.getMap().getCameraPosition().zoom)) {
                        // If distance is less than 100 meters, this is your polyline
                        Log.i(TAG, results[0] + "," + (100 * 20) / gMapLibrary.getMap().getCameraPosition().zoom);
                        if (!isPathAvailable(i)) {
                            if (distanceBetweenPoints[0] == 0 || results[0] < distanceBetweenPoints[0]) {
                                distanceBetweenPoints[0] = results[0];
                                selectedpath.add(i);

                            }
                        }
                    }
                }
               /* if (PolyUtil.isLocationOnPath(point, getPolylineOptionsFromDto(polyLineDtoList.get(i)).getPoints(), false)) {
                    float[] results = new float[1];
                    LatLng latLng = PolyUtil.getLatLong();
                    Location.distanceBetween(point.latitude, point.longitude,
                            latLng.latitude, latLng.longitude, results);
                    if (distanceBetweenPoints[0] == 0 || results[0] < distanceBetweenPoints[0]) {
                        selectedpath.add(i);
                    }
                }*/
            }
            if (selectedpath.size() != 0) {
                int selection = Collections.max(selectedpath);
                Log.i("Selection", selection + "");
                highlightSelectedPath(selection);
                return selection;
            }
        }
        return -1;
    }

    public synchronized void onSelectPath(int pathPosition) {
        selectedpath.clear();
        if (canSelectPath) {
            int polyLineDtoListSize = polyLineDtoList.size();
            for (int i = 0; i < polyLineDtoListSize; i++) {
                if (pathPosition != i) {
                    selectedpath.add(i);
                }
                if (i == polyLineDtoListSize - 1) {
                    selectedpath.add(pathPosition);
                }
            }
            highlightSelectedPath(pathPosition);
        }
    }

    private synchronized void highlightSelectedPath(int selection) {
        polylineList.get(selection).remove();
        polylineList.remove(selection);
        PolyLineDto polyLineDto = polyLineDtoList.get(selection);
        polyLineDtoList.remove(selection);
        for (Polyline polyline : polylineList) {
            polyline.setZIndex(1);
            polyline.setColor(nonSelectionColor);
        }
        polyLineDto.setZindex(2);
        polyLineDtoList.add(polyLineDto);
        drawLineOnMap(polyLineDto, true);
    }


    private boolean isPathAvailable(int pathNum) {
        for (int i = 0; i < selectedpath.size(); i++) {
            if (selectedpath.get(i) == pathNum) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to return PolyLineOptions from PolyLineDto
     */
    private PolylineOptions getPolylineOptionsFromDto(PolyLineDto polyLineDto) {
        PolylineOptions ploptions = new PolylineOptions();
        ploptions.width(polyLineDto.getWidth());
        ploptions.visible(polyLineDto.isVisible());
        ploptions.color(polyLineDto.getColor());
        ploptions.addAll(polyLineDto.getAllPoints());
        ploptions.zIndex(polyLineDto.getZindex());
        return ploptions;
    }


    /**
     * Custom colors for random choice
     */
    private List<Integer> getColors() {
        int[] colors = {Color.parseColor("#fba6b0"), Color.parseColor("#760f30"), Color.parseColor("#7f1101"),
                Color.parseColor("#113b0c"), Color.parseColor("#61d0b3"), Color.parseColor("#81147b"),
                Color.parseColor("#6d6e98"), Color.parseColor("#04255e"), Color.parseColor("#ffdb01"),
                Color.parseColor("#9bf000"), Color.parseColor("#b08cf8"), Color.parseColor("#251e47"),
                Color.parseColor("#0099ff"), Color.BLACK, Color.RED};//Totally 15 colors
        List<Integer> c = new ArrayList<>();
        for (int i = 0; i < colors.length; i++) {
            c.add(colors[i]);
        }

        return c;
    }

    public int getSelectedPath() {
        return selectedpath.get(0);
    }

    public void bindMap(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    public interface AddMarkerListener {

        void onStartMarkerAdded(Marker marker);

        void onEndMarkerAdded(Marker marker);

    }
}
