package com.josh.driver.googlemap;

import android.os.Parcel;
import android.support.annotation.NonNull;

/**
 * Custom parceler class
 */
public class Parceler {

    private Parcel out;
    private int flags;

    public Parceler(@NonNull Parcel out, int flags) {
        this.out = out;
        this.flags = flags;
    }

    public Parcel getOut() {
        return out;
    }

    public void setOut(@NonNull Parcel out) {
        this.out = out;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }
}
