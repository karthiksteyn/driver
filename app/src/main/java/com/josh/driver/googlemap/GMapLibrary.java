package com.josh.driver.googlemap;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Sujith on 18/5/15.
 */
public class GMapLibrary implements OnMapReadyCallback {

    final public int HYBRID = GoogleMap.MAP_TYPE_HYBRID;
    final public int NONE = GoogleMap.MAP_TYPE_NONE;
    final public int NORMAL = GoogleMap.MAP_TYPE_NORMAL;
    final public int SATELLITE = GoogleMap.MAP_TYPE_SATELLITE;
    final public int TERRAIN = GoogleMap.MAP_TYPE_TERRAIN;
    private GoogleMap mMap;
    private Context context;
    private MapListener mapListener;
    private FragmentActivity activity;
    private int mapFragmentResourceId;
    private View mMapView;


    public GMapLibrary(Context context, FragmentActivity activity, int mapFragmentResourceId, MapListener mapListener) {
        this.context = context;
        this.activity = activity;
        this.mapFragmentResourceId = mapFragmentResourceId;
        this.mapListener = mapListener;
    }


    public void setUpMapAsync() {
        SupportMapFragment mapFragment = (SupportMapFragment) activity.getSupportFragmentManager()
                .findFragmentById(mapFragmentResourceId);
        mMapView = mapFragment.getView();
        mapFragment.getMapAsync(this);
    }


    public void setUpMapAsync(SupportMapFragment mapFragment) {
        mMapView = mapFragment.getView();
        mapFragment.getMapAsync(this);
    }


    public void mapDrawn() {
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mapListener.onMapDrawn();
            }
        });
    }

    public Marker addMarker(MarkerOptions markerOptions) {
        return mMap.addMarker(markerOptions);
    }


    public void setMapType(int mapType) {
        mMap.setMapType(mapType);
    }


    public Context getContext() {
        return this.context;
    }

    /**
     * Camera Operations
     */
    public void updateCamera(LatLng latLng, float zoom) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    public void updateCamera(CameraPosition cameraPosition) {
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void updateCamera(LatLng latLng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    public void updateCamera(LatLngBounds bounds, int padding) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
    }

    public void updateCamera(LatLngBounds bounds, int width, int height, int padding) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));
    }

    public void scrollBy(float xPixel, float yPixel) {
        mMap.moveCamera(CameraUpdateFactory.scrollBy(xPixel, yPixel));
    }

    public void zoomBy(float amount, @NonNull Point focus) {
        mMap.moveCamera(CameraUpdateFactory.zoomBy(amount, focus));
    }

    public void zoomBy(float amount) {
        mMap.moveCamera(CameraUpdateFactory.zoomBy(amount));
    }

    public void zoomTo(float zoom) {
        mMap.moveCamera(CameraUpdateFactory.zoomTo(zoom));
    }

    public void zoomIn() {
        mMap.moveCamera(CameraUpdateFactory.zoomIn());
    }

    public void zoomOut() {
        mMap.moveCamera(CameraUpdateFactory.zoomOut());
    }

    /**
     * Polylines
     */


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        mapListener.onMapReady();
        mapDrawn();
    }
    public void removeAllMarkers() {
        mMap.clear();
    }


    public GoogleMap getMap() {
        return this.mMap;
    }

    public interface MapListener {
        void onMapReady();

        void onMapDrawn();
    }

    /**
     * Custom anchor class
     */
    public class Anchor {
        private float u;
        private float v;

        public Anchor(@NonNull float u, @NonNull float v) {
            this.u = u;
            this.v = v;
        }

        public float getU() {
            return u;
        }

        public void setU(float u) {
            this.u = u;
        }

        public float getV() {
            return v;
        }

        public void setV(float v) {
            this.v = v;
        }
    }
}
