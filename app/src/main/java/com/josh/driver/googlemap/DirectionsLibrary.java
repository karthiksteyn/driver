package com.josh.driver.googlemap;

import android.net.Uri;
import android.widget.AutoCompleteTextView;

import com.google.android.gms.maps.model.LatLng;

import com.josh.driver.Constants;
import com.josh.driver.dto.googlemapdto.DirectionsResponseDto;
import com.josh.driver.dto.googlemapdto.Leg;
import com.josh.driver.dto.googlemapdto.Route;
import com.josh.driver.dto.googlemapdto.Step;
import com.josh.driver.webservice.ModelHandler;
import com.josh.driver.webservice.RequestCreator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hmspl on 26/5/15.
 */
public class DirectionsLibrary {

    List<AutoCompleteTextView> autoCompleteTextViews;
    private OnDirectionReceived directionsListener;

    public DirectionsLibrary() {

    }

    public DirectionsLibrary(OnDirectionReceived directionsListener) {
        autoCompleteTextViews = new ArrayList<>();
        this.directionsListener = directionsListener;
    }



    public void initSearchDirections(String fromLatLngString, String toLatLngString, String apiKey) {
        doSearchDirections(fromLatLngString, toLatLngString, Constants.AppKey.GOOGLE_API_KEY);
    }

    private void doSearchDirections(String fromLatLngString, String toLatLngString, String apiKey) {
        ModelHandler.getInstance().enqueueTask(new ModelHandler.ModelHanlderListener<DirectionsResponseDto>() {
            @Override
            public void onSuccessApi(DirectionsResponseDto response) {
                DirectionsJSONParser directionsJSONParser = new DirectionsJSONParser();
                List<PolyLineDto> directions = new ArrayList<>();
                List<Route> routeList = response.getRoutes();
                for (int i = 0; i < routeList.size(); i++) {
                    Route route = routeList.get(i);
                    addPolyLineFromRoute(route, directions, directionsJSONParser);
                }
                directionsListener.getDirectionSuccess(routeList, directions);
            }

            @Override
            public void onFailureApi(int errorCode, String errorMessage) {
                directionsListener.onGetDirectionFailed();
            }

            @Override
            public void onAuthorizationFailed() {

            }
        }, RequestCreator.getInstance().onRouteDirecion(fromLatLngString, toLatLngString, apiKey));
    }

    public void addPolyLineFromRoute(Route route, List<PolyLineDto> directions,
                                     DirectionsJSONParser directionsJSONParser) {
        if (directionsJSONParser == null) {
            directionsJSONParser = new DirectionsJSONParser();
        }
        List<Leg> legList = route.getLegs();

        // List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();
        /** Traversing all legs */
        for (int j = 0; j < legList.size(); j++) {
            /** Getting distance from the json data */
            Leg leg = legList.get(j);
            PolyLineDto polyLineDto = new PolyLineDto();
            List<Step> stepList = leg.getSteps();
            List<LatLng> latLngs = new ArrayList<>();
            /** Traversing all steps */
        String str = "";
            for (int k = 0; k < stepList.size(); k++) {
                String polyline = stepList.get(k).getPolyline().getPoints();
                str += polyline;
                List<LatLng> list = directionsJSONParser.decodePoly(polyline);
                latLngs.addAll(list);
            }
            polyLineDto.addPoints(latLngs);
            polyLineDto.setDistance(leg.getDistance().getValue());
            polyLineDto.setDuration(leg.getDuration().getValue());
            directions.add(polyLineDto);
        }
    }

    private String buildDirectionsUrl(String from, String to) {
        from = from.replaceAll("\\s+", "+");
        to = to.replaceAll("\\s+", "+");
        return MapConst.DIRECTIONS_API_URL_BASE + MapConst.FROM + Uri.encode(from) + MapConst.TO + Uri.encode(to)
                /*+ MapConst.OPTIMIZATION + "|" + from + "|" + to*/;
    }

    private String buildDirectionsUrl(LatLng fromLatLong, LatLng toLatLong) {
        return MapConst.DIRECTIONS_API_URL_BASE + MapConst.FROM + fromLatLong.latitude + "," + fromLatLong.longitude
                + MapConst.TO + toLatLong.latitude + "," + toLatLong.longitude
                +"&departure_time="+String.valueOf(System.currentTimeMillis()/1000 + (5 * 60))
                +"&traffic_model=pessimistic"
                /* +"&traffic_model=optimistic"
                +"&departure_time="+String.valueOf(System.currentTimeMillis()/1000)
                +"&mode=driving"
                +"&alternatives=true"
                +"&units=metric" */
                /*+ MapConst.OPTIMIZATION + "|" + from + "|" + to*/;
    }



    public List<String> getFullLocationList(String response) {
        JSONObject jsonObject = null;
        List<String> locationList = new ArrayList<>();
        try {
            jsonObject = new JSONObject(response);
            JSONArray predictions = jsonObject.getJSONArray("predictions");
            for (int i = 0; i < predictions.length(); i++) {
                JSONObject prediction = predictions.getJSONObject(i);
                locationList.add(prediction.getString("description"));
            }
        } catch (Exception e) {
            //Log.e("Json ex", e.toString());
            e.printStackTrace();
        }
        return locationList;
    }

    public interface OnDirectionReceived {

        void getDirectionSuccess(List<Route> routeList, List<PolyLineDto> polyLineDtos);

        void onGetDirectionFailed();
    }
}
