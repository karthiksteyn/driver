package com.josh.driver.googlemap;


import com.google.android.gms.maps.model.LatLng;
import com.josh.driver.dto.googlemapdto.Leg;
import com.josh.driver.dto.googlemapdto.Route;
import com.josh.driver.dto.googlemapdto.Step;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DirectionsJSONParser {


    public List<PolyLineDto> parse(JSONObject jObject) {
        List<PolyLineDto> routes = new ArrayList<PolyLineDto>();
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;
        JSONObject jDistance = null;
        JSONObject jDuration = null;
        try {
            jRoutes = jObject.getJSONArray("routes");

            /** Traversing all routes */
            for (int i = 0; i < jRoutes.length(); i++) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");

                // List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();
                /** Traversing all legs */
                for (int j = 0; j < jLegs.length(); j++) {
                    /** Getting distance from the json data */
                    PolyLineDto polyLineDto = new PolyLineDto();
                    jDistance = ((JSONObject) jLegs.get(j)).getJSONObject("distance");
                    String distance = jDistance.getString("value");
                    jDuration = ((JSONObject) jLegs.get(j)).getJSONObject("duration");
                    String duretion = jDuration.getString("value");
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");
                    List<LatLng> latLngs = new ArrayList<>();
                    /** Traversing all steps */
                    for (int k = 0; k < jSteps.length(); k++) {
                        String polyline = "";
                        polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);
                        latLngs.addAll(list);
                    }
                    polyLineDto.addPoints(latLngs);
                    polyLineDto.setDistance(Integer.parseInt(distance));
                    polyLineDto.setDuration(Integer.parseInt(duretion));
                    routes.add(polyLineDto);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return routes;
    }

    public PolyLineDto getPolyLineFromRoute(Route route) {
        List<Leg> legList = route.getLegs();
        PolyLineDto polyLineDto = new PolyLineDto();
        /** Traversing all legs */
        for (int j = 0; j < legList.size(); j++) {
            /** Getting distance from the json data */
            Leg leg = legList.get(j);
            List<Step> stepList = leg.getSteps();
            List<LatLng> latLngs = new ArrayList<>();
            /** Traversing all steps */
            for (int k = 0; k < stepList.size(); k++) {
                String polyline = stepList.get(k).getPolyline().getPoints();
                List<LatLng> list = decodePoly(polyline);
                latLngs.addAll(list);
            }
            polyLineDto.addPoints(latLngs);
            polyLineDto.setDistance(leg.getDistance().getValue());
            polyLineDto.setDuration(leg.getDuration().getValue());
        }
        return polyLineDto;
    }

    public List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        try {
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;
            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;
                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;
                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }
        } catch (StringIndexOutOfBoundsException e) {
            return null;
        }
        return poly;
    }
}