package com.josh.driver.googlemap;


import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;


public class PolyLineDto {
    private List<List<LatLng>> points = new ArrayList<>();
    private int distance;
    private int duration;
    private int color = 0xff000000; //default
    private float width = 10; //default
    private boolean visible = true; //default
    private float zindex;
    private boolean geodesic = false; //default
    private MarkerOptions startMarkerOptions;
    private MarkerOptions endMarkerOptions;
    private PolylineOptions polylineOptions;

    public MarkerOptions getStartMarkerOptions() {
        return startMarkerOptions;
    }

    public void setStartMarkerOptions(MarkerOptions startMarkerOptions) {
        this.startMarkerOptions = startMarkerOptions;
    }

    public MarkerOptions getEndMarkerOptions() {
        return endMarkerOptions;
    }

    public void setEndMarkerOptions(MarkerOptions endMarkerOptions) {
        this.endMarkerOptions = endMarkerOptions;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<List<LatLng>> getPoints() {
        return points;
    }

    public void setPoints(@NonNull List<List<LatLng>> points) {
        this.points = points;

    }

    public List<LatLng> getAllPoints() {
        List<LatLng> ll = new ArrayList<>();
        for (int i = 0; i < points.size(); i++) {
            ll.addAll(points.get(i));
        }
        return ll;
    }

    public void addPoints(List<LatLng> latLngList) {
        this.points.add(latLngList);
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setIsVisible(boolean isVisible) {
        this.visible = isVisible;
    }

    public float getZindex() {
        return zindex;
    }

    public void setZindex(float zindex) {
        this.zindex = zindex;
    }

    public boolean isGeodesic() {
        return geodesic;
    }

    public void setGeodesic(boolean geodesic) {
        this.geodesic = geodesic;
    }

    public PolylineOptions getPolylineOptions() {
        return polylineOptions;
    }

    public void setPolylineOptions(PolylineOptions polylineOptions) {
        this.polylineOptions = polylineOptions;
    }
}

