package com.josh.driver.googlemap;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by hmspl on 26/5/15.
 */
public class MarkersPositionForWeb {
    private LatLng latLng;
    private String iconUrl = "http://www.archipictura.com/widget-images/marker.png";// default

    public MarkersPositionForWeb(LatLng latLng) {
        this.latLng = latLng;
    }

    public MarkersPositionForWeb(LatLng latLng, String iconUrl) {
        this.latLng = latLng;
        this.iconUrl = iconUrl;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(@NonNull LatLng latLng) {
        this.latLng = latLng;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
