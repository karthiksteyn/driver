package com.josh.driver.socket;

/**
 * Created by Nandakumar on 8/6/15.
 */

public interface SocketKeys {

    interface SocketListenerKeys {
        String TRACKING = "tracking";

    }

    interface SocketEventKeys {
        String CONNECTED = "connected";
        String CONNECTION_FAILED = "cofabrnnection_failed";
        String DISCONNECTED = "disconnected";
        String ORDER_REQUEST = "ORDER";
        String FORCE_ORDER_REQUEST = "FORDER";
        String SIGNOUT = "SIGNOUT";
        String EMIT_UPDATE_GEO = "GEO";

    }
}
