package com.josh.driver.socket;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter.Listener;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.hm.utils.CodeSnippet;
import com.josh.driver.AppSnippet;
import com.josh.driver.Constants;
import com.josh.driver.dto.request.UpdateDriverLocRequest;
import com.josh.driver.dto.response.Order;
import com.josh.driver.dto.response.TripDetailsResponseDto;
import com.josh.driver.handler.DataHandler;
import com.josh.driver.handler.Url;
import com.josh.driver.handler.UrlId;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sugan on 7/3/17.
 */

public class SocketHandler implements SocketKeys.SocketEventKeys, SocketKeys.SocketListenerKeys {

    private static final String TAG = "SocketHandler";
    private static SocketHandler sSocketHandler;
    private static Map<String, Object> sSocketListenerMap;
    private Socket mSocket;

    private Context mContext;

    private SocketHandler(String driverId) {
        String url = Url.getFactory().getUrl(UrlId.SOCKET) + driverId;

        try {
            IO.Options options = new IO.Options();
            options.forceNew = true;
            mSocket = IO.socket(url, options);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        sSocketListenerMap = new HashMap<>();
        setupSocket();
        registerEvents();
        Log.i(TAG, "Socket Instance created");
    }

    public static SocketHandler getInstance(String driverId, String listenerKey, Object socketListener, boolean newInstance, Context context) {
        if (sSocketHandler == null) {
            sSocketHandler = new SocketHandler(driverId);
        } else if (newInstance) {
            sSocketHandler.disconnectSocket();
            sSocketHandler = new SocketHandler(driverId);
        }
        sSocketHandler.mContext = context;
        sSocketListenerMap.put(listenerKey, socketListener);
        return sSocketHandler;
    }

    public static SocketHandler getInstance() {
        return sSocketHandler;
    }

    public static SocketHandler getInstance(String listenerKey, Object socketListener) {
        if (sSocketListenerMap != null) {
            sSocketListenerMap.put(listenerKey, socketListener);
        }
        return sSocketHandler;
    }

    public void setupSocket() {

        mSocket.on(Socket.EVENT_CONNECT, new Listener() {
            @Override
            public void call(Object... args) {
                onEventFired(CONNECTED, null);
            }
        }).on(Socket.EVENT_ERROR, new Listener() {
            @Override
            public void call(Object... args) {
                onEventFired(CONNECTION_FAILED, null);
            }
        }).on(Socket.EVENT_DISCONNECT, new Listener() {
            @Override
            public void call(Object... args) {
                onEventFired(DISCONNECTED, null);
            }
        }).on(Socket.EVENT_CONNECT_ERROR, new Listener() {
            @Override
            public void call(Object... args) {
                onEventFired(CONNECTION_FAILED, null);
            }
        }).on(Socket.EVENT_CONNECT_TIMEOUT, new Listener() {
            @Override
            public void call(Object... args) {
                onEventFired(CONNECTION_FAILED, null);
            }
        });
        mSocket.connect();
        Log.i(TAG, "Socket Connect called");
    }

    public void registerEvents() {
        mSocket.on(ORDER_REQUEST, new Listener() {
            @Override
            public void call(Object... args) {
                onEventFired(ORDER_REQUEST, args[0].toString());
            }
        }).on(FORCE_ORDER_REQUEST, new Listener() {
            @Override
            public void call(Object... args) {
                onEventFired(FORCE_ORDER_REQUEST, args[0].toString());
            }
        }).on(SIGNOUT, new Listener() {
            @Override
            public void call(Object... args) {
                onEventFired(SIGNOUT, args[0].toString());
            }
        });
        Log.i(TAG, "Socket Events Registered");
    }

    private void onEventFired(final String socketEventKeys, final String response) {
        final Collection<Object> socketListenerList = sSocketListenerMap.values();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                switch (socketEventKeys) {
                    case CONNECTED:
                        AppSnippet.generateNoteOnSD(mContext, "JoshLogs.txt", "\nSocket Connected");
                        onSocketConnectedFired(socketListenerList);
                        break;
                    case CONNECTION_FAILED:
                        AppSnippet.generateNoteOnSD(mContext, "JoshLogs.txt", "\nSocket Connection Failed");
                        onSocketConnectionFailedFired(socketListenerList);
                        break;
                    case DISCONNECTED:
                        AppSnippet.generateNoteOnSD(mContext, "JoshLogs.txt", "\nSocket Disconnected");
                        onSocketDisconnectedFired(socketListenerList);
                        break;
                    case ORDER_REQUEST:
                        onOrderRequestFired(response);
                        break;
                    case FORCE_ORDER_REQUEST:
                        onForceOrderRequestFired(response);
                        break;
                    case SIGNOUT:
                        onSignOutFired(response);
                        break;

                }
            }
        });
    }


    public void onSocketConnectedFired(Collection<Object> socketListenerList) {
        for (Object socketListener : socketListenerList) {
            ((SocketListener) socketListener).onSocketConnected();
        }
    }

    public void onSocketConnectionFailedFired(Collection<Object> socketListenerList) {
        for (Object socketListener : socketListenerList) {
            ((SocketListener) socketListener).onConnectError();
        }
    }

    public void onSocketDisconnectedFired(Collection<Object> socketListenerList) {
        for (Object socketListener : socketListenerList) {
            ((SocketListener) socketListener).onSocketDisconnected();
        }
    }

    public void connectSocket() {
        mSocket.connect();
    }

    public boolean isConnected() {
        return mSocket.connected();
    }


    private void onOrderRequestFired(String response) {
        SocketListener socketListener = (SocketListener)
                sSocketListenerMap.get(TRACKING);
        Order order = getResponseObject(response, Order.class);
        if (socketListener != null) {
//            socketListener.onOrderRequest(order);

            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BundleKey.ORDER, order);
            Intent intent = new Intent(Constants.Receiver.RECEIVER_KEY);
            intent.putExtra(Constants.BundleKey.REQUEST, Constants.AppKey.WAITING_REQUEST);
            intent.putExtras(bundle);
            mContext.sendBroadcast(intent);

        }
    }

    private void onForceOrderRequestFired(String response) {
        SocketListener socketListener = (SocketListener)
                sSocketListenerMap.get(TRACKING);
        TripDetailsResponseDto tripDetailsResponseDto = getResponseObject(response, TripDetailsResponseDto.class);

        if (socketListener != null) {
            DataHandler.getInstance().saveTripDetails(tripDetailsResponseDto);

            Intent intent = new Intent(Constants.Receiver.RECEIVER_KEY);
            intent.putExtra(Constants.BundleKey.REQUEST, Constants.AppKey.FORCE_REQUEST);
            mContext.sendBroadcast(intent);

//            socketListener.onForceOrderRequest(tripDetailsResponseDto);
        }
    }

    private void onSignOutFired(String response) {
        SocketListener socketListener = (SocketListener)
                sSocketListenerMap.get(TRACKING);

        if (socketListener != null) {
            DataHandler.getInstance().onClearAllData();
            sSocketHandler.disconnectSocket();

            Intent intent = new Intent(Constants.Receiver.RECEIVER_KEY);
            intent.putExtra(Constants.BundleKey.REQUEST, Constants.AppKey.SIGNOUT_EXPIRY);
            mContext.sendBroadcast(intent);

//            socketListener.onSignOutRequest();

        }
    }




    public void disconnectSocket() {
        Log.i(TAG, "Socket Disconnect Called");
        AppSnippet.generateNoteOnSD(mContext, "JoshLogs.txt", "\nSocket Disconnect Called");
        sSocketListenerMap.clear();
        mSocket.disconnect();
        clearInstance();
    }

    private void clearInstance() {
        sSocketHandler = null;
    }


    private void emitData(String key, Object object, Ack ack) {
        Log.i(TAG, "Socket Request: Key:" + key + " Value:" + object.toString());
        if (ack == null) {
            mSocket.emit(key, object);
        } else {
            mSocket.emit(key, object, ack);
        }
    }



    public void postLocation(UpdateDriverLocRequest updateDriverLocRequest) {
        if (true) {
            try {
                emitData(EMIT_UPDATE_GEO, new JSONObject(CodeSnippet.getJsonStringFromObject(updateDriverLocRequest)), new Ack() {
                    @Override
                    public void call(Object... args) {
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private <T> T getResponseObject(String response, Class<T> type) {
        Log.i(TAG, "Socket Response:" + response);
        return CodeSnippet.getObjectFromJson(response, type);
    }
}
