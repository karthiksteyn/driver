package com.josh.driver.socket;


import com.josh.driver.dto.response.Order;
import com.josh.driver.dto.response.TripDetailsResponseDto;

/**
 * Created by on 5/1/15.
 */
public interface SocketListener {

    void onSocketConnected();

    void onConnectError();

    void onSocketConnectionFailed();

    void onSocketDisconnected();

    void onOrderRequest(Order order);

    void onForceOrderRequest(TripDetailsResponseDto tripDetailsResponseDto);

    void onSignOutRequest();
}
