package com.hm.activity;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hm.HmLibrary;
import com.hm.R;

/**
 * Created by Nandakumar on 7/12/15.
 */
public class NetworkCallActivity extends BaseActivity {

    public void retryNetworkCall(View view) {
        showNetworkCallProgressbar();
        hideRetry();
    }

    public void showNetworkCallLayout() {
        showNetworkCallProgressbar();
        hideContent();
        findViewById(R.id.network_call_layout).setVisibility(View.VISIBLE);
    }

    public void hideNetworkCallLayout() {
        dismissNetworkCallProgressbar();
        hideRetry();
        findViewById(R.id.network_call_layout).setVisibility(View.GONE);
    }

    public void showNetworkCallProgressbar() {
        findViewById(R.id.network_call_layout_failure_progress_bar).setVisibility(View.VISIBLE);
    }

    public void dismissNetworkCallProgressbar() {
        findViewById(R.id.network_call_layout_failure_progress_bar).setVisibility(View.GONE);
    }

    public void showRetry(String message) {
        hideContent();
        findViewById(R.id.network_call_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.network_call_layout_failure_retry_ll).setVisibility(View.VISIBLE);
        Button button = (Button) findViewById(R.id.retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryNetworkCall(view);
            }
        });
        ((TextView) findViewById(R.id.network_call_layout_failure_text_tv)).setText(message);
        dismissNetworkCallProgressbar();
    }

    public void showRetry(String message, int textColor){
        hideContent();
        findViewById(R.id.network_call_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.network_call_layout_failure_retry_ll).setVisibility(View.VISIBLE);
        Button button = (Button) findViewById(R.id.retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryNetworkCall(view);
            }
        });
        TextView retryTv = (TextView) findViewById(R.id.network_call_layout_failure_text_tv);
        retryTv.setText(message);
        retryTv.setTextColor(textColor);
        dismissNetworkCallProgressbar();
    }

    public void hideRetry() {
        findViewById(R.id.network_call_layout_failure_retry_ll).setVisibility(View.GONE);
    }

    public void showContent() {
        hideNetworkCallLayout();
        findViewById(R.id.content_layout).setVisibility(View.VISIBLE);
    }

    public void hideContent() {
        findViewById(R.id.content_layout).setVisibility(View.GONE);
    }

    public void onNetworkCallFailed(String message) {
        dismissNetworkCallProgressbar();
        showRetry(message);
    }


    @Override
    public boolean hasNetworkConnection(String showMessage) {
        boolean hasInternet = HmLibrary.getHmLibrary().hasNetworkConnection();
        if (!hasInternet && showMessage != null) {
            showRetry(showMessage);
        }
        return hasInternet;
    }

}
