package com.hm.activity;


/**
 * Created by Nandakumar on 7/12/15.
 */
public interface NetworkCallView extends View {

    void showNetworkCallLayout();

    void showNetworkCallProgressbar();

    void dismissNetworkCallProgressbar();

    void hideNetworkCallLayout();

    void showContent();

    void onNetworkCallFailed(String message);

    void showRetry(String message);

    void showRetry(String message, int textColor);

}
