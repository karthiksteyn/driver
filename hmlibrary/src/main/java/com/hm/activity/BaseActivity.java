package com.hm.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.hm.HmLibrary;
import com.hm.handler.PermissionHandler;
import com.hm.widgets.CustomProgressbar;
import com.hm.widgets.CustomToast;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Jeeva on 16/10/14.
 */
public abstract class BaseActivity extends AppCompatActivity implements View{

    private static final String TAG = "BaseActivity";

    private static boolean sAppAlive, sAppMinimized = true;

    protected boolean mDisableClick;

    public PermissionHandler mPermissionHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public FragmentActivity getActivity() {
        return this;
    }

    public Context getContext() {
        return this;
    }


    public void showMessage(String message) {
        CustomToast.getToast(BaseActivity.this).show(message);
    }

    public void showMessage(String message, int duration) {
        CustomToast.getToast(BaseActivity.this).show(message, duration);
    }

    public void showProgressbar(boolean cancellable) {
        if (!isFinishing()) {
            CustomProgressbar.getProgressbar(BaseActivity.this, cancellable).show();
        }
    }

    public void showProgressbar(boolean cancellable, String message) {
        if (!isFinishing()) {
            CustomProgressbar.getProgressbar(this, cancellable, message).show();
        }
    }

    public void showProgressbar() {
        if (!isFinishing()) {
            CustomProgressbar.getProgressbar(this, true).show();
        }
    }

    public void showProgressbar(String message) {
        if (!isFinishing()) {
            CustomProgressbar.getProgressbar(this, true, message).show();
        }
    }

    public void dismissProgressbar() {
        if (!isFinishing()) {
            CustomProgressbar.getProgressbar(this, false).dismiss();
        }
    }

    public boolean hasNetworkConnection(String showMessage) {
        boolean hasInternet = HmLibrary.getHmLibrary().hasNetworkConnection();
        if (!hasInternet && showMessage != null) {
            showMessage(showMessage);
        }
        return hasInternet;
    }

    public void updateProgressbar() {
        // TODO update the progressbar
    }

    public String getTrimmedText(TextView textView) {
        return textView.getText().toString().trim();
    }

    public void enableClick() {
        if (mDisableClick) {
            mDisableClick = false;
        }
    }

    public void showSoftKeyboard(android.view.View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, 0);
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    public void hideSoftKeyboard(android.view.View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        mPermissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sAppMinimized) {
            sAppMinimized = false;
        }
        sAppAlive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        sAppAlive = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!sAppAlive) {
            sAppMinimized = true;
        }
    }

    public String getResourceString(int id) {
        return getResources().getString(id);
    }

    public float getResourceDimen(int id) {
        return getResources().getDimension(id);
    }

    public int getResourceColor(int id) {
        return ContextCompat.getColor(this, id);
    }

    public void onAuthorizationFailed() {
        dismissProgressbar();
    }

    public PermissionHandler getPermissionHandler() {
        return mPermissionHandler;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List fragments = this.getSupportFragmentManager().getFragments();
        if (fragments != null) {
            Iterator var5 = fragments.iterator();

            while (var5.hasNext()) {
                Fragment fragment = (Fragment) var5.next();
                if (null != fragment) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }

    }


}
