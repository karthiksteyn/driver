package com.hm.activity;

import android.content.Context;
import android.support.v4.app.FragmentActivity;

/**
 * Created by Jeeva on 16/10/14.
 */
public interface View {

    FragmentActivity getActivity();

    Context getContext();

    void showMessage(String message);

    void showMessage(String message, int duration);

    void showProgressbar(boolean cancellable);

    void showProgressbar(boolean cancellable, String message);

    void showProgressbar();

    void showProgressbar(String message);

    void dismissProgressbar();

    void updateProgressbar();

    void enableClick();

    void showSoftKeyboard(android.view.View view);

    void hideSoftKeyboard(android.view.View view);

    void hideSoftKeyboard();

    boolean hasNetworkConnection(String message);

    void onAuthorizationFailed();
}