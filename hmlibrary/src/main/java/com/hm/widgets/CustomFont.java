package com.hm.widgets;

import android.content.Context;
import android.widget.TextView;

public final class CustomFont {

    public static void setFont(Context context, TextView textView, int fontValue) {
        String fontPath = null;
        switch (fontValue) {
            case 0:
                fontPath = "fonts/Roboto-Regular.ttf";
                break;
            case 1:
                fontPath = "fonts/Roboto-Light.ttf";
                break;
            case 2:
                fontPath = "fonts/Roboto-Medium.ttf";
                break;
            case 3:
                fontPath = "fonts/Roboto-Bold.ttf";
                break;
            case 4:
                fontPath = "fonts/Roboto-Thin.ttf";
                break;
            case 5:
                fontPath = "fonts/Helvetica-Light.ttf";
                break;
            case 6:
                fontPath = "fonts/Helvetica.ttf";
                break;
            case 7:
                fontPath = "fonts/Helvetica-Bold.ttf";
                break;
            case 8:
                fontPath = "fonts/HelveticaNeue.ttf";
                break;
            case 9:
                fontPath = "fonts/HelveticaNeue-Light.ttf";
                break;
            case 10:
                fontPath = "fonts/HelveticaNeue-Medium.ttf";
                break;
            case 11:
                fontPath = "fonts/HelveticaNeue-Bold.ttf";
                break;
            case 12:
                fontPath = "fonts/HelveticaNeue-Thin.ttf";
                break;
        }

        if (null != fontPath) {
            textView.setTypeface(Typefaces.get(context, fontPath));
        }
    }
}