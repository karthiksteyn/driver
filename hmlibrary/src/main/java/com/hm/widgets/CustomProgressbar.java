package com.hm.widgets;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.hm.R;

public class CustomProgressbar extends Dialog {

    private static CustomProgressbar customProgressbar;

    private String mMessage;

    private boolean mCancellable;

    public CustomProgressbar(Context context) {
        super(context);
        mMessage = null;
        createProgressBar(context);
    }

    public CustomProgressbar(Context context, String message) {
        super(context);
        mMessage = message;
        createProgressBar(context);
    }

    public static CustomProgressbar getProgressbar(Context context, boolean cancellable) {
        if (null == customProgressbar) {
            customProgressbar = new CustomProgressbar(context);
        }
        customProgressbar.mCancellable = cancellable;
        return customProgressbar;
    }

    public static CustomProgressbar getProgressbar(Context context, boolean cancellable, String message) {
        if (null == customProgressbar) {
            customProgressbar = new CustomProgressbar(context, message);
        }
        customProgressbar.mCancellable = cancellable;
        return customProgressbar;
    }

    public View getProgressbarInView(Context context) {
        ProgressBar progressBar = new ProgressBar(context, null,
                android.R.attr.progressBarStyleLarge);
        Resources resources = context.getResources();
        int dimension = resources.getDimensionPixelSize(R.dimen.dp_50);
        int transparentColor = resources.getColor(android.R.color.transparent);
        progressBar.setBackgroundColor(transparentColor);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setId(R.id.progress_circular);
        LayoutParams progressBarLayoutParams = new LayoutParams(dimension,
                dimension);
        progressBarLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        progressBar.setLayoutParams(progressBarLayoutParams);
        RelativeLayout layout = new RelativeLayout(context);
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layout.setLayoutParams(params);
        layout.setBackgroundColor(transparentColor);
        layout.setGravity(Gravity.CENTER);
        layout.addView(progressBar);
        layout.bringToFront();
        if (null != mMessage) {
            params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
            TextView messageTextView = new TextView(context);
            messageTextView.setText(mMessage);
            messageTextView.setTypeface(Typeface.DEFAULT_BOLD);
            messageTextView.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            params.addRule(RelativeLayout.BELOW, progressBar.getId());
            params.setMargins(0, resources.getDimensionPixelSize(R.dimen.dp_20), 0, 0);
            messageTextView.setLayoutParams(params);
            layout.addView(messageTextView);
        }
        return layout;
    }

    private void createProgressBar(Context context) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(getProgressbarInView(context));
        Window window = getWindow();
        window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(android.R.color.transparent);
        setCanceledOnTouchOutside(false);
    }

    private View getAppProgressbar(Context context) {
        ProgressBar progressBar = new ProgressBar(context, null,
                android.R.attr.progressBarStyleLarge);
        progressBar.setId(R.id.progress_circular);
        progressBar.setIndeterminate(true);
        if (null != mMessage) {
            RelativeLayout relativeLayout = new RelativeLayout(context);
            relativeLayout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT));
            LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            progressBar.setLayoutParams(layoutParams);
            relativeLayout.addView(progressBar);
            TextView messageTextView = new TextView(context);
            messageTextView.setText(mMessage);
            messageTextView.setTypeface(Typeface.DEFAULT_BOLD);
            messageTextView.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.BELOW, progressBar.getId());
            layoutParams.setMargins(0, 20, 0, 0);
            messageTextView.setPadding(0, 20, 0, 0);
            messageTextView.setLayoutParams(layoutParams);
            relativeLayout.addView(messageTextView);
            return relativeLayout;
        }

        return progressBar;
    }


    @Override
    public void show() {
        setCancelable(mCancellable);
        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        customProgressbar = null;
    }
}