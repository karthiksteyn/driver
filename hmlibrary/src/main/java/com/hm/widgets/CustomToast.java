package com.hm.widgets;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.hm.R;


public class CustomToast extends Toast {

    private static CustomToast customToast = null;

    private CustomToast(Context context) {
        super(context);
        setGravity(Gravity.BOTTOM, 0, 30);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflate.inflate(R.layout.inflater_custom_toast, null);
        setView(view);
    }

    public static CustomToast getToast(Context context) {
        if (null == customToast) {
            customToast = new CustomToast(context);
        }
        return customToast;
    }

    public void show(String message) {
        setText(message);
        show();
    }

    public void show(String message, int duration) {
        setDuration(duration);
        show(message);
    }
}