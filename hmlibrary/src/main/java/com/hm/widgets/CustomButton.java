package com.hm.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.widget.Button;

import com.hm.R;
import com.hm.utils.FontFamily;


public class CustomButton extends AppCompatButton {

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode())
            init(context, attrs);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode())
            init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomButton);
        setTypeface(FontFamily.getTypeface(context, a.getInt(R.styleable.CustomButton_typeface, 0)));
//        CustomFont.setFont(context, this, a.getInt(R.styleable.CustomButton_typeface, -1));
        a.recycle();
    }


}