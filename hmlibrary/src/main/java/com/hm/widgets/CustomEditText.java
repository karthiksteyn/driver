package com.hm.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;

import com.hm.R;
import com.hm.utils.FontFamily;


public class CustomEditText extends AppCompatEditText /*implements TextWatcher*/ {

    private OnFocusChangeListener mFocusChangeListener;
    private CustomEditTextKeyboardListener mCustomEditTextKeyboardListener;

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode())
            init(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode())
            init(context, attrs);
    }

    /**
     * For JOSH to hide a navigation bar when clicking back button while keyboard is up.
     */
    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
            if (mCustomEditTextKeyboardListener != null) {
                mCustomEditTextKeyboardListener.onKeyDownPressed();
                return true;
            }
        }
        return super.onKeyPreIme(keyCode, event);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (null != mFocusChangeListener) {
            mFocusChangeListener.onFocusChange(this, focused);
        }
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText);
        setTypeface(FontFamily.getTypeface(context, a.getInt(R.styleable.CustomEditText_typeface, 0)));
        setSelectAllOnFocus(true);
        a.recycle();
    }

    public void setCustomFocusListener(OnFocusChangeListener focusListener) {
        mFocusChangeListener = focusListener;
    }

    public void setCustomKeyBoardListener(CustomEditTextKeyboardListener listener) {
        mCustomEditTextKeyboardListener = listener;
    }

    public interface CustomEditTextKeyboardListener {

        void onKeyDownPressed();

    }

}