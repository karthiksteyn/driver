package com.hm.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.util.AttributeSet;

import com.hm.R;
import com.hm.utils.FontFamily;

/**
 * Created by Sridevi on 3/2/17.
 */

public class CustomCheckedTextView extends AppCompatCheckedTextView{

    public CustomCheckedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode())
            init(context, attrs);
    }

    public CustomCheckedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode())
            init(context, attrs);
    }

    public CustomCheckedTextView(Context context) {
        super(context);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomCheckedTextView);
        setTypeface(FontFamily.getTypeface(context, a.getInt(R.styleable.CustomCheckedTextView_typeface, 0)));
        a.recycle();
    }
}

