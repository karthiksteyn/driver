package com.hm.dbservices;

/**
 * Created by Jeeva on 25/9/14.
 */
public class PrimaryKey {

    private String name;
    private String foreignKey;
    private String alternateField;
    private String value;

    public PrimaryKey(String name) {
        this.name = name;
        this.foreignKey = name;
    }

    public PrimaryKey(String name, String alternateField) {
        this(name);
        this.alternateField = alternateField;
    }

    public PrimaryKey(String name, String alternateField, String foreignKey) {
        this.name = name;
        this.alternateField = alternateField;
        this.foreignKey = foreignKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getForeignKey() {
        return foreignKey;
    }

    public void setForeignKey(String foreignKey) {
        this.foreignKey = foreignKey;
    }

    public String getAlternateField() {
        return alternateField;
    }

    public void setAlternateField(String alternateField) {
        this.alternateField = alternateField;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
