package com.hm.dbservices;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.DatabaseUtils.InsertHelper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.fasterxml.jackson.databind.type.TypeFactory;
import com.hm.Log;
import com.hm.utils.CodeSnippet;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class DbHandler extends SQLiteOpenHelper {

    private static final String TAG = "DbHandler";
    private SQLiteDatabase db;
    private final AssetManager assetManager;
    private final String sqlFileName;
    private List<InsertHelper> insertHelpers = new ArrayList<InsertHelper>();
    private static DbHandler mInstance = null;

    private int count = 0;

    public static DbHandler getInstance(final Context ctx, String databaseName,
                                              int databaseVersion, String sqlFile) {
        if (mInstance == null) {
            mInstance = new DbHandler(ctx, databaseName, databaseVersion, sqlFile);
        }
        return mInstance;
    }

    DbHandler(Context context, String databaseName, int databaseVersion, String sqlFile) {
        super(context, databaseName, null, databaseVersion);
        this.assetManager = context.getAssets();
        this.sqlFileName = sqlFile;
        this.db = getWritableDatabase();
        db.execSQL("PRAGMA foreign_keys = ON");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            this.db = db;
            execSqlFile(sqlFileName);
        } catch (SQLException exception) {
            throw new RuntimeException("Database creation failed", exception);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);

    }

    protected void execSqlFile(String sqlFile) throws SQLException, IOException {
        List<String> sqlQuery = SqlParser.parseSqlFile(sqlFile, assetManager);
        for (String sqlInstruction : sqlQuery) {
            db.execSQL(sqlInstruction);
        }
    }

    public <T> void insertObject(T dtoObjectArray, String mainTable, HashMap<String, DbTableDetailDto> tableNames, PrimaryKey foreignData) throws Exception {
        insert(dtoObjectArray, new InsertHelper(db, mainTable), tableNames, foreignData);
    }

    public <T> void insertArray(T[] dtoObjectArray, String mainTable, HashMap<String, DbTableDetailDto> tableNames, PrimaryKey foreignData) throws Exception {
        for (T dtoObject : dtoObjectArray) {
            insert(dtoObject, new InsertHelper(db, mainTable), tableNames, foreignData);
        }
    }

    public <T> void insertList(List<T> dtoObjectArray, String mainTable, HashMap<String, DbTableDetailDto> tableNames, PrimaryKey foreignData) throws Exception {
        for (T dtoObject : dtoObjectArray) {
            insert(dtoObject, new InsertHelper(db, mainTable), tableNames, foreignData);
        }
    }

    private <T> void insert(T dto, InsertHelper helper, HashMap<String, DbTableDetailDto> tableNames,
                            PrimaryKey foreignData) throws Exception {

        helper.prepareForReplace();

        T value;
        String fieldName;
        Class<?> fieldType;
        int index;
        boolean isTable;
        DbTableDetailDto dbTableDetailDto;


        if (null != foreignData) {
            helper.bind(helper.getColumnIndex(foreignData.getForeignKey()), foreignData.getValue());
        }

        if(dto.getClass() == String.class) {
            helper.bind(1, dto.toString());
        } else {
            Map<String, Field> inheritedFields = getInheritedPrivateFields(dto.getClass());
            Collection<Field> fields = inheritedFields.values();
            for (Field field : fields) {

                field.setAccessible(true);

                fieldName = field.getName();
                fieldType = field.getType();

                value = (T) field.get(dto);

                Log.i(fieldName, value + "");

                isTable = tableNames.containsKey(fieldName);
                try {
                    index = helper.getColumnIndex(fieldName);
                } catch (IllegalArgumentException ex) {
                    index = -1;
                    if (isTable) {
                        index = 100;
                    }
                }
                if (null != value && index != -1) {
                    if (isTable) {
                        dbTableDetailDto = tableNames.get(fieldName);

                        // Fill up primarykey value
                        PrimaryKey primaryKey = dbTableDetailDto.getPrimaryKey();
                        if(null == primaryKey.getAlternateField()) {
                            Field field1 = inheritedFields.get(primaryKey.getName());
                            field1.setAccessible(true);
                            primaryKey.setValue(field1.get(dto).toString());
                        } else {
//                        primaryKey = tableNames.get(primaryKey.getAlternateField()).getPrimaryKey();
                            primaryKey.setValue(tableNames.get(primaryKey.getAlternateField()).getPrimaryKey().getValue());
                        }

                        if (fieldType.isArray()) {
                            Log.d(TAG, fieldName + " is Array");
                            insertArray((T[]) value, dbTableDetailDto.getTableName(), tableNames, primaryKey);
                        } else if (fieldType == List.class) {
                            Log.d(TAG, fieldName + " is List");
                            insertList((List<T>) value, dbTableDetailDto.getTableName(), tableNames, primaryKey);
                        } else {
                            Log.d(TAG, fieldName + " is Object");
                            insertObject(value, dbTableDetailDto.getTableName(), tableNames, primaryKey);
                        }
                    } else {

                        if (fieldType == byte[].class) {
                            helper.bind(index, (byte[]) value);
                        } else if (fieldType == Integer.class || fieldType == int.class) {
                            helper.bind(index, value.toString());
                        } else if (fieldType == Double.class || fieldType == double.class) {
                            helper.bind(index, value.toString());
                        } else if (fieldType == Float.class || fieldType == float.class) {
                            helper.bind(index, value.toString());
                        } else if (fieldType == Boolean.class || fieldType == boolean.class) {
                            helper.bind(index, value.toString());
                        } else if (fieldType == Long.class || fieldType == long.class) {
                            helper.bind(index, value.toString());
                        } else if(fieldType == String.class) {
                            helper.bind(index, value.toString());
                        } else {
                            helper.bind(index, CodeSnippet.getJsonStringFromObject(value));
                        }
                    }
                }
            }
        }



        if (null == foreignData) {
            helper.execute();
            int size = insertHelpers.size();
            InsertHelper insertHelper = null;
            for (int i = size - 1; i >= 0; i--) {
                insertHelper = insertHelpers.get(i);
                insertHelper.execute();
                insertHelper.close();
            }
            insertHelpers.clear();
        } else {
            insertHelpers.add(helper);
        }
    }

    public <T> T select(String tableName, String selection, String[] params,
                        Class<T> dtoClass, HashMap<String, DbTableDetailDto> tableDetails)
            throws Exception {
        T t = null;
        Cursor cursor = null;
        try {
            cursor = db.query(tableName, null, selection, params, null, null, null);
            if (null != cursor) {
                int rowCount = cursor.getCount();
                if (rowCount > 0) {
                    cursor.moveToFirst();
                    if (dtoClass == String.class) {
                        t = (T) cursor.getString(0);
                    } else if (dtoClass == byte[].class) {
                        t = (T) cursor.getBlob(0);
                    } else {
                        t = fillDataFromCursor(dtoClass, cursor, tableDetails);
                    }
                }
            }
            return t;
        } finally {
            closeCursor(cursor);
        }
    }

    public <T> T[] selectArrayByDto(String tableName, String selection, String[] params,
                                    Class<T> dtoClass, HashMap<String, DbTableDetailDto> tableDetails) throws Exception {
        T[] objectList = null;
        Cursor cursor = null;
        try {
            cursor = db.query(tableName, null, selection, params, null, null, null, null);
            if (null != cursor) {
                int rowCount = cursor.getCount();
                int count = 0;
                if (rowCount > 0) {
                    objectList = (T[]) Array.newInstance(dtoClass, rowCount);
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        objectList[count++] = fillDataFromCursor(dtoClass, cursor, tableDetails);
                        cursor.moveToNext();
                    }
                }
            }
            return objectList;
        } finally {
            closeCursor(cursor);
        }
    }

    public <T> List<T> selectListByDto(String tableName, String selection, String[] params, String orderBy,
                                       Class<T> dtoClass, HashMap<String, DbTableDetailDto> tableDetails)
            throws Exception {
        List<T> objectList = null;
        Cursor cursor = null;
        try {
            cursor = db.query(tableName, null, selection, params, null, null, orderBy, null);
            if (null != cursor) {
                int rowCount = cursor.getCount();
                if (rowCount > 0) {
                    objectList = new ArrayList<T>();
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        objectList.add(fillDataFromCursor(dtoClass, cursor, tableDetails));
                        cursor.moveToNext();
                    }
                }
            }
            return objectList;
        } finally {
            closeCursor(cursor);
        }
    }

    public <T> List<T> selectListByDto(String tableName, String selection, String[] params,
                                       Class<T> dtoClass, HashMap<String, DbTableDetailDto> tableDetails)
            throws Exception {
        return selectListByDto(tableName, selection, params, null, dtoClass, tableDetails);
    }

    private <T> T fillDataFromCursor(Class<T> dtoClass, Cursor cursor, HashMap<String, DbTableDetailDto> tableDetails) throws Exception {
        Object value;
        int columnIndex;
        String fieldName;
        Class<?> fieldType;
        DbTableDetailDto tableDetailDto = null;
        String primaryName;
        String foreignName;
        boolean table = false;

        T dtoObject = dtoClass.newInstance();

        if(dtoClass == String.class) {
            return (T) cursor.getString(0);
        } else {
            Map<String, Field> inheritedFields = getInheritedPrivateFields(dtoClass);
            Collection<Field> fields = inheritedFields.values();
            for (Field field : fields) {
                field.setAccessible(true); // access the field
                fieldName = field.getName();
                fieldType = field.getType();

                columnIndex = cursor.getColumnIndex(fieldName);
                table =false;

                if(tableDetails.containsKey(fieldName)) {
                    tableDetailDto = tableDetails.get(fieldName);
                    table = tableDetailDto.isTable();
                }

                if (table) {
                    primaryName = tableDetailDto.getPrimaryKey().getName();
                    foreignName = tableDetailDto.getPrimaryKey().getForeignKey();

                    if (fieldType.isArray()) {
                        value = selectArrayByDto(tableDetailDto.getTableName(),
                                foreignName + " = ?", new String[]{cursor.getString(cursor.getColumnIndex(primaryName))},
                                tableDetailDto.getClassName(), tableDetails);
                    } else if (fieldType == List.class) {
                        value = selectListByDto(tableDetailDto.getTableName(),
                                foreignName + " = ?", new String[]{cursor.getString(cursor.getColumnIndex(primaryName))},
                                tableDetailDto.getClassName(), tableDetails);
                    } else {
                        value = select(tableDetailDto.getTableName(),
                                foreignName + " = ?", new String[]{cursor.getString(cursor.getColumnIndex(primaryName))},
                                tableDetailDto.getClassName(), tableDetails);
                    }

                    field.set(dtoObject, value);
                } else if (columnIndex != -1) {
                    if (cursor.getType(columnIndex) == Cursor.FIELD_TYPE_BLOB) {
                        value = cursor.getBlob(columnIndex);
                    } else {
                        value = cursor.getString(columnIndex);
                    }
                    if (null != value) {
                        if(null == tableDetailDto) {
                            field.set(dtoObject, getValueByType(fieldType, value, null));
                        } else {
                            field.set(dtoObject, getValueByType(fieldType, value, tableDetailDto.getClassName()));
                        }
                    }
                }
            }
        }
        return dtoObject;
    }

    private Object getValueByType(Class<?> type, Object value, Class<?> extendType) {
        String data = value.toString();

        if (type == Integer.class || type == int.class) {
            return Integer.parseInt(data);
        } else if (type == Double.class || type == double.class) {
            return Double.parseDouble(data);
        } else if (type == Float.class || type == float.class) {
            return Float.parseFloat(data);
        } else if (type == Boolean.class || type == boolean.class) {
            return Boolean.valueOf(data);
        } else if (type == Long.class || type == long.class) {
            return Long.valueOf(data);
        } else if(type == String.class) {
            return data;
        } else {
            if(type == List.class) {
                return CodeSnippet.getObjectFromJson(data, TypeFactory.defaultInstance().constructCollectionType(List.class, extendType));
            }
            return CodeSnippet.getObjectFromJson(data, type);
        }
    }

    public void deleteQuery(String table, String whereClause, String[] whereArgs)
            throws Exception {
        db.delete(table, whereClause, whereArgs);
    }

    public int update(String tableName, ContentValues args, String wherClause,
                      String[] conditionArgs) {
        return this.db.update(tableName, args, wherClause, conditionArgs);
    }

    public int insert(String tableName, ContentValues args) {
        return (int) this.db.insert(tableName, null, args);
    }

    public void executeQuery(String query, String[] params) {
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(query, params);
            if (null != cursor && cursor.getCount() > 0) {
                Log.i("Update Count", cursor.getCount() + "");
            }
        } finally {
            closeCursor(cursor);
        }
    }

    public int getNumberOfRows(String tableName, String[] columns, String selection, String[] params)
            throws Exception {
        Cursor cursor = null;
        try {
            cursor = db.query(tableName, columns, selection, params, null, null, null);
            if (null != cursor) {
                cursor.moveToFirst();
                return cursor.getCount();
            }
        } finally {
            closeCursor(cursor);
        }
        return 0;
    }


    public void insertContentValues(String mainTable, ContentValues contentValues) throws Exception {
        db.insert(mainTable, null, contentValues);
    }

    private void closeCursor(Cursor cursor) {
        if (null != cursor) {
            cursor.close();
            cursor = null;
        }
    }

    private Map<String, Field> getInheritedPrivateFields(Class<?> type) {
        Map<String, Field> result = new HashMap<>();

        Class<?> inheritedClass = type;
        Field[] fields;
        while (null != inheritedClass && inheritedClass != Object.class) {
            fields = inheritedClass.getDeclaredFields();
            for (Field field : fields) {
                result.put(field.getName(), field);
            }
            inheritedClass = inheritedClass.getSuperclass();
        }

        return result;
    }
}
