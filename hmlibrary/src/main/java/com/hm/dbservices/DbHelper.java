package com.hm.dbservices;

import android.content.ContentValues;
import android.content.Context;

import java.util.HashMap;
import java.util.List;

public class DbHelper {

    private static DbHelper dbHelper = new DbHelper();
    private static DbHandler dbHandler = null;

    private DbHelper() {
    }

    public static DbHelper getDatabaseHelper() {
        return dbHelper;
    }

    public void createDatabase(final Context context, String databaseName, int databaseVersion, String sqlFile) {
        dbHandler = DbHandler.getInstance(context, databaseName, databaseVersion, sqlFile);
    }

    public <T> void insertFromArray(T[] dtoObjectArray, String mainTable, HashMap<String,DbTableDetailDto> tableNames) throws Exception {
        dbHandler.insertArray(dtoObjectArray, mainTable, tableNames, null);
    }

    public <T> void insertFromList(List<T> dtoObjectArray, String mainTable, HashMap<String,DbTableDetailDto> tableNames) throws Exception {
        dbHandler.insertList(dtoObjectArray, mainTable, tableNames, null);
    }

    public <T> void insertFromObject(T dtoObjectArray, String mainTable, HashMap<String,DbTableDetailDto> tableNames) throws Exception {
        dbHandler.insertObject(dtoObjectArray, mainTable, tableNames, null);
    }

    public <T> void insertContentValues(String mainTable, ContentValues contentValues) throws Exception {
        dbHandler.insertContentValues(mainTable, contentValues);
    }

    public <T> List<T> selectListByDto(String tableName, String selection, String[] params, String orderBy,
                                       Class<T> dtoClass, HashMap<String, DbTableDetailDto> tableDetails) throws Exception {
        return dbHandler.selectListByDto(tableName, selection, params, orderBy, dtoClass, tableDetails);
    }

    public <T> List<T> selectListByDto(String tableName, String selection, String[] params, Class<T> dtoClass,
                                       HashMap<String, DbTableDetailDto> tableDetails) throws Exception {
        return selectListByDto(tableName, selection, params, null, dtoClass, tableDetails);
    }

    public <T> T[] selectArrayByDto(String tableName, String selection, String[] params, Class<T> dtoClass,
                                    HashMap<String, DbTableDetailDto> tableDetails) throws Exception {
        return dbHandler.selectArrayByDto(tableName, selection, params, dtoClass, tableDetails);
    }

    public <T> T select(String tableName, String selection, String[] params, Class<T> dtoClass,
                        HashMap<String, DbTableDetailDto> tableDetails) throws Exception {
        return dbHandler.select(tableName, selection, params, dtoClass, tableDetails);
    }

    public void deleteQuery(String table, String whereClause, String[] params) throws Exception {
        dbHandler.deleteQuery(table, whereClause, params);
    }

    public void updateQuery(String query, String[] params) {
        dbHandler.executeQuery(query, params);
    }

    public int updateQuery(String tableName, ContentValues args, String where, String[] condition) {
        return dbHandler.update(tableName, args, where, condition);
    }

    public int getNumberOfRows(String tableName, String[] columns, String selection, String[] params) throws Exception {
        return dbHandler.getNumberOfRows(tableName, columns, selection, params);
    }
}