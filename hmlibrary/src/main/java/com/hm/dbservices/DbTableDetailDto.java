package com.hm.dbservices;

/**
 * Created by Jeeva on 24/9/14.
 */
public class DbTableDetailDto {

    private String tableName;
    private PrimaryKey primaryKey;
    private Class<?> className;
    private boolean table = true;

    public DbTableDetailDto(String tableName, PrimaryKey primaryKey, Class<?> className) {
        this.tableName = tableName;
        this.primaryKey = primaryKey;
        this.className = className;
    }

    public DbTableDetailDto(String tableName, PrimaryKey primaryKey, Class<?> className, boolean table) {
        this.tableName = tableName;
        this.primaryKey = primaryKey;
        this.className = className;
        this.table = table;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public PrimaryKey getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(PrimaryKey primaryKey) {
        this.primaryKey = primaryKey;
    }

    public Class<?> getClassName() {
        return className;
    }

    public void setClassName(Class<?> className) {
        this.className = className;
    }

    public boolean isTable() {
        return table;
    }

    public void setTable(boolean table) {
        this.table = table;
    }
}
