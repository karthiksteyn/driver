package com.hm.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.hm.activity.BaseActivity;

import java.lang.reflect.Field;


/**
 * Created by Jeeva on 16/10/14.
 */
public abstract class BaseFragment extends Fragment {

    private final String TAG = "BaseFragment";

    public boolean mDisableClick;

    public void showProgressbar(boolean cancellable) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showProgressbar(cancellable);
        }
    }

    public void showProgressbar(boolean cancellable, String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showProgressbar(cancellable, message);
        }
    }

    public void showProgressbar() {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showProgressbar();
        }
    }

    public void showProgressbar(String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showProgressbar(message);
        }
    }

    public void showMessage(String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(message);
        }
    }

    public boolean hasNetworkConnection(String showMessage) {
        BaseActivity baseActivity = getBaseActivity();
        return baseActivity != null && baseActivity.hasNetworkConnection(showMessage);
    }

    public void showMessage(String message, int duration) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(message, duration);
        }
    }

    public boolean isFinishing() {
        Activity activity = getActivity();
        return activity == null || activity.isFinishing() || getView() == null;
    }

    public void dismissProgressbar() {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.dismissProgressbar();
        }
    }

    public String getTrimmedText(TextView textView) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
           return baseActivity.getTrimmedText(textView);
        }
        return null;
    }

    public BaseActivity getBaseActivity() {
        Activity activity = getActivity();
        return activity != null && !activity.isFinishing() ? (BaseActivity) activity : null;
    }


    public void enableClick() {
        if (mDisableClick) {
            mDisableClick = false;
        }
    }

    public void showSoftKeyboard(android.view.View v) {
        getBaseActivity().showSoftKeyboard(v);
    }

    public void hideSoftKeyboard() {
        getBaseActivity().hideSoftKeyboard();
    }

    public void hideSoftKeyboard(android.view.View v) {
        getBaseActivity().hideSoftKeyboard(v);
    }

    public android.view.View findViewById(int id) {
        View view = getView();
        return view != null ? view.findViewById(id) : null;
    }

    public void updateProgressbar() {

    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }


    public String getResourceString(int id) {
        return getResources().getString(id);
    }

    public float getResourceDimen(int id) {
        return getResources().getDimension(id);
    }

    public int getResourceColor(int id) {
        return ContextCompat.getColor(getContext(), id);
    }

    public void onAuthorizationFailed() {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.onAuthorizationFailed();
        }
    }
}