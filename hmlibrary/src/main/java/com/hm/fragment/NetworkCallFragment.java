package com.hm.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hm.R;
import com.hm.activity.BaseActivity;


/**
 * Created by kumaresan on 18-Dec-15.
 */
public class NetworkCallFragment extends BaseFragment {

    public void retryNetworkCall(View view) {
        if (getView() != null) {
            showNetworkCallProgressbar();
            hideRetry();
        }
    }

    public void showNetworkCallLayout() {
        if (getView() != null) {
            showNetworkCallProgressbar();
            hideContent();
            findViewById(R.id.network_call_layout).setVisibility(View.VISIBLE);
        }
    }

    public void hideNetworkCallLayout() {
        if (getView() != null) {
            dismissNetworkCallProgressbar();
            hideRetry();
            findViewById(R.id.network_call_layout).setVisibility(View.GONE);
        }
    }

    public void showNetworkCallProgressbar() {
        if (getView() != null) {
            findViewById(R.id.network_call_layout_failure_progress_bar).setVisibility(View.VISIBLE);
        }
    }

    public void dismissNetworkCallProgressbar() {
        if (getView() != null) {
            findViewById(R.id.network_call_layout_failure_progress_bar).setVisibility(View.GONE);
        }
    }

    public void showRetry(String message) {
        if (getView() != null) {
            hideContent();
            findViewById(R.id.network_call_layout).setVisibility(View.VISIBLE);
            findViewById(R.id.network_call_layout_failure_retry_ll).setVisibility(View.VISIBLE);
            Button button = (Button) findViewById(R.id.retry);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retryNetworkCall(view);
                }
            });
            ((TextView) findViewById(R.id.network_call_layout_failure_text_tv)).setText(message);
            dismissNetworkCallProgressbar();
        }
    }

    public void showRetry(String message, int textColor){
        if (getView() != null) {
            hideContent();
            findViewById(R.id.network_call_layout).setVisibility(View.VISIBLE);
            findViewById(R.id.network_call_layout_failure_retry_ll).setVisibility(View.VISIBLE);
            Button button = (Button) findViewById(R.id.retry);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retryNetworkCall(view);
                }
            });
            TextView retryTv = (TextView) findViewById(R.id.network_call_layout_failure_text_tv);
            retryTv.setText(message);
            retryTv.setTextColor(textColor);
            dismissNetworkCallProgressbar();
        }
    }

    public void hideRetry() {
        if (getView() != null) {
            findViewById(R.id.network_call_layout_failure_retry_ll).setVisibility(View.GONE);
        }
    }

    public void showContent() {
        if (getView() != null) {
            hideNetworkCallLayout();
            findViewById(R.id.content_layout).setVisibility(View.VISIBLE);
        }
    }


    public void hideContent() {
        if (getView() != null) {
            findViewById(R.id.content_layout).setVisibility(View.GONE);
        }
    }

    public void onNetworkCallFailed(String message) {
        if (getView() != null) {
            showRetry(message);
        }
    }

    public boolean hasNetworkConnection(String message) {
        BaseActivity baseActivity = getBaseActivity();
        return baseActivity != null && baseActivity.hasNetworkConnection(message);
    }
}
