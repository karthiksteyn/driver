package com.hm.fragment;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.hm.activity.BaseActivity;

/**
 * Created by Jeeva on 16/10/14.
 */
public abstract class BaseDialogFragment extends DialogFragment {

    public Context getContext(){
        return getActivity();
    }

    public void showMessage(String message) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(message);
        }
    }

    public void showMessage(String message, int duration) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showMessage(message, duration);
        }
    }

    public void showProgressbar() {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showProgressbar();
        }
    }

    public void updateProgressbar() {
    }

    public void dismissProgressbar() {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.dismissProgressbar();
        }
    }

    public void showSoftKeyboard(android.view.View view) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.showSoftKeyboard(view);
        }
    }

    public void hideSoftKeyboard() {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.hideSoftKeyboard();
        }
    }

    public LayoutInflater getLayoutInflater() {
        BaseActivity baseActivity = getBaseActivity();
        if(baseActivity != null){
            return baseActivity.getLayoutInflater();
        }
        return null;
    }

    public String getTrimmedText(TextView textView) {
        BaseActivity baseActivity = getBaseActivity();
        if (baseActivity != null) {
            baseActivity.getTrimmedText(textView);
        }
        return null;
    }

    public BaseActivity getBaseActivity() {
        Activity activity = getActivity();
        return activity != null && !activity.isFinishing() ? (BaseActivity) activity : null;
    }

}