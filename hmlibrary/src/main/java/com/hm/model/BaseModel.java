package com.hm.model;

import android.app.Activity;
import android.content.Context;

/**
 * Created by Nandakumar on 8/30/16.
 */
public abstract class BaseModel implements Model {

    protected ModelListener mModelListener;

    public BaseModel(ModelListener modelListener) {
        mModelListener = modelListener;
    }

    protected void onAuthorizationFailed() {
        mModelListener.onAuthorizationFailed();
    }

    public interface ModelListener {

        void onAuthorizationFailed();

        Context getContext();

        Activity getActivity();

    }



}
