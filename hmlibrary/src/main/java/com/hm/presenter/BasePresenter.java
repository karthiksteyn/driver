package com.hm.presenter;

import android.app.Activity;
import android.content.Context;

import com.hm.activity.View;

/**
 * Created by Nandakumar on 8/30/16.
 */
public abstract class BasePresenter {

    private View mView;

    public BasePresenter(View view) {
        mView = view;
    }

    public Context getContext() {
        return mView.getActivity();
    }

    public Activity getActivity() {
        return mView.getActivity();
    }

    public void onAuthorizationFailed() {
        mView.onAuthorizationFailed();
    }


}
