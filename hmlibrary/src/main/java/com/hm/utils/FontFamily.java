package com.hm.utils;

import android.content.Context;
import android.graphics.Typeface;

import com.hm.widgets.Typefaces;


/**
 * Created by hmspl on 15/1/16.
 */
public class FontFamily {


    public static final int OSWALD_REGULAR = 13;
    public static final int OSWALD_LIGHT = 14;
    public static final int OSWALD_MEDIUM = 15;
    public static final int OSWALD_BOLD = 16;
    public static final int MYRIAD_REGULAR = 17;



    public static Typeface getTypeface(Context context, int selection) {
        String path;
        switch (selection) {
            case MYRIAD_REGULAR:
                path = "fonts/Myriad Pro Regular.ttf";
                break;
            case OSWALD_LIGHT:
                path = "fonts/Oswald-Light.ttf";
                break;
            case OSWALD_MEDIUM:
                path = "fonts/Oswald-Medium.ttf";
                break;
            case OSWALD_BOLD:
                path = "fonts/Oswald-Bold.ttf";
                break;
            case OSWALD_REGULAR:
            default:
                path = "fonts/Oswald-Regular.ttf";
                break;
        }
        return Typefaces.get(context, path);
    }

}
