package com.hm.handler;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

/**
 * Created by Nandakumar on 8/22/16.
 */

public class PermissionHandler {

    public static PermissionHandler sPermissionHandler = new PermissionHandler();

    private PermissionHandleListener mPermissionHandleListener;

    public static PermissionHandler getInstance(PermissionHandleListener permissionHandleListener) {
        sPermissionHandler.mPermissionHandleListener = permissionHandleListener;
        return sPermissionHandler;
    }

    public void checkPermission(final Activity activity, final String permission, final int requestCode, String message) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            mPermissionHandleListener.onPermissionGranted(requestCode);
        } else {
            if (ContextCompat.checkSelfPermission(activity, permission)
                    != PackageManager.PERMISSION_GRANTED) {

                /*if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                    AlertDialog mPermissionsAlertDialog = new AlertDialog.Builder(activity)
                            .setMessage(message)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    requestPermission(activity, permission, requestCode);
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    mPermissionHandleListener.onPermissionReqCancel();
//                                    mPermissionHandleListener.onPermissionDenied(requestCode);
                                }
                            })
                            .create();
                    mPermissionsAlertDialog.show();

                } else {*/
                    requestPermission(activity, permission, requestCode);
//                }
            }else {
                mPermissionHandleListener.onPermissionGranted(requestCode);
            }
        }
    }

    private void requestPermission(Activity activity, String permission, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
    }

    public void onRequestPermissionResult(int requestCode, String permissions[], int[] grantResults) {
        Log.d("CheckingSSSSSSSSSS", "onRequestPermissionResult: ");
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mPermissionHandleListener.onPermissionGranted(requestCode);
        } else {
            mPermissionHandleListener.onPermissionDenied(requestCode);
        }
    }

    public boolean isPermissionGranted(Activity activity, String permission){
        return ContextCompat.checkSelfPermission(activity, permission)
                == PackageManager.PERMISSION_GRANTED;
    }


    public interface PermissionHandleListener {

        void onPermissionGranted(int requestCode);

        void onPermissionDenied(int requestCode);

        void onPermissionReqCancel();
    }

}
