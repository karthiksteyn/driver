package com.hm;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

/**
 * Created by Nandakumar on 8/22/16.
 */

public class HmLibrary {

    private static HmLibrary sHmLibrary = new HmLibrary();

    private Context mContext;

    private SharedPreferences sharedPreferences;

    public static HmLibrary getHmLibrary() {
        return sHmLibrary;
    }

    public static void init(Context context) {
        sHmLibrary.mContext = context;
        sHmLibrary.sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

    public void setSharedIntData(String key, int value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putInt(key, value);
        editor.commit();
    }

    public int getSharedIntData(String key) {
        return getSharedPreferences().getInt(key, -1);
    }

    public void setSharedStringData(String key, String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(key, value);
        editor.commit();
    }

    public float getSharedFloatData(String key) {
        return getSharedPreferences().getFloat(key, -1f);
    }

    public void setSharedFloatData(String key, float value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putFloat(key, value);
        editor.commit();
    }

    public String getSharedStringData(String key) {
        return getSharedPreferences().getString(key, null);
    }

    public long getSharedLongData(String key) {
        return getSharedPreferences().getLong(key, -1);
    }

    public void setSharedLongData(String key, long value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putLong(key, value);
        editor.commit();
    }


    public void setSharedBooleanData(String key, boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getSharedBooleanData(String key) {
        return getSharedPreferences().getBoolean(key, false);
    }

    public void clearData(String key) {
        getEditor().remove(key).commit();
    }

    private SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }

    public ConnectivityManager getConnectivityManager() {
        return (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public TelephonyManager getTelephonyManager() {
        return (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
    }

    /**
     * Checking the internet connectivity
     *
     * @return true if the connection is available otherwise false
     */
    public boolean hasNetworkConnection() {
        // TODO Auto-generated method stub

        ConnectivityManager cm = getConnectivityManager();
        boolean valid = false;

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnectedOrConnecting()) {
            valid = true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnectedOrConnecting()) {
            valid = true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
            valid = true;
        }

        return valid;
    }

    public void clearAllData() {
        getEditor().clear().commit();
    }

}
